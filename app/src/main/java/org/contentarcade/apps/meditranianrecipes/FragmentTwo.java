package org.contentarcade.apps.meditranianrecipes;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentTwo.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentTwo#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentTwo extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    LinearLayout myLinearLayout;

    TextView breakfastAdd;

    View view;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragmentTwo() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter first.
     * @param param2 Parameter second.
     * @return A new instance of fragment FragmentTwo.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentTwo newInstance(String param1, String param2) {
        FragmentTwo fragment = new FragmentTwo();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

         view = inflater.inflate(org.contentarcade.apps.meditranianrecipes.R.layout.fragment_fragment_two, container, false);
        breakfastAdd = (TextView)view.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.breakfastadd);
        addBreakfast();
        myLinearLayout = (LinearLayout)view.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.breakfastitems);
        final int N = 10; // total number of textviews to add

        final TextView[] myTextViews = new TextView[N]; // create an empty array;





        for (int i = 0; i < N; i++) {


            RelativeLayout rl = new RelativeLayout(getActivity());
            rl.setId(i);
            rl.setBackgroundResource(org.contentarcade.apps.meditranianrecipes.R.drawable.border);
            RelativeLayout.LayoutParams Lparams = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT);
            RelativeLayout.LayoutParams Lparams2 = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT);
            Lparams2.addRule(RelativeLayout.BELOW, org.contentarcade.apps.meditranianrecipes.R.id.Rl_Default);
            Lparams2.setMargins(3, 5, 3, 0);
            rl.setLayoutParams(Lparams);

            Lparams = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT);
            Lparams.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
            Lparams.setMargins(10, 0, 0, 0);



            TextView txt = new TextView(getActivity());
            txt.setTextColor(Color.parseColor("#000000"));
            txt.setId(0);
            txt.setTextSize(25);
            txt.setLayoutParams(Lparams);
            txt.setText("name");
            rl.addView(txt);

            Lparams = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT);
            Lparams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
            Lparams.addRule(RelativeLayout.BELOW, txt.getId());
            Lparams.setMargins(10, 0, 0, 0);

            TextView txtS = new TextView(getActivity());
            txt.setTextColor(Color.parseColor("#000000"));
            txtS.setId(org.contentarcade.apps.meditranianrecipes.R.id.calorielabel);
            txtS.setText("50");
            txtS.setTextSize(22);

            txtS.setLayoutParams(Lparams);
            txtS.setGravity(Gravity.BOTTOM);
            txtS.setPadding(0, 0, 0, 20);
            rl.addView(txtS);


            // create a new textview
            final TextView rowTextView = new TextView(getActivity());



            // set some properties of rowTextView or something
            rowTextView.setText("This is row #" + i);

            // add the textview to the linearlayout
           // myLinearLayout.addView(rowTextView);
            myLinearLayout.addView(rl);
            // save a reference to the textview for later
            myTextViews[i] = rowTextView;
        }


        return  view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public  void  initViews(){



    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public  void addBreakfast(){

        breakfastAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), AddMeal.class);

                startActivity(i);
            }
        });


    }

}