package org.contentarcade.apps.meditranianrecipes;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.fernandodev.easyratingdialog.library.EasyRatingDialog;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class HomeScreen extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener{


    private NavigationView navigationView;
    private DrawerLayout drawer;
    private View navHeader;

    //Animations
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Toolbar toolbar;
    //Add Mob
    private InterstitialAd interstitial;

    Dialog dialog;

    ImageView fab;
    RelativeLayout fab1;
    RelativeLayout fab2;
    RelativeLayout fab3;
    RelativeLayout fab4;
    RelativeLayout fab5;
    RelativeLayout fab6;
    RelativeLayout fab7;
    RelativeLayout fab8;



    CoordinatorLayout rootLayout;

    RelativeLayout fabContainer;
    TextView popup2;
    TextView smoothiePop;
    TextView beafPop;
    TextView fishPop;


    //Easy Rating Dialoge

    EasyRatingDialog easyRatingDialog;


    //Save the FAB's active status
    //false -> fab = close
    //true -> fab = open
    private boolean FAB_Status = false;


    SingeltonPattern sPattern;


    private boolean doubleBackToExitPressedOnce;

    //Animations
    Animation show_fab_1;
    Animation hide_fab_1;
    Animation show_fab_2;
    Animation hide_fab_2;
    Animation show_fab_3;
    Animation hide_fab_3;
    Animation show_fab_4;
    Animation hide_fab_4;
    Animation fade_in_bg;
    Animation fade_out_bg;

    Animation fab1_forkitkat_show;
    Animation fab1_forkitkat_hide;
    Animation fab2_forkitkat_show;
    Animation fab2_forkitkat_hide;
    Animation fab3_forkitkat_show;
    Animation fab3_forkitkat_hide;
    Animation fab4_forkitkat_show;
    Animation fab4_forkitkat_hide;


    View myView;


    boolean ratingFlag;

    Animation rotate;

    Animation fade_in;
    Animation fade_out;

    Context mContext;

    //UI References
    private EditText fromDateEtxt;
    private EditText toDateEtxt;

    private DatePickerDialog fromDatePickerDialog;
    private DatePickerDialog toDatePickerDialog;

    private SimpleDateFormat dateFormatter;



    // recipies Arrays

    ArrayList<FoodItem> filteredFood ;
    ArrayList<FoodItem> filterArray ;
    LinearLayout fl;
    LinearLayout flv;



    //Total Recipes of an ingredient TextViews


    TextView totalFishRecipestxtview;
    TextView totalBeafRecipestxtview;
    TextView totalVegetableRecipestxtview;
    TextView totalChickenRecipestxtview;
    TextView totalStarchRecipestxtview;
    TextView totalPastaRecipestxtview;
    TextView totalBreadRecipestxtview;
    TextView totalTurkyRecipestxtview;

    CustomGrid adapter;
    GridView grid;




    
    
    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        MobileAds.initialize(this, getString(R.string.app_id));
        sPattern = SingeltonPattern.getInstance();
        mContext = this;

        sPattern.setAnimationshowflag(false);
        sPattern.setCurrentArray(loadJSONFromAsset());
        drawerLayoutandToolbarLayout();

        ratingFlag = true;
        // addmethod();
    //    fl = (LinearLayout)findViewById(org.contentarcade.apps.southerenrecipes.R.id.filterfabbg);

        //Add filter fab layout with respect to android version

        addfablayoutwithVersion();


        // initialize fab views
        initViews();

        //initializing fab views
        initializeFab();


        initializerecipeCountTextviews();



       //fab layout from first to fourth functionality
        fab1tofab4Functionality();


        setCounterTextValue();
        initializerecipeCountTextviews();
        setTotalIngredientRecipes(sPattern.getCurrentArray());





        smoothiePop=(TextView) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.smoothpop);

        sPattern.setMealType("");
        viewpagerandTablayoutfunctionality();
        //Rate us method
        RateUs.app_launched(this);


         adapter = new CustomGrid(HomeScreen.this, sPattern.getCatList());



        grid=(GridView)findViewById(R.id.grid);
        
        


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(org.contentarcade.apps.meditranianrecipes.R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == org.contentarcade.apps.meditranianrecipes.R.id.nav_camera) {

            // Handle the camera action
            Intent i = new Intent(HomeScreen.this,ShoppingListActivity.class);
            startActivity(i);


        }
        if(id == R.id.privacy)  {

            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.contentarcade.com/privacy"
            )));
        }
        if(id==R.id.nav_moreappas){


            moreApps();
//            final String appPackageName =getPackageName();
//             // getPackageName() from Context or Activity object
//            try {
//                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://search?q=pub:Content+Arcade+Apps")));
//            }
//            catch (android.content.ActivityNotFoundException anfe) {
//                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/developer?id=Content+Arcade+Apps")));
//            }
        }
//        else if (id == R.id.nav_gallery) {
//
////            Intent detail = new Intent(MainActivity.this,ShoppingListActivity.class);
////            startActivity(detail);
//
//
//        } else if (id == R.id.nav_slideshow) {
//
//        } else if (id == R.id.nav_manage) {
//
//        } else if (id == R.id.nav_share) {
//
//        } else if (id == R.id.nav_send) {
//
//        }

        DrawerLayout drawer = (DrawerLayout) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new OneFragment(), "Weight Loss");
        adapter.addFragment(new LunchFragment(), "Detox");
//        adapter.addFragment(new DinnerFragment(), "Soup");
//        adapter.addFragment(new Dessert(), "Dessert");
//        adapter.addFragment(new Snacks(), "Bar");
//        adapter.addFragment(new Sandwich(), "Sandwich");
//        adapter.addFragment(new Appetizer(), "Pancake");
//        adapter.addFragment(new Waffle(), "Waffle");
//        adapter.addFragment(new Smoothie(), "Smoothie");
//        adapter.addFragment(new Bread(), "Bread");
//        adapter.addFragment(new Cookie(), "Cookie");






        viewPager.setAdapter(adapter);

    }



    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {




            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }



    private void expandFAB() {

        SingeltonPattern sPattern = SingeltonPattern.getInstance();

        if (viewPager.getCurrentItem()==0){
            sPattern.setMealType("Breakfast");
            setTotalIngredientRecipes(sPattern.getFoodItems());
            setCounterTextValue();
            setGridAdapter();

        }

       else{

            setTotalIngredientRecipes(sPattern.getCurrentArray());
            setCounterTextValue();
            setGridAdapter();

        }










        fab.startAnimation(rotate);
        fab.setImageResource(org.contentarcade.apps.meditranianrecipes.R.drawable.cancel);

//        FrameLayout fl = (FrameLayout)findViewById(R.id.filterfabbg);
//
//        fl.setBackgroundColor(getResources().getColor(R.color.filterlayoutbgcoloronshow));


       if(fl.getChildCount()<1) {

           fl.addView(myView);

       }
      //  fl = (LinearLayout)findViewById(org.contentarcade.apps.southerenrecipes.R.id.filterfabbg);
        fl.startAnimation(fade_in_bg);
        fl.setClickable(true);
        //flv.setClickable(false);

//        SingeltonPattern sPattern = SingeltonPattern.getInstance();

        sPattern.setAnimationshowflag(true);
        PlayGifView pGif = (PlayGifView) findViewById(R.id.animation);
        pGif.setImageResource(R.drawable.anim_spoon);


    }

    private void hideFAB() {

        // FrameLayout fl = (FrameLayout)findViewById(R.id.filterfabbg);

        // fl.setBackgroundColor(getResources().getColor(R.color.filterlayoutbgcoloronhide));

        fab.startAnimation(rotate);

        fab.setImageResource(org.contentarcade.apps.meditranianrecipes.R.drawable.filter);

        fl.startAnimation(fade_out_bg);
      //  fl = (LinearLayout)findViewById(org.contentarcade.apps.southerenrecipes.R.id.filterfabbg);

        fl.removeView(myView);


        fl.setClickable(false);

        SingeltonPattern sPattern = SingeltonPattern.getInstance();

        sPattern.setAnimationshowflag(false);




    }





    public ArrayList<FoodItem> loadJSONFromAsset() {
        ArrayList<FoodItem> locList = new ArrayList<>();
        String json = null;
        try {
            //InputStream is = getAssets().open("southeren.json");
            InputStream is = getResources().openRawResource(R.raw.southeren);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        try {
            JSONObject obj = new JSONObject(json);
            JSONArray m_jArry = obj.getJSONArray("Southern");

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);

                if(jo_inside.getString("Type of Diet").contains("Weight Loss")){


                    FoodItem recipies = new FoodItem();
                    String ingredientArray[] = jo_inside.getString("Ingredients").split("\\r?\\n");
                    recipies.setIngredientsArray(ingredientArray);

                    recipies.setRecipename(jo_inside.getString("Recipe Name"));
                    recipies.setIngredients(jo_inside.getString("Ingredients"));
                    recipies.setMethod(jo_inside.getString("Method"));
                    recipies.setIngredientType(jo_inside.getString("Type of Diet"));
                    recipies.setMealType(jo_inside.getString("Meal Type"));
                    recipies.setFoodType(jo_inside.getString("Food Type"));
                    recipies.setServings(jo_inside.getString("No. of Servings"));
                    recipies.setCaloriesBudget(jo_inside.getString("Calorie Budget"));
                    recipies.setNutriInfo(jo_inside.getString("Nutritional Information"));


                    //Add your values in your `ArrayList` as below:
                    locList.add(recipies);

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return locList;
    }
    public void filter (String maxCalories,String type,String mealType){

        sPattern = SingeltonPattern.getInstance();





//             if (mealType.contentEquals("Lunch")) {
//
//                 filteredFood = sPattern.getLunchItems();
//                 filterArray = new ArrayList<FoodItem>();
//
//             }
//             else if (mealType.contentEquals("Dinner")) {
//                 filteredFood = sPattern.getDinnerItems();
//                 filterArray = new ArrayList<FoodItem>();
//             }
//             else if (mealType.contentEquals("Snacks")) {
//                 filteredFood = sPattern.getSnackItems();
//                 filterArray = new ArrayList<FoodItem>();
//             }
//
//             else {
//
//                 // sPattern.setMealType("Breakfast");
//                 filteredFood = sPattern.getFoodItems();
//                 filterArray = new ArrayList<FoodItem>();
//             }
//



        if (maxCalories.contentEquals("All recipes")) {

            filterArray = new ArrayList<FoodItem>();
            for (int i = 0; i < sPattern.getCurrentArray().size(); i++) {

                String mxCal = maxCalories;

                if ( type.contains(sPattern.getCurrentArray().get(i).getIngredientType())) {
                    {

                        filterArray.add(sPattern.getCurrentArray().get(i));


                    }


                }
                sPattern.setFilterArray(filterArray);

            }
        }

        else {
            filterArray = new ArrayList<FoodItem>();
            for (int i = 0; i <sPattern.getCurrentArray().size(); i++) {

//            int minCalFilter = Integer.parseInt(filteredFood.get(i).getCaloriesBudget());
//            int mnCal = Integer.parseInt(minCalories);
                // int maxCalFilter = Integer.parseInt(filteredFood.get(i).getCaloriesBudget());
                String mxCal = maxCalories;

                if (sPattern.getCurrentArray().get(i).getCaloriesBudget().contentEquals(maxCalories)&&type.contains(sPattern.getCurrentArray().get(i).getIngredientType()))
                {

                    filterArray.add(sPattern.getCurrentArray().get(i));


                }


            }
            sPattern.setFilterArray(filterArray);
        }









    }


    public static String removeWords(String word ,String remove) {
        return word.replace(remove,"");
    }



    /**
     * Exit the app if user select yes.
     */
    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.drawer_layout);



        if (doubleBackToExitPressedOnce ) {
            super.onBackPressed();

        }



        if(FAB_Status == false){

            Toast.makeText(this, this.getResources().getString(org.contentarcade.apps.meditranianrecipes.R.string.exit_toast), Toast.LENGTH_SHORT).show();

            ratingFlag = false;


        }






            if (sPattern.isAnimationshowflag()) {

                hideFAB();
                sPattern.setAnimationshowflag(false);
                //  Toast.makeText(this, this.getResources().getString(R.string.exit_toast), Toast.LENGTH_SHORT).show();
                FAB_Status = false;


            }
        //  }

        this.doubleBackToExitPressedOnce = true;


        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;


            }
        }, 1000);


    }

    @Override
    protected void onStart() {
        super.onStart();
        // easyRatingDialog.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //  easyRatingDialog.showIfNeeded();
    }


    public void addmethod(){


        // Prepare the Interstitial Ad
        interstitial = new InterstitialAd(HomeScreen.this);
        // Insert the Ad Unit ID
        interstitial.setAdUnitId("ca-app-pub-123456789/123456789");

        //Locate the Banner Ad in activity_main.xml
        // AdView adView = (AdView) this.findViewById(R.id.adView);

        // Request for Ads
        AdRequest adRequest = new AdRequest.Builder()

                // Add a test device to show Test Ads
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("D5D58A2C3DF4AD796F86B8DF57535701")
                .build();

        // Load ads into Banner Ads
        // adView.loadAd(adRequest);

        // Load ads into Interstitial Ads
        interstitial.loadAd(adRequest);

        // Prepare an Interstitial Ad Listener
        interstitial.setAdListener(new AdListener() {
            public void onAdLoaded() {
                // Call displayInterstitial() function
                displayInterstitial();
            }
        });

    }
    public void displayInterstitial() {
        // If Ads are loaded, show Interstitial else show nothing.
        if (interstitial.isLoaded()) {
            interstitial.show();
        }
    }
    @Override
    public void onStop() {

        super.onStop();


    }

    public void pasta(){

        fab5.setOnClickListener(new View.OnClickListener()

                                {
                                    @Override
                                    public void onClick(View v) {


                                        //   Toast.makeText(getApplication(), "Floating Action Button third", Toast.LENGTH_SHORT).show();
                                        //  Toast.makeText(getApplication(), "Floating Action Button second", Toast.LENGTH_SHORT).show();

                                        if(totalPastaRecipestxtview.getText()=="0" ||totalPastaRecipestxtview.getText()==""){

                                            return;


                                        }


                                        sPattern.setIngredientType("Pasta");
                                        sPattern.setCaloriesBudgetIndex(0);

                                        sPattern.setMaxCalorie("All recipes");


                                        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
                                        View layout1 = inflater.inflate(org.contentarcade.apps.meditranianrecipes.R.layout.dialog_layout, (ViewGroup) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.root));

                                        Button search = (Button) layout1.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.searchButton);


//               final TextView tv = (TextView) layout1.findViewById(R.id.);
                                        NumberPicker np = (NumberPicker) layout1.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.np);
                                        //   NumberPicker np1 = (NumberPicker) layout1.findViewById(R.id.np1);

                                        //Set TextView text color
                                        // tv.setTextColor(Color.parseColor("#FF2C834F"));

                                        //Initializing a new string array with elements
//                                        final String[] values = {"All recipies", ">100 calories", ">200 calories", ">300 calories"
//
//                                        };

                                        //Populate NumberPicker values from String array valuese
                                        String[] values = new String[getCaloriesRange("Pasta").size()];
                                        ;

                                        values = getCaloriesRange("Pasta").toArray(values);

                                        final String[] values2 = {"No Maximum", "<100 calories", "<200 calories", "<300 calories", "<400 calories", "<500 calories",

                                                "<600 calories", "<700 calories", "<800 calories", "<900 calories", "<1000 calories"

                                        };


                                        //Populate NumberPicker values from String array valuese
                                        //Set the minimum value of NumberPicker
                                        np.setMinValue(0); //from array first value
                                        //Specify the maximum value/number of NumberPicker
                                        np.setMaxValue(values.length - 1); //to array last value

                                        //Specify the NumberPicker data source as array elements
                                        np.setDisplayedValues(values);

                                        //Gets whether the selector wheel wraps when reaching the min/max value.
                                        np.setWrapSelectorWheel(true);
                                        sPattern.setMaxCalorie(values[0]);
                                        sPattern.setCaloriesBudgetIndex(0);

                                        //Set a value change listener for NumberPicker
                                        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                                            @Override
                                            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                                                //Display the newly selected value from picker
                                                //  tv.setText("Selected value : " + values[newVal]);
                                                //Populate NumberPicker values from String array valuese
                                                String[] values = new String[getCaloriesRange("Pasta").size()];
                                                ;

                                                values = getCaloriesRange("Pasta").toArray(values);
                                                sPattern.setMaxCalorie(values[newVal]);
                                                sPattern.setCaloriesBudgetIndex(newVal);
                                                // Toast.makeText(getApplication(), String.valueOf(sPattern.getMaxCalorie()), Toast.LENGTH_SHORT).show();

                                            }
                                        });


                                        AlertDialog.Builder yourDialog = new AlertDialog.Builder(
                                                HomeScreen.this);

                                        yourDialog.setPositiveButton("Search", new DialogInterface.OnClickListener() {


                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                                filter(sPattern.getMaxCalorie().toString(), sPattern.getIngredientType().toString(), sPattern.getMealType().toString());


                                                if (sPattern.getFilterArray().size() == 0) {

                                                    Toast.makeText(mContext, "No Record Found", Toast.LENGTH_SHORT).show();

                                                } else {
                                                    Intent filterRresultActivityIntent = new Intent(HomeScreen.this, FilterresultActivity.class);

                                                    startActivity(filterRresultActivityIntent);

                                                }
                                            }


                                        });

                                        yourDialog.setNegativeButton("Cancel", null);

                                        yourDialog.setView(layout1);

                                        AlertDialog dialog = yourDialog.create();

                                        dialog.show();
                                    }


                                }
        );


    }


    public void starch() {

        fab8.setOnClickListener(new View.OnClickListener()

                                {
                                    @Override
                                    public void onClick(View v) {


                                        if (totalStarchRecipestxtview.getText() == "0" || totalStarchRecipestxtview.getText() == "") {

                                            return;


                                        }

                                        //   Toast.makeText(getApplication(), "Floating Action Button third", Toast.LENGTH_SHORT).show();
                                        //  Toast.makeText(getApplication(), "Floating Action Button second", Toast.LENGTH_SHORT).show();

                                        sPattern.setIngredientType("Starch");
                                        sPattern.setCaloriesBudgetIndex(0);

                                        sPattern.setMaxCalorie("All recipes");


                                        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
                                        View layout1 = inflater.inflate(org.contentarcade.apps.meditranianrecipes.R.layout.dialog_layout, (ViewGroup) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.root));

                                        Button search = (Button) layout1.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.searchButton);


//               final TextView tv = (TextView) layout1.findViewById(R.id.);
                                        NumberPicker np = (NumberPicker) layout1.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.np);
                                        //   NumberPicker np1 = (NumberPicker) layout1.findViewById(R.id.np1);

                                        //Set TextView text color
                                        // tv.setTextColor(Color.parseColor("#FF2C834F"));

                                        //Initializing a new string array with elements
                                        // final String[] values = {"All recipies", ">100 calories", ">200 calories", ">300 calories"};

                                        //Populate NumberPicker values from String array valuese
                                        String[] values = new String[getCaloriesRange("Starch").size()];
                                        ;

                                        values = getCaloriesRange("Starch").toArray(values);

                                        final String[] values2 = {"No Maximum", "<100 calories", "<200 calories", "<300 calories", "<400 calories", "<500 calories",

                                                "<600 calories", "<700 calories", "<800 calories", "<900 calories", "<1000 calories"

                                        };


                                        //Populate NumberPicker values from String array valuese
                                        //Set the minimum value of NumberPicker
                                        np.setMinValue(0); //from array first value
                                        //Specify the maximum value/number of NumberPicker
                                        np.setMaxValue(values.length - 1); //to array last value

                                        //Specify the NumberPicker data source as array elements
                                        np.setDisplayedValues(values);

                                        //Gets whether the selector wheel wraps when reaching the min/max value.
                                        np.setWrapSelectorWheel(true);
                                        sPattern.setMaxCalorie(values[0]);
                                        sPattern.setCaloriesBudgetIndex(0);

                                        //Set a value change listener for NumberPicker
                                        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                                            @Override
                                            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                                                //Display the newly selected value from picker
                                                //  tv.setText("Selected value : " + values[newVal]);
                                                //Populate NumberPicker values from String array valuese
                                                String[] values = new String[getCaloriesRange("Starch").size()];
                                                ;

                                                values = getCaloriesRange("Starch").toArray(values);


                                                sPattern.setMaxCalorie(values[newVal]);
                                                sPattern.setCaloriesBudgetIndex(newVal);
                                                // Toast.makeText(getApplication(), String.valueOf(sPattern.getMaxCalorie()), Toast.LENGTH_SHORT).show();

                                            }
                                        });


                                        AlertDialog.Builder yourDialog = new AlertDialog.Builder(
                                                HomeScreen.this);

                                        yourDialog.setPositiveButton("Search", new DialogInterface.OnClickListener() {


                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                                filter(sPattern.getMaxCalorie().toString(), sPattern.getIngredientType().toString(), sPattern.getMealType().toString());


                                                if (sPattern.getFilterArray().size() == 0) {

                                                    Toast.makeText(mContext, "No Record Found", Toast.LENGTH_SHORT).show();

                                                } else {
                                                    Intent filterRresultActivityIntent = new Intent(HomeScreen.this, FilterresultActivity.class);

                                                    startActivity(filterRresultActivityIntent);

                                                }
                                            }


                                        });

                                        yourDialog.setNegativeButton("Cancel", null);

                                        yourDialog.setView(layout1);

                                        AlertDialog dialog = yourDialog.create();

                                        dialog.show();
                                    }


                                }
        );





    }

    public void bread(){

        fab7.setOnClickListener(new View.OnClickListener()

                                {
                                    @Override
                                    public void onClick(View v) {


                                        if (totalBreadRecipestxtview.getText() == "0" || totalBreadRecipestxtview.getText() == "") {

                                            return;


                                        }

                                        //   Toast.makeText(getApplication(), "Floating Action Button third", Toast.LENGTH_SHORT).show();
                                        //  Toast.makeText(getApplication(), "Floating Action Button second", Toast.LENGTH_SHORT).show();

                                        sPattern.setIngredientType("Bread");
                                        sPattern.setCaloriesBudgetIndex(0);

                                        sPattern.setMaxCalorie("All recipes");


                                        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
                                        View layout1 = inflater.inflate(org.contentarcade.apps.meditranianrecipes.R.layout.dialog_layout, (ViewGroup) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.root));

                                        Button search = (Button) layout1.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.searchButton);


//               final TextView tv = (TextView) layout1.findViewById(R.id.);
                                        NumberPicker np = (NumberPicker) layout1.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.np);
                                        //   NumberPicker np1 = (NumberPicker) layout1.findViewById(R.id.np1);

                                        //Set TextView text color
                                        // tv.setTextColor(Color.parseColor("#FF2C834F"));

                                        //Initializing a new string array with elements
//                                        final String[] values = {"All recipies", ">100 calories", ">200 calories", ">300 calories"
//
//                                        };
                                        //Populate NumberPicker values from String array valuese
                                        String[] values = new String[getCaloriesRange("Bread").size()];
                                        ;

                                        values = getCaloriesRange("Bread").toArray(values);
                                        final String[] values2 = {"No Maximum", "<100 calories", "<200 calories", "<300 calories", "<400 calories", "<500 calories",

                                                "<600 calories", "<700 calories", "<800 calories", "<900 calories", "<1000 calories"

                                        };


                                        //Populate NumberPicker values from String array valuese
                                        //Set the minimum value of NumberPicker
                                        np.setMinValue(0); //from array first value
                                        //Specify the maximum value/number of NumberPicker
                                        np.setMaxValue(values.length - 1); //to array last value

                                        //Specify the NumberPicker data source as array elements
                                        np.setDisplayedValues(values);

                                        //Gets whether the selector wheel wraps when reaching the min/max value.
                                        np.setWrapSelectorWheel(true);
                                        sPattern.setMaxCalorie(values[0]);
                                        sPattern.setCaloriesBudgetIndex(0);

                                        //Set a value change listener for NumberPicker
                                        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                                            @Override
                                            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                                                //Display the newly selected value from picker
                                                //  tv.setText("Selected value : " + values[newVal]);

                                                //Populate NumberPicker values from String array valuese
                                                String[] values = new String[getCaloriesRange("Bread").size()];
                                                ;

                                                values = getCaloriesRange("Bread").toArray(values);


                                                sPattern.setMaxCalorie(values[newVal]);
                                                sPattern.setCaloriesBudgetIndex(newVal);
                                                // Toast.makeText(getApplication(), String.valueOf(sPattern.getMaxCalorie()), Toast.LENGTH_SHORT).show();

                                            }
                                        });


                                        AlertDialog.Builder yourDialog = new AlertDialog.Builder(
                                                HomeScreen.this);

                                        yourDialog.setPositiveButton("Search", new DialogInterface.OnClickListener() {


                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                                filter(sPattern.getMaxCalorie().toString(), sPattern.getIngredientType().toString(), sPattern.getMealType().toString());


                                                if (sPattern.getFilterArray().size() == 0) {

                                                    Toast.makeText(mContext, "No Record Found", Toast.LENGTH_SHORT).show();

                                                } else {
                                                    Intent filterRresultActivityIntent = new Intent(HomeScreen.this, FilterresultActivity.class);

                                                    startActivity(filterRresultActivityIntent);

                                                }
                                            }


                                        });

                                        yourDialog.setNegativeButton("Cancel", null);

                                        yourDialog.setView(layout1);

                                        AlertDialog dialog = yourDialog.create();

                                        dialog.show();
                                    }


                                }
        );



    }

    public void turkey(){

        fab6.setOnClickListener(new View.OnClickListener()

                                {
                                    @Override
                                    public void onClick(View v) {


                                        if (totalTurkyRecipestxtview.getText() == "0" || totalTurkyRecipestxtview.getText() == "") {

                                            return;


                                        }


                                        //   Toast.makeText(getApplication(), "Floating Action Button third", Toast.LENGTH_SHORT).show();
                                        //  Toast.makeText(getApplication(), "Floating Action Button second", Toast.LENGTH_SHORT).show();

                                        sPattern.setIngredientType("Turkey");
                                        sPattern.setCaloriesBudgetIndex(0);

                                        sPattern.setMaxCalorie("All recipes");


                                        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
                                        View layout1 = inflater.inflate(org.contentarcade.apps.meditranianrecipes.R.layout.dialog_layout, (ViewGroup) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.root));

                                        Button search = (Button) layout1.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.searchButton);


//               final TextView tv = (TextView) layout1.findViewById(R.id.);
                                        NumberPicker np = (NumberPicker) layout1.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.np);
                                        //   NumberPicker np1 = (NumberPicker) layout1.findViewById(R.id.np1);

                                        //Set TextView text color
                                        // tv.setTextColor(Color.parseColor("#FF2C834F"));

                                        //Initializing a new string array with elements
                                        // final String[] values = {"All recipies", ">100 calories", ">200 calories", ">300 calories"};

                                        //Populate NumberPicker values from String array valuese
                                        String[] values = new String[getCaloriesRange("Turkey").size()];
                                        ;

                                        values = getCaloriesRange("Turkey").toArray(values);

                                        final String[] values2 = {"No Maximum", "<100 calories", "<200 calories", "<300 calories", "<400 calories", "<500 calories",

                                                "<600 calories", "<700 calories", "<800 calories", "<900 calories", "<1000 calories"

                                        };


                                        //Populate NumberPicker values from String array valuese
                                        //Set the minimum value of NumberPicker
                                        np.setMinValue(0); //from array first value
                                        //Specify the maximum value/number of NumberPicker
                                        np.setMaxValue(values.length - 1); //to array last value

                                        //Specify the NumberPicker data source as array elements
                                        np.setDisplayedValues(values);

                                        //Gets whether the selector wheel wraps when reaching the min/max value.
                                        np.setWrapSelectorWheel(true);
                                        sPattern.setMaxCalorie(values[0]);
                                        sPattern.setCaloriesBudgetIndex(0);

                                        //Set a value change listener for NumberPicker
                                        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                                            @Override
                                            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                                                //Display the newly selected value from picker
                                                //  tv.setText("Selected value : " + values[newVal]);
                                                String[] values = new String[getCaloriesRange("Turkey").size()];
                                                ;

                                                values = getCaloriesRange("Turkey").toArray(values);
                                                sPattern.setMaxCalorie(values[newVal]);
                                                sPattern.setCaloriesBudgetIndex(newVal);
                                                // Toast.makeText(getApplication(), String.valueOf(sPattern.getMaxCalorie()), Toast.LENGTH_SHORT).show();

                                            }
                                        });


                                        AlertDialog.Builder yourDialog = new AlertDialog.Builder(
                                                HomeScreen.this);

                                        yourDialog.setPositiveButton("Search", new DialogInterface.OnClickListener() {


                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                                filter(sPattern.getMaxCalorie().toString(), sPattern.getIngredientType().toString(), sPattern.getMealType().toString());


                                                if (sPattern.getFilterArray().size() == 0) {

                                                    Toast.makeText(mContext, "No Record Found", Toast.LENGTH_SHORT).show();

                                                } else {
                                                    Intent filterRresultActivityIntent = new Intent(HomeScreen.this, FilterresultActivity.class);

                                                    startActivity(filterRresultActivityIntent);

                                                }
                                            }


                                        });

                                        yourDialog.setNegativeButton("Cancel", null);

                                        yourDialog.setView(layout1);

                                        AlertDialog dialog = yourDialog.create();

                                        dialog.show();
                                    }


                                }
        );



    }



    public void initializeFab(){

        pasta();
        starch();
        bread();
        turkey();



    }




   


 public void initViews(){

     //Floating Action Buttons
     fab = (ImageView) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.fab);
     fab1 = (RelativeLayout) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.fab_1);
     fab2 = (RelativeLayout) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.fab_2);
     fab3 = (RelativeLayout) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.fab_3);
     fab4 = (RelativeLayout)findViewById(org.contentarcade.apps.meditranianrecipes.R.id.fab_4);
     fab5 = (RelativeLayout)findViewById(org.contentarcade.apps.meditranianrecipes.R.id.fab_51);
     fab6 = (RelativeLayout) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.fab_6);
     fab7 = (RelativeLayout)findViewById(org.contentarcade.apps.meditranianrecipes.R.id.fab_7);
     fab8 = (RelativeLayout)findViewById(org.contentarcade.apps.meditranianrecipes.R.id.fab_8);



     popup2 = (TextView)findViewById(org.contentarcade.apps.meditranianrecipes.R.id.pop);
     beafPop = (TextView)findViewById(org.contentarcade.apps.meditranianrecipes.R.id.beifpop);
     fishPop = (TextView)findViewById(org.contentarcade.apps.meditranianrecipes.R.id.fishpop);
     //Animations
     show_fab_1 = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fab1_show);
     hide_fab_1 = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fab1_hide);
     show_fab_2 = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fab2_show);
     hide_fab_2 = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fab2_hide);
     show_fab_3 = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fab3_show);
     hide_fab_3 = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fab3_hide);
     show_fab_4 = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fab4_show);
     hide_fab_4 = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fab4_hide);

     fab1_forkitkat_show = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fab1_forkitkat_animation_show);
     fab1_forkitkat_hide = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fab1_forkitkat_animation_hide);
     fab2_forkitkat_show = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fab2_forkitkat_animation_show);
     fab2_forkitkat_hide = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fab2_forkitkat_animation_hide);
     fab3_forkitkat_hide = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fab3_forkitkat_animation_hide);
     fab3_forkitkat_show = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fab3_forkitkat_animation_show);
     fab4_forkitkat_hide = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fab4_forkitkat_animation_hide);
     fab4_forkitkat_show = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fab4_forkitkat_animation_show);

     fade_in = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fade_in);
     fade_out = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fade_out);
     fade_in_bg = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fade_in_bg);
     fade_out_bg = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fade_out_bg);
     rotate = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.rotate);

 }

    public void fab1tofab4Functionality() {


        fab.setOnClickListener(new View.OnClickListener() {
                                   @Override
                                   public void onClick(View view) {

                                       sPattern = SingeltonPattern.getInstance();

//                fl = (FrameLayout)findViewById(R.id.filterfabbg);
//
                                       //   fl.setVisibility(View.VISIBLE);

                                       DisplayMetrics metrics;
                                       int width = 0;
                                       int height = 0;

                                       metrics = new DisplayMetrics();

                                       getWindowManager().getDefaultDisplay().getMetrics(metrics);

                                       height = metrics.heightPixels;
                                       width = metrics.widthPixels;


                                       if (FAB_Status == false) {
                                           //Display FAB menu
                                           //  fl = (LinearLayout) findViewById(org.contentarcade.apps.southerenrecipes.R.id.filterfabbg);
                                           //      fl.setVisibility(View.VISIBLE);
                                           expandFAB();


                                           //
                                           //  fl.setClickable(true);

                                           FAB_Status = true;

                                       } else {
                                           //Close FAB menu

                                           hideFAB();


                                           FAB_Status = false;


                                       }
                                       //     }


                                       // Check if this is the page you want.


                                       if (viewPager.getCurrentItem() == 0) {

                                           sPattern.setMealType("Breakfast");
                                           sPattern.setCurrentArray(loadJSONFromAsset());
                                           Log.d("foodItems", sPattern.getCurrentArray().get(1).getRecipename());
                                           //  Toast.makeText(AddMeal.this,"Chicken",Toast.LENGTH_SHORT).show();
                                           if (sPattern.isAnimationshowflag()) {

                                               setGridAdapter();


                                           }


                                       }


                                       if (viewPager.getCurrentItem() == 1) {

                                           //sPattern.setMealType("Lunch");
                                           sPattern.setCurrentArray(sPattern.getLunchItems());

                                           //Toast.makeText(AddMeal.this,"Chicken",Toast.LENGTH_SHORT).show();
                                           if (sPattern.isAnimationshowflag()) {

                                               setGridAdapter();


                                           }

                                       }


//                                       if (viewPager.getCurrentItem() == 2) {
//
//                                           //sPattern.setMealType("Dinner");
//                                           sPattern.setCurrentArray(sPattern.getDinnerItems());
//                                           if (sPattern.isAnimationshowflag()) {
//
//                                               setGridAdapter();
//
//
//                                           }
//
//
//                                       }
//
//                                       if (viewPager.getCurrentItem() == 3) {
//
//                                           //sPattern.setMealType("Snack");
//
//                                           sPattern.setCurrentArray(sPattern.getDesrtItems());
//                                           if (sPattern.isAnimationshowflag()) {
//
//                                               setGridAdapter();
//
//
//                                           }
//
//
//                                       }
//                                       if (viewPager.getCurrentItem() == 4) {
//
//                                           //sPattern.setMealType("Snack");
//
//                                           sPattern.setCurrentArray(sPattern.getSandwichItems());
//                                           if (sPattern.isAnimationshowflag()) {
//
//                                               setGridAdapter();
//
//
//                                           }
//
//
//                                       }
//
//                                       if (viewPager.getCurrentItem() == 5) {
//
//                                           //sPattern.setMealType("Snack");
//
//                                           //sPattern.setCurrentArray(sPattern.getPancakeItems());
//
//
//                                           sPattern.setCurrentArray(sPattern.getAppetizeItems());
//                                           if (sPattern.isAnimationshowflag()) {
//
//                                               setGridAdapter();
//
//
//                                           }
//
//
//                                       }
//
//
//                                       if (viewPager.getCurrentItem() == 6) {
//
//                                           //sPattern.setMealType("Snack");
//
//                                           sPattern.setCurrentArray(sPattern.getWaffleItems());
//                                           if (sPattern.isAnimationshowflag()) {
//
//                                               setGridAdapter();
//
//
//                                           }
//
//
//                                       }
//
//                                       if (viewPager.getCurrentItem() ==7) {
//
//                                           //sPattern.setMealType("Snack");
//
//                                           sPattern.setCurrentArray(sPattern.getSmoothieItems());
//                                           if (sPattern.isAnimationshowflag()) {
//
//                                               setGridAdapter();
//
//
//                                           }
//
//
//                                       }
//
//
//
//                                       if (viewPager.getCurrentItem() == 8) {
//
//                                           //sPattern.setMealType("Snack");
//
//                                           sPattern.setCurrentArray(sPattern.getDesrtItems());
//                                           if (sPattern.isAnimationshowflag()) {
//
//                                               setGridAdapter();
//
//
//                                           }
//
//                                           if (viewPager.getCurrentItem() == 9) {
//
//                                               //sPattern.setMealType("Snack");
//
//                                               sPattern.setCurrentArray(sPattern.getBreadItems());
//                                               if (sPattern.isAnimationshowflag()) {
//
//                                                   setGridAdapter();
//
//
//                                               }
//
//
//                                       }
//
//
//                                           if (viewPager.getCurrentItem() == 10) {
//
//                                               //sPattern.setMealType("Snack");
//
//                                               sPattern.setCurrentArray(sPattern.getCookieItems());
//                                               if (sPattern.isAnimationshowflag()) {
//
//                                                   setGridAdapter();
//
//
//                                               }
//
//
//                                           }





                                   }
                               }

        );

        fab4.setOnClickListener(new View.OnClickListener()

                                {
                                    @Override
                                    public void onClick(View v) {

                                        if (totalVegetableRecipestxtview.getText() == "0" || totalVegetableRecipestxtview.getText() == "") {

                                            return;


                                        }

                                        //
                                        // Toast.makeText(getApplication(), "Floating Action Button first", Toast.LENGTH_SHORT).show();
                                        // Setup the new range seek bar
                                        //   final RangeSeekBar<Integer> rangeSeekBar = new RangeSeekBar<Integer>(HomeScreen.this);


                                        sPattern.setCaloriesBudgetIndex(0);

                                        sPattern.setMaxCalorie("All recipes");

                                        sPattern.setIngredientType("Fruit, Vegetable");


                                        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
                                        View layout1 = inflater.inflate(org.contentarcade.apps.meditranianrecipes.R.layout.dialog_layout, (ViewGroup) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.root));
//                LinearLayout ll = (LinearLayout)layout1.findViewById(R.id.placeholder);
//                ll.addView(rangeSeekBar);

                                        Button search = (Button) layout1.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.searchButton);


//               final TextView tv = (TextView) layout1.findViewById(R.id.);
                                        NumberPicker np = (NumberPicker) layout1.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.np);
                                        //   NumberPicker np1 = (NumberPicker) layout1.findViewById(R.id.np1);

                                        //Set TextView text color
                                        // tv.setTextColor(Color.parseColor("#FF2C834F"));

                                        //Initializing a new string array with elements
//                                        final String[] values = {"All recipies", ">100 calories", ">200 calories"
//
//                                        };

                                        String[] values = new String[getCaloriesRange("Fruit, Vegetable").size()];
                                        ;

                                        values = getCaloriesRange("Fruit, Vegetable").toArray(values);

                                        final String[] values2 = {"No Maximum", "<100 calories", "<200 calories", "<300 calories", "<400 calories", "<500 calories",

                                                "<600 calories", "<700 calories", "<800 calories", "<900 calories", "<1000 calories"

                                        };


                                        //Populate NumberPicker values from String array values
                                        //Set the minimum value of NumberPicker
                                        np.setMinValue(0); //from array first value
                                        //Specify the maximum value/number of NumberPicker
                                        np.setMaxValue(values.length - 1); //to array last value

                                        //Specify the NumberPicker data source as array elements
                                        np.setDisplayedValues(values);

                                        //Gets whether the selector wheel wraps when reaching the min/max value.
                                        np.setWrapSelectorWheel(true);

                                        //Set a value change listener for NumberPicker
                                        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                                            @Override
                                            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                                                //Display the newly selected value from picker
                                                //  tv.setText("Selected value : " + values[newVal]);
                                                String[] values = new String[getCaloriesRange("Fruit, Vegetable").size()];
                                                ;

                                                values = getCaloriesRange("Fruit, Vegetable").toArray(values);
                                                sPattern.setMaxCalorie(values[newVal]);
                                                sPattern.setCaloriesBudgetIndex(newVal);
                                                //   Toast.makeText(getApplication(), String.valueOf(sPattern.getMaxCalorie()), Toast.LENGTH_SHORT).show();

                                            }
                                        });


                                        AlertDialog.Builder yourDialog = new AlertDialog.Builder(HomeScreen.this);

                                        yourDialog.setPositiveButton("Search", new DialogInterface.OnClickListener() {


                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                                filter(sPattern.getMaxCalorie().toString(), sPattern.getIngredientType().toString(), sPattern.getMealType().toString());


                                                if (sPattern.getFilterArray().size() == 0) {

                                                    Toast.makeText(mContext, "No Record Found", Toast.LENGTH_SHORT).show();

                                                } else {
                                                    Intent filterRresultActivityIntent = new Intent(HomeScreen.this, FilterresultActivity.class);

                                                    startActivity(filterRresultActivityIntent);

                                                }

                                            }


                                        });

                                        yourDialog.setNegativeButton("Cancel", null);

                                        yourDialog.setView(layout1);

                                        AlertDialog dialog = yourDialog.create();

                                        dialog.show();


                                    }
                                }

        );

        fab2.setOnClickListener(new View.OnClickListener()

                                {
                                    @Override
                                    public void onClick(View v) {

                                        if (totalChickenRecipestxtview.getText() == "0" || totalChickenRecipestxtview.getText() == "") {

                                            return;


                                        }


                                        // Toast.makeText(getApplication(), "Floating Action Button second", Toast.LENGTH_SHORT).show();

                                        sPattern.setIngredientType("Chicken&Egg");

                                        sPattern.setRecipieType("Beverage - Smoothie");

                                        sPattern.setCaloriesBudgetIndex(0);

                                        sPattern.setMaxCalorie("All recipes");


                                        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
                                        View layout1 = inflater.inflate(org.contentarcade.apps.meditranianrecipes.R.layout.dialog_layout, (ViewGroup) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.root));

                                        Button search = (Button) layout1.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.searchButton);


//               final TextView tv = (TextView) layout1.findViewById(R.id.);
                                        NumberPicker np = (NumberPicker) layout1.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.np);
                                        //   NumberPicker np1 = (NumberPicker) layout1.findViewById(R.id.np1);

                                        //Set TextView text color
                                        // tv.setTextColor(Color.parseColor("#FF2C834F"));

                                        //Initializing a new string array with elements
//                                        final String[] values = {"All recipies", ">100 calories", ">200 calories", ">300 calories", ">400 calories"
//
//                                        };
                                        String[] values = new String[getCaloriesRange("Chicken,Egg").size()];
                                        ;

                                        values = getCaloriesRange("Chicken,Egg").toArray(values);
                                        final String[] values2 = {"No Maximum", "<100 calories", "<200 calories", "<300 calories", "<400 calories", "<500 calories",

                                                "<600 calories", "<700 calories", "<800 calories", "<900 calories", "<1000 calories"

                                        };


                                        //Populate NumberPicker values from String array values
                                        //Set the minimum value of NumberPicker
                                        np.setMinValue(0); //from array first value
                                        //Specify the maximum value/number of NumberPicker
                                        np.setMaxValue(values.length - 1); //to array last value

                                        //Specify the NumberPicker data source as array elements
                                        np.setDisplayedValues(values);

                                        //Gets whether the selector wheel wraps when reaching the min/max value.
                                        np.setWrapSelectorWheel(true);

                                        //Set a value change listener for NumberPicker
                                        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                                            @Override
                                            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                                                //Display the newly selected value from picker
                                                //  tv.setText("Selected value : " + values[newVal]);
                                                String[] values = new String[getCaloriesRange("Chicken, Egg").size()];
                                                ;

                                                values = getCaloriesRange("Chicken").toArray(values);
                                                sPattern.setMaxCalorie(values[newVal]);
                                                sPattern.setCaloriesBudgetIndex(newVal);
                                                // Toast.makeText(getApplication(), String.valueOf(sPattern.getMaxCalorie()), Toast.LENGTH_SHORT).show();

                                            }
                                        });


                                        AlertDialog.Builder yourDialog = new AlertDialog.Builder(
                                                HomeScreen.this);

                                        yourDialog.setPositiveButton("Search", new DialogInterface.OnClickListener() {


                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                                filter(sPattern.getMaxCalorie().toString(), sPattern.getIngredientType().toString(), sPattern.getMealType().toString());


                                                if (sPattern.getFilterArray().size() == 0) {

                                                    Toast.makeText(mContext, "No Record Found", Toast.LENGTH_SHORT).show();

                                                } else {
                                                    Intent filterRresultActivityIntent = new Intent(HomeScreen.this, FilterresultActivity.class);

                                                    startActivity(filterRresultActivityIntent);

                                                }
                                            }


                                        });

                                        yourDialog.setNegativeButton("Cancel", null);

                                        yourDialog.setView(layout1);

                                        AlertDialog dialog = yourDialog.create();

                                        dialog.show();
                                    }


                                }

        );

        fab3.setOnClickListener(new View.OnClickListener()

                                {
                                    @Override
                                    public void onClick(View v) {

                                        if (totalBeafRecipestxtview.getText() == "0" || totalBeafRecipestxtview.getText() == "") {

                                            return;


                                        }
                                        //   Toast.makeText(getApplication(), "Floating Action Button third", Toast.LENGTH_SHORT).show();
                                        //  Toast.makeText(getApplication(), "Floating Action Button second", Toast.LENGTH_SHORT).show();

                                        sPattern.setIngredientType("Beef");
                                        sPattern.setCaloriesBudgetIndex(0);

                                        sPattern.setMaxCalorie("All recipes");


                                        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
                                        View layout1 = inflater.inflate(org.contentarcade.apps.meditranianrecipes.R.layout.dialog_layout, (ViewGroup) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.root));
//                LinearLayout ll = (LinearLayout)layout1.findViewById(R.id.placeholder);
//                ll.addView(rangeSeekBar);
                                        Button search = (Button) layout1.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.searchButton);


//               final TextView tv = (TextView) layout1.findViewById(R.id.);
                                        NumberPicker np = (NumberPicker) layout1.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.np);
                                        //   NumberPicker np1 = (NumberPicker) layout1.findViewById(R.id.np1);

                                        //Set TextView text color
                                        // tv.setTextColor(Color.parseColor("#FF2C834F"));

                                        //Initializing a new string array with elements
//                                        final String[] values = {"All recipies", ">100 calories", ">200 calories", ">300 calories"
//
//                                        };
                                        final String[] values2 = {"No Maximum", "<100 calories", "<200 calories", "<300 calories", "<400 calories", "<500 calories",

                                                "<600 calories", "<700 calories", "<800 calories", "<900 calories", "<1000 calories"

                                        };

                                        String[] values = new String[getCaloriesRange("Beaf").size()];
                                        ;

                                        values = getCaloriesRange("Beaf").toArray(values);


                                        //Populate NumberPicker values from String array valuese
                                        //Set the minimum value of NumberPicker
                                        np.setMinValue(0); //from array first value
                                        //Specify the maximum value/number of NumberPicker
                                        np.setMaxValue(values.length - 1); //to array last value

                                        //Specify the NumberPicker data source as array elements
                                        np.setDisplayedValues(values);

                                        //Gets whether the selector wheel wraps when reaching the min/max value.
                                        np.setWrapSelectorWheel(true);
                                        sPattern.setMaxCalorie(values[0]);
                                        sPattern.setCaloriesBudgetIndex(0);

                                        //Set a value change listener for NumberPicker
                                        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                                            @Override
                                            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                                                //Display the newly selected value from picker
                                                //  tv.setText("Selected value : " + values[newVal]);
                                                String[] values = new String[getCaloriesRange("Beaf").size()];
                                                ;

                                                values = getCaloriesRange("Beaf").toArray(values);
                                                sPattern.setMaxCalorie(values[newVal]);
                                                sPattern.setCaloriesBudgetIndex(newVal);
                                                // Toast.makeText(getApplication(), String.valueOf(sPattern.getMaxCalorie()), Toast.LENGTH_SHORT).show();

                                            }
                                        });


                                        AlertDialog.Builder yourDialog = new AlertDialog.Builder(
                                                HomeScreen.this);

                                        yourDialog.setPositiveButton("Search", new DialogInterface.OnClickListener() {


                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                                filter(sPattern.getMaxCalorie().toString(), sPattern.getIngredientType().toString(), sPattern.getMealType().toString());


                                                if (sPattern.getFilterArray().size() == 0) {

                                                    Toast.makeText(mContext, "No Record Found", Toast.LENGTH_SHORT).show();

                                                } else {
                                                    Intent filterRresultActivityIntent = new Intent(HomeScreen.this, FilterresultActivity.class);

                                                    startActivity(filterRresultActivityIntent);

                                                }
                                            }


                                        });

                                        yourDialog.setNegativeButton("Cancel", null);

                                        yourDialog.setView(layout1);

                                        AlertDialog dialog = yourDialog.create();

                                        dialog.show();
                                    }


                                }
        );

        fab1.setOnClickListener(new View.OnClickListener()

                                {
                                    @Override
                                    public void onClick(View v) {

                                        if (totalFishRecipestxtview.getText() == "0" || totalFishRecipestxtview.getText() == "") {

                                            return;


                                        }


                                        //   Toast.makeText(getApplication(), "Floating Action Button third", Toast.LENGTH_SHORT).show();
                                        //  Toast.makeText(getApplication(), "Floating Action Button second", Toast.LENGTH_SHORT).show();

                                        sPattern.setIngredientType("Fish,Seafood");
                                        sPattern.setCaloriesBudgetIndex(0);

                                        sPattern.setMaxCalorie("All recipes");


                                        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
                                        View layout1 = inflater.inflate(org.contentarcade.apps.meditranianrecipes.R.layout.dialog_layout, (ViewGroup) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.root));

                                        Button search = (Button) layout1.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.searchButton);


//               final TextView tv = (TextView) layout1.findViewById(R.id.);
                                        NumberPicker np = (NumberPicker) layout1.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.np);
                                        //   NumberPicker np1 = (NumberPicker) layout1.findViewById(R.id.np1);

                                        //Set TextView text color
                                        // tv.setTextColor(Color.parseColor("#FF2C834F"));

                                        //Initializing a new string array with elements

                                        // {"All recipies", ">100 calories", ">200 calories", ">300 calories"};
                                        final String[] values2 = {"No Maximum", "<100 calories", "<200 calories", "<300 calories", "<400 calories", "<500 calories",

                                                "<600 calories", "<700 calories", "<800 calories", "<900 calories", "<1000 calories"

                                        };


                                        //Populate NumberPicker values from String array valuese
                                        String[] values = new String[getCaloriesRange("Fish,Seafood").size()];
                                        ;

                                        values = getCaloriesRange("Fish,Seafood").toArray(values);


                                        //Set the minimum value of NumberPicker
                                        np.setMinValue(0); //from array first value
                                        //Specify the maximum value/number of NumberPicker
                                        np.setMaxValue(values.length - 1); //to array last value

                                        //Specify the NumberPicker data source as array elements
                                        np.setDisplayedValues(values);

                                        //Gets whether the selector wheel wraps when reaching the min/max value.
                                        np.setWrapSelectorWheel(true);
                                        sPattern.setMaxCalorie(values[0]);
                                        sPattern.setCaloriesBudgetIndex(0);

                                        //Set a value change listener for NumberPicker
                                        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                                            @Override
                                            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                                                //Display the newly selected value from picker
                                                //  tv.setText("Selected value : " + values[newVal]);
                                                String[] values = new String[getCaloriesRange("Fish,Seafood").size()];
                                                ;

                                                values = getCaloriesRange("Fish,Seafood").toArray(values);
                                                sPattern.setMaxCalorie(values[newVal]);
                                                sPattern.setCaloriesBudgetIndex(newVal);
                                                // Toast.makeText(getApplication(), String.valueOf(sPattern.getMaxCalorie()), Toast.LENGTH_SHORT).show();

                                            }
                                        });


                                        AlertDialog.Builder yourDialog = new AlertDialog.Builder(
                                                HomeScreen.this);

                                        yourDialog.setPositiveButton("Search", new DialogInterface.OnClickListener() {


                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                                filter(sPattern.getMaxCalorie().toString(), sPattern.getIngredientType().toString(), sPattern.getMealType().toString());


                                                if (sPattern.getFilterArray().size() == 0) {

                                                    Toast.makeText(mContext, "No Record Found", Toast.LENGTH_SHORT).show();

                                                } else {
                                                    Intent filterRresultActivityIntent = new Intent(HomeScreen.this, FilterresultActivity.class);

                                                    startActivity(filterRresultActivityIntent);

                                                }
                                            }


                                        });

                                        yourDialog.setNegativeButton("Cancel", null);

                                        yourDialog.setView(layout1);

                                        AlertDialog dialog = yourDialog.create();

                                        dialog.show();
                                    }


                                }
        );


    }


  public void viewpagerandTablayoutfunctionality(){


      SingeltonPattern sPattern = SingeltonPattern.getInstance();

      viewPager=(ViewPager)

              findViewById(org.contentarcade.apps.meditranianrecipes.R.id.viewpager);


      viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener()

      {
          public void onPageScrollStateChanged(int state) {
          }

          public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
          }

          public void onPageSelected(int position) {

              // Check if this is the page you want.

              SingeltonPattern sPattern = SingeltonPattern.getInstance();


              if (position == 0) {
                  //sPattern.setMealType("Breakfast");
                  sPattern.setCurrentArray(loadJSONFromAsset());
                  Log.d("foodItems", sPattern.getCurrentArray().get(1).getRecipename());
                  //  Toast.makeText(AddMeal.this,"Chicken",Toast.LENGTH_SHORT).show();
                  setTotalIngredientRecipes(sPattern.getCurrentArray());
                  if (sPattern.isAnimationshowflag()) {

                      setGridAdapter();
                  }
              }


              if (position == 1) {

                 // sPattern.setMealType("Lunch");
                  sPattern.setCurrentArray(sPattern.getLunchItems());
                  //Toast.makeText(AddMeal.this,"Chicken",Toast.LENGTH_SHORT).show();
                  setTotalIngredientRecipes(sPattern.getCurrentArray());
                  if (sPattern.isAnimationshowflag()) {

                      setGridAdapter();
                  }
              }



//              if (position == 2) {
//                  //sPattern.setMealType("Dinner");
//                  sPattern.setCurrentArray(sPattern.getDinnerItems());
//                  setTotalIngredientRecipes(sPattern.getCurrentArray());
//                  if (sPattern.isAnimationshowflag()) {
//
//                      setGridAdapter();
//                  }
//              }
//              if (position == 3) {
//
//                  //sPattern.setMealType("Snacks");
//                  sPattern.setCurrentArray(sPattern.getDesrtItems());
//                  setTotalIngredientRecipes(sPattern.getCurrentArray());
//                  if (sPattern.isAnimationshowflag()) {
//
//                      setGridAdapter();
//                  }
//
//
//              }
//              if (position == 4) {
//
//                  //sPattern.setMealType("Sandwich");
//                  sPattern.setCurrentArray(sPattern.getSandwichItems());
//                  setTotalIngredientRecipes(sPattern.getCurrentArray());
//                  if (sPattern.isAnimationshowflag()) {
//
//                      setGridAdapter();
//                  }
//
//
//              }
//              if (position == 5) {
//
//                 // sPattern.setMealType("Appetizer");
//                  sPattern.setCurrentArray(sPattern.getAppetizeItems());
//                  setTotalIngredientRecipes(sPattern.getCurrentArray());
//                  if (sPattern.isAnimationshowflag()) {
//
//                      setGridAdapter();
//                  }
//
//
//              }
//
//
//              if (position == 6) {
//
//                  // sPattern.setMealType("Appetizer");
//                  sPattern.setCurrentArray(sPattern.getWaffleItems());
//                  setTotalIngredientRecipes(sPattern.getCurrentArray());
//                  if (sPattern.isAnimationshowflag()) {
//
//                      setGridAdapter();
//                  }
//
//
//              }
//
//
//              if (position == 7) {
//
//                  // sPattern.setMealType("Appetizer");
//                  sPattern.setCurrentArray(sPattern.getSmoothieItems());
//                  setTotalIngredientRecipes(sPattern.getCurrentArray());
//                  if (sPattern.isAnimationshowflag()) {
//
//                      setGridAdapter();
//                  }
//
//
//              }
//
//
//              if (position == 8) {
//
//                  // sPattern.setMealType("Appetizer");
//                  sPattern.setCurrentArray(sPattern.getDesrtItems());
//                  setTotalIngredientRecipes(sPattern.getCurrentArray());
//                  if (sPattern.isAnimationshowflag()) {
//
//                      setGridAdapter();
//                  }
//
//
//              }
//
//
//              if (position == 9) {
//
//                  // sPattern.setMealType("Appetizer");
//                  sPattern.setCurrentArray(sPattern.getBreadItems());
//                  setTotalIngredientRecipes(sPattern.getCurrentArray());
//                  if (sPattern.isAnimationshowflag()) {
//
//                      setGridAdapter();
//                  }
//
//
//              }
//
//              if (position == 10) {
//
//                  // sPattern.setMealType("Appetizer");
//                  sPattern.setCurrentArray(sPattern.getCookieItems());
//                  setTotalIngredientRecipes(sPattern.getCurrentArray());
//                  if (sPattern.isAnimationshowflag()) {
//
//                      setGridAdapter();
//                  }
//
//
//              }



          }
      });



      setupViewPager(viewPager);

      tabLayout = (TabLayout) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.tabs);
      tabLayout.setupWithViewPager(viewPager);



  }

    public void addfablayoutwithVersion(){


        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {

            fl= (LinearLayout)findViewById(org.contentarcade.apps.meditranianrecipes.R.id.filterfabbg);
            // fl.setVisibility(View.VISIBLE);
            LayoutInflater inflater =(LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            myView = inflater.inflate(R.layout.circle_fab_layout, null);
            myView.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));

            fl.addView(myView);
            fl.setVisibility(View.INVISIBLE);

        }
        else{
            fl= (LinearLayout)findViewById(org.contentarcade.apps.meditranianrecipes.R.id.filterfabbg);
            //  fl.setVisibility(View.VISIBLE);
            LayoutInflater inflater =(LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            myView = inflater.inflate(R.layout.circle_fab_layout, null);
            myView.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
            fl.addView(myView);
            fl.setVisibility(View.INVISIBLE);
        }


    }
    public void drawerLayoutandToolbarLayout(){

        final Toolbar toolbar = (Toolbar) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                ImageView imageView = (ImageView)toolbar.findViewById(R.id.navlogo);
                if (toolbar == null) {
                    return;
                }
                int toolbarWidth = toolbar.getWidth();
                int imageWidth = imageView.getWidth();
                imageView.setX((toolbarWidth - imageWidth) / 2);
            }
        });




        DrawerLayout drawer = (DrawerLayout) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, org.contentarcade.apps.meditranianrecipes.R.string.navigation_drawer_open, org.contentarcade.apps.meditranianrecipes.R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = (NavigationView) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);




    }

    public enum Ingredients {


        Beaf,
        Fruit,
        Vegetable,
        Chicken,
        Egg,
        Starch,
        Bread,
        Turkey,
        Fish,
        Seafood,
        Pasta
    }



    public void setTotalIngredientRecipes(ArrayList<FoodItem> food){

        int totalSeafoodrecipes = 0;
        int totalChickenrecipes = 0;
        int totalBeafrecipes = 0;
        int totalVegetablerecipes = 0;
        int totalStarchrecipes = 0;
        int totalBreadrecipes = 0;
        int totalPastarecipes = 0;
        int totalTurkyrecipes = 0;
       SingeltonPattern sPattern =  SingeltonPattern.getInstance();


        for(int i=0;i<sPattern.getCurrentArray().size();i++) {


            String ingredientType = sPattern.getCurrentArray().get(i).getIngredientType();

            if(ingredientType.contains("Fish")||ingredientType.contains("Seafood")){

                totalSeafoodrecipes = totalSeafoodrecipes+1;

            }
            if(ingredientType.contains("Turkey")){


                totalTurkyrecipes = totalTurkyrecipes+1;

            }
            if(ingredientType.contains("Chicken")||ingredientType.contains("Egg")){

                totalChickenrecipes = totalChickenrecipes +1;

            }
            if(ingredientType.contains("Fruit")||ingredientType.contains("Vegetable")){


                totalVegetablerecipes = totalVegetablerecipes +1;

            }
            if(ingredientType.contains("Beaf")){


                totalBeafrecipes = totalBeafrecipes +1;

            }
            if(ingredientType.contains("Pasta")){


                totalPastarecipes = totalPastarecipes +1;

            }
            if(ingredientType.contains("Starch")){


                totalStarchrecipes = totalStarchrecipes +1;

            }
            if(ingredientType.contains("Bread")){


                totalBreadrecipes = totalBreadrecipes +1;

            }



            sPattern.ingredientTurky = Integer.toString(totalTurkyrecipes);
          //  editJsonValue("turkey","15","bluecircle");
            sPattern.ingredientChicken = Integer.toString(totalChickenrecipes);
           // editJsonValue("chicken",Integer.toString(totalChickenrecipes),"greencircle");

            sPattern.ingredientSeafood = Integer.toString(totalSeafoodrecipes);
          //  editJsonValue("fish",Integer.toString(totalSeafoodrecipes),"yellowcircle");

            sPattern.ingredientBeef = Integer.toString(totalBeafrecipes);
          //  editJsonValue("beef",Integer.toString(totalBeafrecipes),"browncircle");
            sPattern.ingredientFruit = Integer.toString(totalVegetablerecipes);
          //  editJsonValue("fruit",Integer.toString(totalVegetablerecipes),"blackcircle");

            sPattern.ingredientBread = Integer.toString(totalBreadrecipes);
           // editJsonValue("bread",Integer.toString(totalBreadrecipes),"greencircle");

            sPattern.ingredientPasta = Integer.toString(totalPastarecipes);
          //  editJsonValue("bread",Integer.toString(totalBeafrecipes),"pinkcircle");

            sPattern.ingredientStarch = Integer.toString(totalStarchrecipes);
          //  editJsonValue("starch",Integer.toString(totalStarchrecipes),"pinkcircle");






        }





    }

    public void initializerecipeCountTextviews(){

        totalFishRecipestxtview = (TextView)findViewById(R.id.recipecounter);
        totalBeafRecipestxtview = (TextView)findViewById(R.id.recipecounterbeaf);
        totalBreadRecipestxtview = (TextView)findViewById(R.id.recipecounterbread);
        totalChickenRecipestxtview = (TextView)findViewById(R.id.recipecounterchicken);
        totalPastaRecipestxtview = (TextView)findViewById(R.id.recipecounterpasta);
        totalStarchRecipestxtview = (TextView)findViewById(R.id.recipecounterstarch);
        totalTurkyRecipestxtview = (TextView)findViewById(R.id.recipecounterturky);
        totalVegetableRecipestxtview   = (TextView)findViewById(R.id.recipecounterfruit);



    }

    public void setCounterTextValue(){

        SingeltonPattern sPattren =  SingeltonPattern.getInstance();

        totalVegetableRecipestxtview.setText(sPattren.ingredientFruit);

        totalChickenRecipestxtview.setText(sPattren.ingredientChicken);

        totalBeafRecipestxtview.setText(sPattren.ingredientBeef);

        totalTurkyRecipestxtview.setText(sPattren.ingredientTurky);

        totalBreadRecipestxtview.setText(sPattren.ingredientBread);

        totalStarchRecipestxtview.setText(sPattren.ingredientStarch);

        totalFishRecipestxtview.setText(sPattren.ingredientSeafood);

        totalPastaRecipestxtview.setText(sPattren.ingredientPasta);

    }

    public ArrayList<String> getCaloriesRange(String ingre){

        ArrayList<String> filterBudgetValue = new ArrayList<String>();
        SingeltonPattern sPattren =  SingeltonPattern.getInstance();

        for(int i=0;i<sPattern.getCurrentArray().size();i++) {

            String ingredientType = sPattern.getCurrentArray().get(i).getIngredientType();

            if (filterBudgetValue.size() == 0) {

                filterBudgetValue.add("All recipes");


            }
            else{
            if (ingre.contains(ingredientType)) {

                boolean flag = alreadyContains(filterBudgetValue, sPattern.getCurrentArray().get(i).getCaloriesBudget());
                if (flag == false) {


                    filterBudgetValue.add(sPattern.getCurrentArray().get(i).getCaloriesBudget());

                }

            }



            }


        }



                      return  filterBudgetValue;

    }

    public boolean alreadyContains(ArrayList<String> arr,String caloriesBudgetValue){


        boolean contain=false;
        for (int i=0;i<arr.size();i++){

            if (arr.get(i).contentEquals(caloriesBudgetValue)){

                contain = true;

            }



        }



        return contain;

    }

    public ArrayList<category> loadJSONFromCategoris() {
        ArrayList<category> locList = new ArrayList<>();

        SingeltonPattern sPattern = SingeltonPattern.getInstance();
        String json = null;
        try {
            InputStream is = getAssets().open("categories.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
          // Toast.makeText(this,json+"Enter",Toast.LENGTH_SHORT).show();
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        try {
            JSONObject obj = new JSONObject(json);
            JSONArray m_jArry = obj.getJSONArray("Category");




            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);

                category cat = new category();

                cat.setIcon(jo_inside.getString("Ingredient Name"));
                cat.setBg(jo_inside.getString("Background"));
                cat.setIngredientType(jo_inside.getString("Ingredient Category"));
                int totalrecipes=0;
                for(int j=0;j<sPattern.getCurrentArray().size();j++) {


                    if(jo_inside.getString("Ingredient Category").contains(sPattern.getCurrentArray().get(j).getIngredientType())){


                                totalrecipes = totalrecipes+1;

                    }
                    cat.setCounter(Integer.toString(totalrecipes));

                }









                    //Add your values in your `ArrayList` as below:

                if(totalrecipes>0) {

                    locList.add(cat);
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sPattern.setCatList(locList);
        return locList;
    }


//    public void editJsonValue(String ingredient,String count,String bg){
//
//
//       SingeltonPattern sPattern = SingeltonPattern.getInstance();
//
//        try {
//
//            ArrayList<JSONObject> m_jArry = loadJSONFromCategoris();
//
//            for (int i = 0; i < m_jArry.size(); i++) {
//                JSONObject jo_inside = m_jArry.get(i);
//
//                if(jo_inside.getString("Ingredient Name").contains(ingredient)) {
//
//
//
//
//                    //Add your values in your `ArrayList` as below:
//                   // locList.add(recipies);
//                    (m_jArry.get(i)).put("Recipe Count"+"123", count);
//                    (m_jArry.get(i)).put("Background",bg);
//
//                    sPattern.categoriesArray = m_jArry;
//
//                   Toast.makeText(this,sPattern.categoriesArray.get(1).getString("Recipe Count"),Toast.LENGTH_SHORT).show();
//
//                }
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//
//    }



    public void  setGridAdapter(){

        loadJSONFromCategoris();

        adapter = new CustomGrid(HomeScreen.this, sPattern.getCatList());
        adapter.notifyDataSetChanged();

        grid.setAdapter(adapter);
        grid.startAnimation(fade_in_bg);
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                SingeltonPattern sPattern = SingeltonPattern.getInstance();
                ingredientClickFunction(sPattern.getCatList().get(position).getIngredientType(),sPattern.getCatList().get(position).getCounter());
//                try {
//
//
//                    Toast.makeText(HomeScreen.this, "You Clicked at " , Toast.LENGTH_SHORT).show();
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }

            }
        });



    }

    public void ingredientClickFunction(final String Ingredient,String ingredientCount){


        if (ingredientCount == "0") {

            return;


        }

        sPattern.setIngredientType(Ingredient);
        sPattern.setCaloriesBudgetIndex(0);

        sPattern.setMaxCalorie("All recipes");


        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        View layout1 = inflater.inflate(org.contentarcade.apps.meditranianrecipes.R.layout.dialog_layout, (ViewGroup) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.root));

        Button search = (Button) layout1.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.searchButton);


        NumberPicker np = (NumberPicker) layout1.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.np);

        final String[] values2 = {"No Maximum", "<100 calories", "<200 calories", "<300 calories", "<400 calories", "<500 calories",

                "<600 calories", "<700 calories", "<800 calories", "<900 calories", "<1000 calories"

        };

        String[] values = new String[getCaloriesRange(Ingredient).size()];
        ;

        values = getCaloriesRange(Ingredient).toArray(values);


        //Populate NumberPicker values from String array valuese
        //Set the minimum value of NumberPicker
        np.setMinValue(0); //from array first value
        //Specify the maximum value/number of NumberPicker
        np.setMaxValue(values.length - 1); //to array last value

        //Specify the NumberPicker data source as array elements
        np.setDisplayedValues(values);

        //Gets whether the selector wheel wraps when reaching the min/max value.
        np.setWrapSelectorWheel(true);
        sPattern.setMaxCalorie(values[0]);
        sPattern.setCaloriesBudgetIndex(0);

        //Set a value change listener for NumberPicker
        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                //Display the newly selected value from picker
                //  tv.setText("Selected value : " + values[newVal]);
                String[] values = new String[getCaloriesRange(Ingredient).size()];
                ;

                values = getCaloriesRange(Ingredient).toArray(values);
                sPattern.setMaxCalorie(values[newVal]);
                sPattern.setCaloriesBudgetIndex(newVal);
                // Toast.makeText(getApplication(), String.valueOf(sPattern.getMaxCalorie()), Toast.LENGTH_SHORT).show();

            }
        });


        AlertDialog.Builder yourDialog = new AlertDialog.Builder(
                HomeScreen.this);

        yourDialog.setPositiveButton("Search", new DialogInterface.OnClickListener() {


            @Override
            public void onClick(DialogInterface dialog, int which) {

                filter(sPattern.getMaxCalorie().toString(), sPattern.getIngredientType().toString(), sPattern.getMealType().toString());


                if (sPattern.getFilterArray().size() == 0) {

                    Toast.makeText(mContext, "No Record Found", Toast.LENGTH_SHORT).show();

                } else {
                    Intent filterRresultActivityIntent = new Intent(HomeScreen.this, FilterresultActivity.class);

                    startActivity(filterRresultActivityIntent);

                }
            }


        });

        yourDialog.setNegativeButton("Cancel", null);

        yourDialog.setView(layout1);

        AlertDialog dialog = yourDialog.create();

        dialog.show();
    }

    public void moreApps(){
        final ImageView close;
        Button pro, skip;
        LayoutInflater inflater = (LayoutInflater) HomeScreen.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.more_dialoge, null);
        pro = (Button) view.findViewById(R.id.yes);
        skip = (Button) view.findViewById(R.id.no);
        dialog = new Dialog(HomeScreen.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(view);
        dialog.setCancelable(true);
        dialog.show();



        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
        pro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String appPackageName =getPackageName();
                // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://search?q=pub:Content+Arcade+Apps")));
                    // startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://search?q=pub:C.A+Apps")));
                }
                catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/developer?id=Content+Arcade+Apps")));
                    // startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/developer?id=pub:C.A+Apps")));
                }
                dialog.dismiss();
            }
        });



    }



}
