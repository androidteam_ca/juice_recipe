package org.contentarcade.apps.meditranianrecipes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by yasir on 11/15/2016.
 */
public class ShoppingListActivityAdapter extends BaseAdapter{

    private static List<ShoppingItem> listContact;

    private LayoutInflater mInflater;

    Context mContext;
    String extractedIntegers;

    public ShoppingListActivityAdapter(){


    }

    public ShoppingListActivityAdapter(Context photosFragment, List<ShoppingItem> results){
        listContact = results;
        mInflater = LayoutInflater.from(photosFragment);
        mContext = photosFragment;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return listContact.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return listContact.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return arg0;
    }


    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder;
        int i=0;
        final View result;
        if(convertView == null) {

            convertView = mInflater.inflate(org.contentarcade.apps.meditranianrecipes.R.layout.shopinglistitem, null);
            holder = new ViewHolder();
            // holder.foodName = (ImageView) convertView.findViewById(R.id.ingredient);
            holder.recipeRefrence = (TextView) convertView.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.reciperefrence);
            holder.shopingIngredient = (TextView)convertView.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.ingredientitem);
            holder.minusButton = (ImageView)convertView.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.minusbutton);


            //  imgs.recycle();

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        holder.recipeRefrence.setText(listContact.get(position).getRecipeRefrence());
        holder.shopingIngredient.setText(listContact.get(position).getShoppingItem());





//        holder.minusButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                DatabaseHandler db = new DatabaseHandler(mContext);
//
//                /**
//                 * CRUD Operations
//                 * */
//                // Inserting Contacts
//                Log.d("Insert: ", "Inserting ..");
//                db.deleteContact(listContact.get(position));
//
//
//
//            }
//        });







//        TextView textViewItemName = (TextView)
//                convertView.findViewById(R.id.foodname);
//        TextView textViewItemDesc = (TextView)
//                convertView.findViewById(R.id.fooddesc);
//        textViewItemName.setText(listContact.get(position).getName());
//
//        textViewItemDesc.setText(listContact.get(position).getDescription());
        // holder.txtphone.setText(listContact);

        return convertView;
    }

    private class ViewHolder {
       TextView shopingIngredient;
        TextView recipeRefrence;
        ImageView minusButton;



    }

    public void updateList(){
        notifyDataSetChanged();
    }

    public void swapItems(List<ShoppingItem> items) {
        this.listContact = items;
        notifyDataSetChanged();
    }



}
