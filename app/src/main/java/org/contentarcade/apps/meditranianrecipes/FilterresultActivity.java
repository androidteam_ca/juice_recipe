package org.contentarcade.apps.meditranianrecipes;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class FilterresultActivity extends AppCompatActivity {


    SingeltonPattern sPattern;
    CollapsingToolbarLayout collapsingToolbarLayout;
    Toolbar toolbar;
    ArrayList<FoodItem> foodItems;

    private RecyclerView recyclerView;
    private MoviesAdapter mAdapter;
    public Context mContext;

    ImageView iconofIngredient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filterresult);
         toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mContext = this;

        toolbar.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT >= 16) {
                    toolbar.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    toolbar.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
                toolbar.animate().translationY(-toolbar.getBottom()).setInterpolator(new AccelerateInterpolator()).start();
            }
        });

        iconofIngredient = (ImageView)findViewById(R.id.iv);

        sPattern = SingeltonPattern.getInstance();

        if(sPattern.getIngredientType()!=null) {



            collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
            collapsingToolbarLayout.setTitle(sPattern.getIngredientType().toString());


//            if (sPattern.getIngredientType().contains("Chicken") || sPattern.getIngredientType().contains("Eggs")) {
//
//                collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
//                collapsingToolbarLayout.setTitle("Chicken & Egg");
//                iconofIngredient.setImageResource(R.drawable.chickenicon);
//
//            }
//            if (sPattern.getIngredientType().contains("Fruit, Vegetable") || sPattern.getIngredientType().contentEquals("Fruit") || sPattern.getIngredientType().contentEquals("Vegetable")) {
//
//                collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
//                collapsingToolbarLayout.setTitle("Fruit & Veg");
//                iconofIngredient.setImageResource(R.drawable.smoothie1);
//
//            }
//            if (sPattern.getIngredientType().contains("Beef")) {
//
//                collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
//                collapsingToolbarLayout.setTitle("Beef");
//                iconofIngredient.setImageResource(R.drawable.cowicon);
//
//            }
//            if (sPattern.getIngredientType().contains("Fish") || sPattern.getIngredientType().contains("Seafood")) {
//
//                collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
//                collapsingToolbarLayout.setTitle("Fish & Seafood");
//                iconofIngredient.setImageResource(R.drawable.fishicon);
//
//            }

        }
         foodItems = sPattern.getFilterArray();
        Log.d("Deb smoothiesfilter", Integer.toString(sPattern.getFilterArray().size()+1) );





        mAdapter = new MoviesAdapter(this,foodItems);
        final RecyclerView  recyclerView = (RecyclerView) findViewById(R.id.recyclerView);


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);




        ImageView increaseCalories = (ImageView)findViewById(R.id.increasecalories);
        ImageView decreaseCalories = (ImageView)findViewById(R.id.decreasesecalories);
        final TextView caloriesBudget = (TextView)findViewById(R.id.caloriesfilter);



        increaseCalories.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                sPattern = SingeltonPattern.getInstance();


                if(sPattern.getCaloriesBudgetIndex()<sPattern.values.length-1) {

                    caloriesBudget.setText(sPattern.values[sPattern.getCaloriesBudgetIndex() + 1]);
                    sPattern.setCaloriesBudgetIndex(sPattern.getCaloriesBudgetIndex() + 1);
                    sPattern.setMaxCalorie(sPattern.values[sPattern.getCaloriesBudgetIndex()]);
//                  Toast.makeText(FilterresultActivity.this, Integer.toString(sPattern.caloriesBudgetIndex) + sPattern.values[sPattern.caloriesBudgetIndex + 1],
//                            Toast.LENGTH_LONG).show();
//                    Toast.makeText(FilterresultActivity.this, Integer.toString(sPattern.caloriesBudgetIndex),
//                            Toast.LENGTH_LONG).show();
                    sPattern = SingeltonPattern.getInstance();
                    filter(sPattern.getMaxCalorie(), sPattern.getIngredientType(), sPattern.getMealType());

                    foodItems = filter(sPattern.getMaxCalorie(), sPattern.getIngredientType(), sPattern.getMealType());
                    mAdapter = new MoviesAdapter(mContext,foodItems);
                    recyclerView.setAdapter(mAdapter);

                    mAdapter.notifyDataSetChanged();


                }



            }
        });

        decreaseCalories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sPattern = SingeltonPattern.getInstance();


                if(sPattern.getCaloriesBudgetIndex()>0) {

                    caloriesBudget.setText(sPattern.values[sPattern.getCaloriesBudgetIndex() - 1]);
                    sPattern.setCaloriesBudgetIndex(sPattern.getCaloriesBudgetIndex() - 1);
                    sPattern.setMaxCalorie(sPattern.values[sPattern.getCaloriesBudgetIndex()]);

//                    Toast.makeText(FilterresultActivity.this, Integer.toString(sPattern.caloriesBudgetIndex) + sPattern.values[sPattern.caloriesBudgetIndex + 1],
//                            Toast.LENGTH_LONG).show();
//                    Toast.makeText(FilterresultActivity.this, Integer.toString(sPattern.caloriesBudgetIndex),
//                            Toast.LENGTH_LONG).show();
                    sPattern = SingeltonPattern.getInstance();
                    filter(sPattern.getMaxCalorie(), sPattern.getIngredientType(), sPattern.getMealType());

                    foodItems =filter(sPattern.getMaxCalorie(), sPattern.getIngredientType(), sPattern.getMealType());
                    mAdapter = new MoviesAdapter(mContext,foodItems);
                    recyclerView.setAdapter(mAdapter);

                    mAdapter.notifyDataSetChanged();


                }

            }
        });





      //  Log.d("foodItems", Integer.toString(foodItems.size()));

        TextView caloriesFilterlabelinAppbar = (TextView)findViewById(R.id.caloriesfilter);

        caloriesFilterlabelinAppbar.setText(sPattern.getMaxCalorie());








    }

    public ArrayList<FoodItem> filter (String maxCalories,String type,String mealType) {
        ArrayList<FoodItem> filteredFood;

        ArrayList<FoodItem> filterArray;

        SingeltonPattern sPatteren = SingeltonPattern.getInstance();


//        if (sPattern.mealType.contentEquals("Lunch")) {
//
//            filteredFood = sPattern.getLunchItems();
//            filterArray = new ArrayList<FoodItem>();
//            Log.d("filterlunch", Integer.toString(filteredFood.size()));
//
//        } else if (sPattern.getMealType().contentEquals("Dinner")) {
//            filteredFood = sPattern.getDinnerItems();
//            filterArray = new ArrayList<FoodItem>();
//            Log.d("filterlunchbrek", Integer.toString(filteredFood.size()));
//        } else if (sPattern.getMealType().contentEquals("Breakfast")) {
//
//            filteredFood = sPattern.getFoodItems();
//            filterArray = new ArrayList<FoodItem>();
//            Log.d("filterbrek", Integer.toString(filteredFood.size() + 1));
//        } else {
//
//            filteredFood = sPattern.getSnackItems();
//            filterArray = new ArrayList<FoodItem>();
//            Log.d("filterbrek2", Integer.toString(filteredFood.size()));
//        }
//        if(sPattern.getIngredientType().contentEquals("Fruit, Vegetable")|| sPattern.getIngredientType().contentEquals("Fruit")||sPattern.getIngredientType().contentEquals("Vegetable")) {
//
//
//            filteredFood = loadJSONFromAsset();
//            filterArray = new ArrayList<FoodItem>();
//        }

                filteredFood = sPattern.getFilterArray();

            if (maxCalories.contentEquals("All recipies")) {

                filterArray = new ArrayList<FoodItem>();
                for (int i = 0; i < filteredFood.size(); i++) {

                    String mxCal = maxCalories;

                    if ( type.contains(filteredFood.get(i).getIngredientType())) {
                        {

                            filterArray.add(filteredFood.get(i));


                        }


                    }


                }
            }
                else {

            filterArray = new ArrayList<FoodItem>();
            for (int i = 0; i < filteredFood.size(); i++) {

                String mxCal = maxCalories;

                if (filteredFood.get(i).getCaloriesBudget().contentEquals(maxCalories) && type.contains(filteredFood.get(i).getIngredientType())) {
                    {

                        filterArray.add(filteredFood.get(i));


                    }




                }
            }

//            sPattern.setFilterArray(filterArray);
//           Log.d("foodItems", Integer.toString(filterArray.size()));


        }
        return  filterArray;
    }


    public ArrayList<FoodItem> loadJSONFromAsset() {
        ArrayList<FoodItem> locList = new ArrayList<>();
        String json = null;
        try {
            InputStream is = getAssets().open("breakfast.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        try {
            JSONObject obj = new JSONObject(json);
            JSONArray m_jArry = obj.getJSONArray("Smoothies");

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);
                FoodItem recipies = new FoodItem();
                String ingredientArray[] = jo_inside.getString("Ingredients").split("\\r?\\n");
                recipies.setIngredientsArray(ingredientArray);

                recipies.setRecipename(jo_inside.getString("Recipe Name"));
                recipies.setIngredients(jo_inside.getString("Ingredients"));
                recipies.setMethod(jo_inside.getString("Method"));
                recipies.setIngredientType(jo_inside.getString("Ingredient Type"));
                recipies.setMealType(jo_inside.getString("Meal Type"));
                recipies.setFoodType(jo_inside.getString("Food Type"));
                recipies.setServings(jo_inside.getString("No. of Servings"));
                recipies.setCaloriesBudget(jo_inside.getString("Calorie Budget"));
                recipies.setNutriInfo(jo_inside.getString("Nutritional Information"));




                //Add your values in your `ArrayList` as below:
                locList.add(recipies);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return locList;
    }


}
