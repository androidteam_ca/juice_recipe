package org.contentarcade.apps.meditranianrecipes;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.fernandodev.easyratingdialog.library.EasyRatingDialog;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class AddMeal extends AppCompatActivity implements OnClickListener {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Toolbar toolbar;
   //Add Mob
    private InterstitialAd interstitial;

    FloatingActionButton fab;
    FloatingActionButton fab1;
    FloatingActionButton fab2;
    FloatingActionButton fab3;
    FloatingActionButton fab4;
    FloatingActionButton fab5;
    FloatingActionButton fab6;
    FloatingActionButton fab7;
    FloatingActionButton fab8;



    CoordinatorLayout rootLayout;

    RelativeLayout fabContainer;
    TextView popup2;
    TextView smoothiePop;
    TextView beafPop;
    TextView fishPop;


    //Easy Rating Dialoge

    EasyRatingDialog easyRatingDialog;


    //Save the FAB's active status
    //false -> fab = close
    //true -> fab = open
    private boolean FAB_Status = false;


    SingeltonPattern sPattern;


    private boolean doubleBackToExitPressedOnce;

    //Animations
    Animation show_fab_1;
    Animation hide_fab_1;
    Animation show_fab_2;
    Animation hide_fab_2;
    Animation show_fab_3;
    Animation hide_fab_3;
    Animation show_fab_4;
    Animation hide_fab_4;
    Animation fade_in_bg;
    Animation fade_out_bg;

    Animation fab1_forkitkat_show;
    Animation fab1_forkitkat_hide;
    Animation fab2_forkitkat_show;
    Animation fab2_forkitkat_hide;
    Animation fab3_forkitkat_show;
    Animation fab3_forkitkat_hide;
    Animation fab4_forkitkat_show;
    Animation fab4_forkitkat_hide;


    boolean ratingFlag;

    Animation rotate;

    Animation fade_in;
    Animation fade_out;

    Context mContext;

    //UI References
    private EditText fromDateEtxt;
    private EditText toDateEtxt;

    private DatePickerDialog fromDatePickerDialog;
    private DatePickerDialog toDatePickerDialog;

    private SimpleDateFormat dateFormatter;



    // recipies Arrays

    ArrayList<FoodItem> filteredFood ;
    ArrayList<FoodItem> filterArray ;
    LinearLayout fl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(org.contentarcade.apps.meditranianrecipes.R.layout.activity_add_meal);
        Toolbar toolbar = (Toolbar) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.toolbar);
        setSupportActionBar(toolbar);

        MobileAds.initialize(this, getString(R.string.app_id));
       // FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        ratingFlag = true;



        fl = (LinearLayout)findViewById(org.contentarcade.apps.meditranianrecipes.R.id.filterfabbg);

    //   easyRatingDialog = new EasyRatingDialog(this);


       // addmethod();


        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

      // fl.setBackgroundColor(getResources().getColor(R.color.colorPrimary));


        sPattern = SingeltonPattern.getInstance();

        //Floating Action Buttons
        fab = (FloatingActionButton) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.fab);
        fab1 = (FloatingActionButton) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.fab_1);
        fab2 = (FloatingActionButton) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.fab_2);
        fab3 = (FloatingActionButton) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.fab_3);
        fab4 = (FloatingActionButton)findViewById(org.contentarcade.apps.meditranianrecipes.R.id.fab_4);
        fab5 = (FloatingActionButton)findViewById(org.contentarcade.apps.meditranianrecipes.R.id.fab_51);
        fab6 = (FloatingActionButton) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.fab_6);
        fab7 = (FloatingActionButton)findViewById(org.contentarcade.apps.meditranianrecipes.R.id.fab_7);
        fab8 = (FloatingActionButton)findViewById(org.contentarcade.apps.meditranianrecipes.R.id.fab_8);



        popup2 = (TextView)findViewById(org.contentarcade.apps.meditranianrecipes.R.id.pop);
        beafPop = (TextView)findViewById(org.contentarcade.apps.meditranianrecipes.R.id.beifpop);
        fishPop = (TextView)findViewById(org.contentarcade.apps.meditranianrecipes.R.id.fishpop);
        //Animations
        show_fab_1 = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fab1_show);
        hide_fab_1 = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fab1_hide);
        show_fab_2 = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fab2_show);
        hide_fab_2 = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fab2_hide);
        show_fab_3 = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fab3_show);
        hide_fab_3 = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fab3_hide);
        show_fab_4 = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fab4_show);
        hide_fab_4 = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fab4_hide);

        fab1_forkitkat_show = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fab1_forkitkat_animation_show);
        fab1_forkitkat_hide = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fab1_forkitkat_animation_hide);
        fab2_forkitkat_show = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fab2_forkitkat_animation_show);
        fab2_forkitkat_hide = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fab2_forkitkat_animation_hide);
        fab3_forkitkat_hide = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fab3_forkitkat_animation_hide);
        fab3_forkitkat_show = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fab3_forkitkat_animation_show);
        fab4_forkitkat_hide = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fab4_forkitkat_animation_hide);
        fab4_forkitkat_show = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fab4_forkitkat_animation_show);

        fade_in = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fade_in);
        fade_out = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fade_out);
        fade_in_bg = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fade_in_bg);
        fade_out_bg = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.fade_out_bg);
        rotate = AnimationUtils.loadAnimation(getApplication(), org.contentarcade.apps.meditranianrecipes.R.anim.rotate);

        fl = (LinearLayout)findViewById(org.contentarcade.apps.meditranianrecipes.R.id.filterfabbg);

        fl.setVisibility(View.INVISIBLE);

        fl.setOnClickListener(new View.OnClickListener() {
                                  @Override
                                  public void onClick(View v) {

                                      if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {


                                          hideFABlessthan1260();
                                          FAB_Status = false;
                                          fl = (LinearLayout) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.filterfabbg);
                                          fl.setVisibility(View.INVISIBLE);
                                          fl.setClickable(false);

                                      } else {
                                          hideFAB();
//                FrameLayout fl = (FrameLayout)findViewById(R.id.filterfabbg);
//                fl.setVisibility(View.INVISIBLE);
                                          FAB_Status = false;
                                          fl = (LinearLayout) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.filterfabbg);
                                          fl.setVisibility(View.INVISIBLE);
                                          fl.setClickable(false);

                                      }
                                  }
                              }
        );



        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
//            CoordinatorLayout.LayoutParams p = (CoordinatorLayout.LayoutParams) fab.getLayoutParams();
//            p.setMargins(0, 0, 0, 0); // get rid of margins since shadow area is now the margin
//            fab.setLayoutParams(p);
//            FrameLayout.LayoutParams p2 = (FrameLayout.LayoutParams) fab2.getLayoutParams();
//            p2.setMargins(0, 0, 0, 0); // get rid of margins since shadow area is now the margin
//            fab2.setLayoutParams(p2);
//            FrameLayout.LayoutParams p3 = (FrameLayout.LayoutParams) fab3.getLayoutParams();
//            p2.setMargins(0, 0, 0, 0); // get rid of margins since shadow area is now the margin
//            fab3.setLayoutParams(p3);
//            FrameLayout.LayoutParams p4 = (FrameLayout.LayoutParams) fab4.getLayoutParams();
//            p2.setMargins(0, 0, 0, 0); // get rid of margins since shadow area is now the margin
//            fab4.setLayoutParams(p4);
//            FrameLayout.LayoutParams p1 = (FrameLayout.LayoutParams) fab1.getLayoutParams();
//            p1.rightMargin = p1.rightMargin-20;// get rid of margins since shadow area is now the margin
//            fab1.setLayoutParams(p1);

//            FrameLayout.LayoutParams beafpopmargin = (FrameLayout.LayoutParams) beafPop.getLayoutParams();
//            beafpopmargin.rightMargin = beafpopmargin.rightMargin/second; // get rid of margins since shadow area is now the margin
//            beafPop.setLayoutParams(beafpopmargin);





        }

        mContext = this;

        sPattern.setAnimationshowflag(false);


        fab.setOnClickListener(new View.OnClickListener() {
                                   @Override
                                   public void onClick(View view) {

                                       sPattern = SingeltonPattern.getInstance();

//                fl = (FrameLayout)findViewById(R.id.filterfabbg);
//
//                fl.setVisibility(View.VISIBLE);

                                       DisplayMetrics metrics;
                                       int width = 0;
                                       int height = 0;

                                       metrics = new DisplayMetrics();

                                       getWindowManager().getDefaultDisplay().getMetrics(metrics);

                                       height = metrics.heightPixels;
                                       width = metrics.widthPixels;


                                       if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {

                                           if (FAB_Status == false) {
                                               //Display FAB menu
                                               fl = (LinearLayout) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.filterfabbg);
                                               fl.setVisibility(View.VISIBLE);
                                               expandFABlessthan1260();

                                               //
                                               fl.setClickable(true);

                                               FAB_Status = true;

                                           } else {
                                               //Close FAB menu

                                               hideFABlessthan1260();


                                               FAB_Status = false;


                                           }

                                       } else {

                                           if (FAB_Status == false) {
                                               //Display FAB menu
                                               fl = (LinearLayout) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.filterfabbg);
                                               fl.setVisibility(View.VISIBLE);
                                               expandFAB();


                                               //
                                               fl.setClickable(true);

                                               FAB_Status = true;

                                           } else {
                                               //Close FAB menu

                                               hideFAB();


                                               FAB_Status = false;


                                           }
                                       }


                                       // Check if this is the page you want.
                                       if (viewPager.getCurrentItem() == 1) {

                                           sPattern.setMealType("Lunch");
                                           sPattern.setCurrentArray(sPattern.getLunchItems());

                                           //Toast.makeText(AddMeal.this,"Chicken",Toast.LENGTH_SHORT).show();

                                       }
                                       if (viewPager.getCurrentItem() == 0) {

                                           sPattern.setMealType("Breakfast");
                                           sPattern.setCurrentArray(loadJSONFromAsset());
                                           Log.d("foodItems", sPattern.getCurrentArray().get(1).getRecipename());
                                           //  Toast.makeText(AddMeal.this,"Chicken",Toast.LENGTH_SHORT).show();


                                       }

                                       if (viewPager.getCurrentItem() == 2) {

                                           sPattern.setMealType("Dinner");
                                           sPattern.setCurrentArray(sPattern.getDinnerItems());


                                       }
                                       if (viewPager.getCurrentItem() == 3) {

                                           sPattern.setMealType("Snacks");
                                           sPattern.setCurrentArray(sPattern.getSnackItems());


                                       }


                                   }
                               }

        );

            fab4.setOnClickListener(new View.OnClickListener()

            {
                @Override
                public void onClick (View v){
               //
               // Toast.makeText(getApplication(), "Floating Action Button first", Toast.LENGTH_SHORT).show();
                // Setup the new range seek bar
             //   final RangeSeekBar<Integer> rangeSeekBar = new RangeSeekBar<Integer>(AddMeal.this);


                    sPattern.setCaloriesBudgetIndex(0);

                    sPattern.setMaxCalorie("All recipies");

                    // Set the range
//                rangeSeekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {
//                    @Override
//                    public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer minValue, Integer maxValue) {
//                        // handle changed range values
//                        // Log.i(TAG, "User selected new range values: MIN=" + minValue + ", MAX=" + maxValue);
//
//                        Toast.makeText(AddMeal.this, "User selected new range values: MIN=" + minValue + ", MAX=" + maxValue,
//                                Toast.LENGTH_LONG).show();
//                        sPattern.setMinCalories(minValue.toString());
//                        sPattern.setMaxCalorie(maxValue.toString());
//
//
//                    }
//
//
//
//
//
//
//                });


//                rangeSeekBar.setRangeValues(50, 2000);
//                rangeSeekBar.setSelectedMinValue(50);
//                rangeSeekBar.setSelectedMaxValue(500);

                //  sPattern.setRecipieType("Beverage - Smoothie");
                sPattern.setIngredientType("Fruit, Vegetable");


                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
                View layout1 = inflater.inflate(org.contentarcade.apps.meditranianrecipes.R.layout.dialog_layout, (ViewGroup) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.root));
//                LinearLayout ll = (LinearLayout)layout1.findViewById(R.id.placeholder);
//                ll.addView(rangeSeekBar);

                Button search = (Button) layout1.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.searchButton);


//               final TextView tv = (TextView) layout1.findViewById(R.id.);
                NumberPicker np = (NumberPicker) layout1.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.np);
                //   NumberPicker np1 = (NumberPicker) layout1.findViewById(R.id.np1);

                //Set TextView text color
                // tv.setTextColor(Color.parseColor("#FF2C834F"));

                //Initializing a new string array with elements
                final String[] values = {"All recipies", ">100 calories", ">200 calories"

                };
                final String[] values2 = {"No Maximum", "<100 calories", "<200 calories", "<300 calories", "<400 calories", "<500 calories",

                        "<600 calories", "<700 calories", "<800 calories", "<900 calories", "<1000 calories"

                };


                //Populate NumberPicker values from String array values
                //Set the minimum value of NumberPicker
                np.setMinValue(0); //from array first value
                //Specify the maximum value/number of NumberPicker
                np.setMaxValue(values.length - 1); //to array last value

                //Specify the NumberPicker data source as array elements
                np.setDisplayedValues(values);

                //Gets whether the selector wheel wraps when reaching the min/max value.
                np.setWrapSelectorWheel(true);

                //Set a value change listener for NumberPicker
                np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                        //Display the newly selected value from picker
                        //  tv.setText("Selected value : " + values[newVal]);
                        sPattern.setMaxCalorie(values[newVal]);
                        sPattern.setCaloriesBudgetIndex(newVal);
                        //   Toast.makeText(getApplication(), String.valueOf(sPattern.getMaxCalorie()), Toast.LENGTH_SHORT).show();

                    }
                });

           /*     Button searchBtn = (Button) layout1.findViewById(R.id.searchbutton);

                sPattern.setMaxCalorie("All recipies");

                searchBtn.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        filter(sPattern.getMaxCalorie().toString(), sPattern.getIngredientType().toString(), sPattern.getMealType().toString());


                        if (sPattern.getFilterArray().size() == 0) {

                            Toast.makeText(mContext, "No Record Found", Toast.LENGTH_SHORT).show();

                        } else {
                            Intent filterRresultActivityIntent = new Intent(AddMeal.this, FilterresultActivity.class);

                            startActivity(filterRresultActivityIntent);

                        }


                    }
                });
                */


                    AlertDialog.Builder yourDialog = new AlertDialog.Builder(
                            AddMeal.this);

                    yourDialog.setPositiveButton("Search" ,new DialogInterface.OnClickListener(){


                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            filter(sPattern.getMaxCalorie().toString(), sPattern.getIngredientType().toString(), sPattern.getMealType().toString());


                            if (sPattern.getFilterArray().size() == 0) {

                                Toast.makeText(mContext, "No Record Found", Toast.LENGTH_SHORT).show();

                            } else {
                                Intent filterRresultActivityIntent = new Intent(AddMeal.this, FilterresultActivity.class);

                                startActivity(filterRresultActivityIntent);

                            }

                        }


                    });

                    yourDialog.setNegativeButton("Cancel",null);

                    yourDialog.setView(layout1);

                    AlertDialog dialog = yourDialog.create();

                    dialog.show();







            }
            }

            );

            fab2.setOnClickListener(new View.OnClickListener()

            {
                @Override
                public void onClick (View v){
               // Toast.makeText(getApplication(), "Floating Action Button second", Toast.LENGTH_SHORT).show();

                    sPattern.setIngredientType("Chicken&Egg");

                    sPattern.setRecipieType("Beverage - Smoothie");

                sPattern.setCaloriesBudgetIndex(0);

                    sPattern.setMaxCalorie("All recipies");


                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
                View layout1 = inflater.inflate(org.contentarcade.apps.meditranianrecipes.R.layout.dialog_layout, (ViewGroup) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.root));
//                LinearLayout ll = (LinearLayout)layout1.findViewById(R.id.placeholder);
//                ll.addView(rangeSeekBar);
                Button search = (Button) layout1.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.searchButton);


//               final TextView tv = (TextView) layout1.findViewById(R.id.);
                NumberPicker np = (NumberPicker) layout1.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.np);
                //   NumberPicker np1 = (NumberPicker) layout1.findViewById(R.id.np1);

                //Set TextView text color
                // tv.setTextColor(Color.parseColor("#FF2C834F"));

                //Initializing a new string array with elements
                final String[] values = {"All recipies", ">100 calories", ">200 calories", ">300 calories", ">400 calories"

                };
                final String[] values2 = {"No Maximum", "<100 calories", "<200 calories", "<300 calories", "<400 calories", "<500 calories",

                        "<600 calories", "<700 calories", "<800 calories", "<900 calories", "<1000 calories"

                };


                //Populate NumberPicker values from String array values
                //Set the minimum value of NumberPicker
                np.setMinValue(0); //from array first value
                //Specify the maximum value/number of NumberPicker
                np.setMaxValue(values.length - 1); //to array last value

                //Specify the NumberPicker data source as array elements
                np.setDisplayedValues(values);

                //Gets whether the selector wheel wraps when reaching the min/max value.
                np.setWrapSelectorWheel(true);

                //Set a value change listener for NumberPicker
                np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                        //Display the newly selected value from picker
                        //  tv.setText("Selected value : " + values[newVal]);
                        sPattern.setMaxCalorie(values[newVal]);
                        sPattern.setCaloriesBudgetIndex(newVal);
                       // Toast.makeText(getApplication(), String.valueOf(sPattern.getMaxCalorie()), Toast.LENGTH_SHORT).show();

                    }
                });

               /* Button searchBtn = (Button) layout1.findViewById(R.id.searchbutton);

                searchBtn.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        // yourDialog.dismiss();

                        filter(sPattern.getMaxCalorie().toString(), sPattern.getIngredientType().toString(), sPattern.getMealType().toString());


                        if(sPattern.getFilterArray().size()==0){

                            Toast.makeText(mContext,"No Record Found",Toast.LENGTH_SHORT).show();

                        }
                        else {
                            Intent filterRresultActivityIntent = new Intent(AddMeal.this, FilterresultActivity.class);

                            startActivity(filterRresultActivityIntent);

                        }


                    }
                });*/

/*
                np1.setMinValue(0); //from array first value
                //Specify the maximum value/number of NumberPicker
                np1.setMaxValue(values2.length-first); //to array last value

                //Specify the NumberPicker data source as array elements
                np1.setDisplayedValues(values2);

                //Gets whether the selector wheel wraps when reaching the min/max value.
                np1.setWrapSelectorWheel(true);

                //Set a value change listener for NumberPicker
                np1.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal){
                        //Display the newly selected value from picker
                        //  tv.setText("Selected value : " + values[newVal]);
                    }
                });
                */


//                // Add to layout
//                LinearLayout layout = (LinearLayout) findViewById(R.id.seekbar_placeholder);
//                layout.addView(rangeSeekBar);





                    AlertDialog.Builder yourDialog = new AlertDialog.Builder(
                            AddMeal.this);

                    yourDialog.setPositiveButton("Search" ,new DialogInterface.OnClickListener(){


                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            filter(sPattern.getMaxCalorie().toString(), sPattern.getIngredientType().toString(), sPattern.getMealType().toString());


                            if(sPattern.getFilterArray().size()==0){

                                Toast.makeText(mContext,"No Record Found",Toast.LENGTH_SHORT).show();

                            }
                            else {
                                Intent filterRresultActivityIntent = new Intent(AddMeal.this, FilterresultActivity.class);

                                startActivity(filterRresultActivityIntent);

                            }
                        }


                    });

                    yourDialog.setNegativeButton("Cancel",null);

                    yourDialog.setView(layout1);

                    AlertDialog dialog = yourDialog.create();

                    dialog.show();
            }


            }

            );

            fab3.setOnClickListener(new View.OnClickListener()

            {
                @Override
                public void onClick (View v){


             //   Toast.makeText(getApplication(), "Floating Action Button third", Toast.LENGTH_SHORT).show();
              //  Toast.makeText(getApplication(), "Floating Action Button second", Toast.LENGTH_SHORT).show();

                sPattern.setIngredientType("Beef");
                    sPattern.setCaloriesBudgetIndex(0);

                    sPattern.setMaxCalorie("All recipies");


                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
                View layout1 = inflater.inflate(org.contentarcade.apps.meditranianrecipes.R.layout.dialog_layout, (ViewGroup) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.root));
//                LinearLayout ll = (LinearLayout)layout1.findViewById(R.id.placeholder);
//                ll.addView(rangeSeekBar);
                Button search = (Button) layout1.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.searchButton);


//               final TextView tv = (TextView) layout1.findViewById(R.id.);
                NumberPicker np = (NumberPicker) layout1.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.np);
                //   NumberPicker np1 = (NumberPicker) layout1.findViewById(R.id.np1);

                //Set TextView text color
                // tv.setTextColor(Color.parseColor("#FF2C834F"));

                //Initializing a new string array with elements
                final String[] values = {"All recipies", ">100 calories", ">200 calories", ">300 calories"

                };
                final String[] values2 = {"No Maximum", "<100 calories", "<200 calories", "<300 calories", "<400 calories", "<500 calories",

                        "<600 calories", "<700 calories", "<800 calories", "<900 calories", "<1000 calories"

                };


                //Populate NumberPicker values from String array valuese
                //Set the minimum value of NumberPicker
                np.setMinValue(0); //from array first value
                //Specify the maximum value/number of NumberPicker
                np.setMaxValue(values.length - 1); //to array last value

                //Specify the NumberPicker data source as array elements
                np.setDisplayedValues(values);

                //Gets whether the selector wheel wraps when reaching the min/max value.
                np.setWrapSelectorWheel(true);
                sPattern.setMaxCalorie(values[0]);
                sPattern.setCaloriesBudgetIndex(0);

                //Set a value change listener for NumberPicker
                np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                        //Display the newly selected value from picker
                        //  tv.setText("Selected value : " + values[newVal]);
                        sPattern.setMaxCalorie(values[newVal]);
                        sPattern.setCaloriesBudgetIndex(newVal);
                       // Toast.makeText(getApplication(), String.valueOf(sPattern.getMaxCalorie()), Toast.LENGTH_SHORT).show();

                    }
                });

             /*   Button searchBtn = (Button) layout1.findViewById(R.id.searchbutton);

                searchBtn.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        //  yourDialog.dismiss();

                        filter(sPattern.getMaxCalorie(), sPattern.getIngredientType().toString(), sPattern.getMealType());
                        if(sPattern.getFilterArray().size()==0){

                            Toast.makeText(mContext,"No Record Found",Toast.LENGTH_SHORT).show();

                        }
                        else {
                            Intent filterRresultActivityIntent = new Intent(AddMeal.this, FilterresultActivity.class);

                            startActivity(filterRresultActivityIntent);

                        }


                    }
                });*/

/*
                np1.setMinValue(0); //from array first value
                //Specify the maximum value/number of NumberPicker
                np1.setMaxValue(values2.length-first); //to array last value

                //Specify the NumberPicker data source as array elements
                np1.setDisplayedValues(values2);

                //Gets whether the selector wheel wraps when reaching the min/max value.
                np1.setWrapSelectorWheel(true);

                //Set a value change listener for NumberPicker
                np1.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal){
                        //Display the newly selected value from picker
                        //  tv.setText("Selected value : " + values[newVal]);
                    }
                });
                */


//                // Add to layout
//                LinearLayout layout = (LinearLayout) findViewById(R.id.seekbar_placeholder);
//                layout.addView(rangeSeekBar);


                    AlertDialog.Builder yourDialog = new AlertDialog.Builder(
                            AddMeal.this);

                    yourDialog.setPositiveButton("Search" ,new DialogInterface.OnClickListener(){


                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            filter(sPattern.getMaxCalorie().toString(), sPattern.getIngredientType().toString(), sPattern.getMealType().toString());


                            if(sPattern.getFilterArray().size()==0){

                                Toast.makeText(mContext,"No Record Found",Toast.LENGTH_SHORT).show();

                            }
                            else {
                                Intent filterRresultActivityIntent = new Intent(AddMeal.this, FilterresultActivity.class);

                                startActivity(filterRresultActivityIntent);

                            }
                        }


                    });

                    yourDialog.setNegativeButton("Cancel",null);

                    yourDialog.setView(layout1);

                    AlertDialog dialog = yourDialog.create();

                    dialog.show();
                }


            }
            );

        fab1.setOnClickListener(new View.OnClickListener()

                                {
                                    @Override
                                    public void onClick (View v){


                                        //   Toast.makeText(getApplication(), "Floating Action Button third", Toast.LENGTH_SHORT).show();
                                        //  Toast.makeText(getApplication(), "Floating Action Button second", Toast.LENGTH_SHORT).show();

                                        sPattern.setIngredientType("Fish,Seafood");
                                        sPattern.setCaloriesBudgetIndex(0);

                                        sPattern.setMaxCalorie("All recipies");


                                        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
                                        View layout1 = inflater.inflate(org.contentarcade.apps.meditranianrecipes.R.layout.dialog_layout, (ViewGroup) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.root));

                                        Button search = (Button) layout1.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.searchButton);


//               final TextView tv = (TextView) layout1.findViewById(R.id.);
                                        NumberPicker np = (NumberPicker) layout1.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.np);
                                        //   NumberPicker np1 = (NumberPicker) layout1.findViewById(R.id.np1);

                                        //Set TextView text color
                                        // tv.setTextColor(Color.parseColor("#FF2C834F"));

                                        //Initializing a new string array with elements
                                        final String[] values = {"All recipies", ">100 calories", ">200 calories", ">300 calories"

                                        };
                                        final String[] values2 = {"No Maximum", "<100 calories", "<200 calories", "<300 calories", "<400 calories", "<500 calories",

                                                "<600 calories", "<700 calories", "<800 calories", "<900 calories", "<1000 calories"

                                        };


                                        //Populate NumberPicker values from String array valuese
                                        //Set the minimum value of NumberPicker
                                        np.setMinValue(0); //from array first value
                                        //Specify the maximum value/number of NumberPicker
                                        np.setMaxValue(values.length - 1); //to array last value

                                        //Specify the NumberPicker data source as array elements
                                        np.setDisplayedValues(values);

                                        //Gets whether the selector wheel wraps when reaching the min/max value.
                                        np.setWrapSelectorWheel(true);
                                        sPattern.setMaxCalorie(values[0]);
                                        sPattern.setCaloriesBudgetIndex(0);

                                        //Set a value change listener for NumberPicker
                                        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                                            @Override
                                            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                                                //Display the newly selected value from picker
                                                //  tv.setText("Selected value : " + values[newVal]);
                                                sPattern.setMaxCalorie(values[newVal]);
                                                sPattern.setCaloriesBudgetIndex(newVal);
                                                // Toast.makeText(getApplication(), String.valueOf(sPattern.getMaxCalorie()), Toast.LENGTH_SHORT).show();

                                            }
                                        });

             /*   Button searchBtn = (Button) layout1.findViewById(R.id.searchbutton);

                searchBtn.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        //  yourDialog.dismiss();

                        filter(sPattern.getMaxCalorie(), sPattern.getIngredientType().toString(), sPattern.getMealType());
                        if(sPattern.getFilterArray().size()==0){

                            Toast.makeText(mContext,"No Record Found",Toast.LENGTH_SHORT).show();

                        }
                        else {
                            Intent filterRresultActivityIntent = new Intent(AddMeal.this, FilterresultActivity.class);

                            startActivity(filterRresultActivityIntent);

                        }


                    }
                });*/

/*
                np1.setMinValue(0); //from array first value
                //Specify the maximum value/number of NumberPicker
                np1.setMaxValue(values2.length-first); //to array last value

                //Specify the NumberPicker data source as array elements
                np1.setDisplayedValues(values2);

                //Gets whether the selector wheel wraps when reaching the min/max value.
                np1.setWrapSelectorWheel(true);

                //Set a value change listener for NumberPicker
                np1.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal){
                        //Display the newly selected value from picker
                        //  tv.setText("Selected value : " + values[newVal]);
                    }
                });
                */


//                // Add to layout
//                LinearLayout layout = (LinearLayout) findViewById(R.id.seekbar_placeholder);
//                layout.addView(rangeSeekBar);


                                        AlertDialog.Builder yourDialog = new AlertDialog.Builder(
                                                AddMeal.this);

                                        yourDialog.setPositiveButton("Search" ,new DialogInterface.OnClickListener(){


                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                                filter(sPattern.getMaxCalorie().toString(), sPattern.getIngredientType().toString(), sPattern.getMealType().toString());


                                                if(sPattern.getFilterArray().size()==0){

                                                    Toast.makeText(mContext,"No Record Found",Toast.LENGTH_SHORT).show();

                                                }
                                                else {
                                                    Intent filterRresultActivityIntent = new Intent(AddMeal.this, FilterresultActivity.class);

                                                    startActivity(filterRresultActivityIntent);

                                                }
                                            }


                                        });

                                        yourDialog.setNegativeButton("Cancel",null);

                                        yourDialog.setView(layout1);

                                        AlertDialog dialog = yourDialog.create();

                                        dialog.show();
                                    }


                                }
        );










            smoothiePop=(TextView)

            findViewById(org.contentarcade.apps.meditranianrecipes.R.id.smoothpop);


            toolbar=(Toolbar)

            findViewById(org.contentarcade.apps.meditranianrecipes.R.id.toolbar);

            setSupportActionBar(toolbar);

            getSupportActionBar()

            .

            setDisplayHomeAsUpEnabled(false);

            sPattern.setMealType("");
            viewPager=(ViewPager)

            findViewById(org.contentarcade.apps.meditranianrecipes.R.id.viewpager);


            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener()

            {
                public void onPageScrollStateChanged(int state) {
                }

                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                public void onPageSelected(int position) {

                    // Check if this is the page you want.
                    if (position == 1) {

                        sPattern.setMealType("Lunch");
                        sPattern.setCurrentArray(sPattern.getLunchItems());
                        //Toast.makeText(AddMeal.this,"Chicken",Toast.LENGTH_SHORT).show();
                    }
                    if (position == 0) {

                        sPattern.setMealType("Breakfast");
                        sPattern.setCurrentArray(loadJSONFromAsset());
                        Log.d("foodItems", sPattern.getCurrentArray().get(1).getRecipename());
                        //  Toast.makeText(AddMeal.this,"Chicken",Toast.LENGTH_SHORT).show();


                    }

                    if (position == 2) {

                        sPattern.setMealType("Dinner");
                        sPattern.setCurrentArray(sPattern.getDinnerItems());


                    }
                    if (position == 3) {

                        sPattern.setMealType("Snacks");
                        sPattern.setCurrentArray(loadJSONFromAsset());


                    }


                }
            });



        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        RateUs.app_launched(this);


        initializeFab();

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new OneFragment(), "Breakfast");
        adapter.addFragment(new LunchFragment(), "Lunch");
        adapter.addFragment(new DinnerFragment(), "Dinner");
        adapter.addFragment(new Snacks(), "Snack");
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {

        if(v == fromDateEtxt) {
            fromDatePickerDialog.show();
        } else if(v == toDateEtxt) {
            toDatePickerDialog.show();
        }

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {




            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }



    private void expandFAB() {

       if (viewPager.getCurrentItem()==0){
           sPattern.setMealType("Breakfast");
       }



         fab.startAnimation(rotate);
        fab.setImageResource(org.contentarcade.apps.meditranianrecipes.R.drawable.cancel);

//        FrameLayout fl = (FrameLayout)findViewById(R.id.filterfabbg);
//
//        fl.setBackgroundColor(getResources().getColor(R.color.filterlayoutbgcoloronshow));

        fl = (LinearLayout)findViewById(org.contentarcade.apps.meditranianrecipes.R.id.filterfabbg);
        fl.startAnimation(fade_in_bg);
        fl.setClickable(true);

        SingeltonPattern sPattern = SingeltonPattern.getInstance();

        sPattern.setAnimationshowflag(true);
        PlayGifView pGif = (PlayGifView) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.ingredientanimation);
        pGif.setImageResource(org.contentarcade.apps.meditranianrecipes.R.drawable.anim_ingredients);

/*
        //Floating Action Button third beaf
        FrameLayout.LayoutParams layoutParams3 = (FrameLayout.LayoutParams) fab3.getLayoutParams();
//        layoutParams3.rightMargin += (int) (fab3.getWidth() * 0.25);
//        layoutParams3.bottomMargin += (int) (fab3.getHeight() * first.seventh);
        layoutParams3.rightMargin += (int) (fab3.getWidth());
        layoutParams3.bottomMargin += (int) (fab3.getHeight()*fourth);
        fab3.setLayoutParams(layoutParams3);
        fab3.startAnimation(show_fab_3);
        fab3.setClickable(true);
        FrameLayout.LayoutParams layoutParams3text = (FrameLayout.LayoutParams) beafPop.getLayoutParams();
        layoutParams3text.rightMargin +=(int)((fab3.getWidth()*first.third));
        layoutParams3text.bottomMargin += (int) (fab3.getHeight()+fab3.getHeight()*fourth);
        beafPop.setLayoutParams(layoutParams3text);
        beafPop.startAnimation(fade_in);


//        //Floating Action Button fourth smoothies
//        FrameLayout.LayoutParams layoutParams4 = (FrameLayout.LayoutParams) fab4.getLayoutParams();
//        layoutParams4.rightMargin += (int) ((fab4.getWidth() * second.fourth));
//        layoutParams4.bottomMargin += (int) (fab4.getHeight() * second.9);
//        fab4.setLayoutParams(layoutParams4);
//        fab4.startAnimation(show_fab_4);
//        fab4.setClickable(true);
//        FrameLayout.LayoutParams layoutParams4text = (FrameLayout.LayoutParams) smoothiePop.getLayoutParams();
//        layoutParams4text.rightMargin +=(int)(fab4.getWidth()+(fab4.getWidth()*second.seventh));
//        layoutParams4text.bottomMargin += (int) (fab4.getHeight()+(fab4.getHeight() *second.9));
//        smoothiePop.setLayoutParams(layoutParams4text);
//        smoothiePop.startAnimation(fade_in);





        //Floating Action Button second chicken
        FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) fab2.getLayoutParams();
       layoutParams2.rightMargin += (int) (fab2.getWidth() * third.five);
        layoutParams2.bottomMargin += (int) (fab2.getHeight() * first.five);
       // layoutParams2.rightMargin += (int) (fab2.getWidth() +90);
        //layoutParams2.bottomMargin += (int) (fab2.getHeight()+60);
        fab2.setLayoutParams(layoutParams2);
        fab2.startAnimation(show_fab_2);
        fab2.setClickable(true);
        FrameLayout.LayoutParams layoutParams2text = (FrameLayout.LayoutParams) popup2.getLayoutParams();
        layoutParams2text.rightMargin +=(int)(fab2.getWidth()+(fab2.getWidth() * third.fourth) );
        layoutParams2text.bottomMargin += (int) (fab2.getHeight()/second+(fab2.getHeight() *first.seventh));
        popup2.startAnimation(fade_in);
        popup2.setLayoutParams(layoutParams2text);
       // popup2.setVisibility(View.VISIBLE);





        //Floating Action Button first fish
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) fab1.getLayoutParams();
        layoutParams.rightMargin += (int) (fab1.getWidth() * fourth.25);
        //  layoutParams.rightMargin += (int) (fab1.getWidth() * 100);
        layoutParams.bottomMargin += (int) (fab1.getHeight()*0.05 );
        //  layoutParams.bottomMargin += (int) (fab1.getHeight() * 10);
        fab1.setLayoutParams(layoutParams);

        fab1.startAnimation(show_fab_1);
        fab1.setClickable(true);
        FrameLayout.LayoutParams layoutParams1text = (FrameLayout.LayoutParams) fishPop.getLayoutParams();
        layoutParams1text.rightMargin +=(int)(fab1.getWidth()+(fab1.getWidth()*fourth.25));
        layoutParams1text.bottomMargin += (int) (fab1.getHeight()*0.second+(fab1.getHeight()/second));
        fishPop.startAnimation(fade_in);
        fishPop.setLayoutParams(layoutParams1text);
*/
    }

    private void hideFAB() {

       // FrameLayout fl = (FrameLayout)findViewById(R.id.filterfabbg);

       // fl.setBackgroundColor(getResources().getColor(R.color.filterlayoutbgcoloronhide));

        fab.startAnimation(rotate);
        fab.setImageResource(org.contentarcade.apps.meditranianrecipes.R.drawable.filter);

        fl = (LinearLayout)findViewById(org.contentarcade.apps.meditranianrecipes.R.id.filterfabbg);
        fl.startAnimation(fade_out_bg);
        fl.setClickable(false);

        SingeltonPattern sPattern = SingeltonPattern.getInstance();

        sPattern.setAnimationshowflag(false);



//        //Floating Action Button third cow icon
//        FrameLayout.LayoutParams layoutParams3 = (FrameLayout.LayoutParams) fab3.getLayoutParams();
////        layoutParams3.rightMargin -= (int) (fab3.getWidth() * 0.25);
////        layoutParams3.bottomMargin -= (int) (fab3.getHeight() * first.seventh);
//        layoutParams3.rightMargin -= (int) (fab3.getWidth() );
//        layoutParams3.bottomMargin -= (int) (fab3.getHeight() *fourth);
//        fab3.setLayoutParams(layoutParams3);
//        fab3.startAnimation(hide_fab_3);
//        fab3.setClickable(false);
//
//        beafPop.startAnimation(fade_out);
//
//        FrameLayout.LayoutParams layoutParams3text = (FrameLayout.LayoutParams) beafPop.getLayoutParams();
//        layoutParams3text.rightMargin -=(int)((fab3.getWidth()*first.third));
//        layoutParams3text.bottomMargin -= (int) (fab3.getHeight()+fab3.getHeight()*fourth);
//
//        beafPop.setLayoutParams(layoutParams3text);
//        beafPop.startAnimation(fade_out);
//
//
//
//        //Floating Action Button third smoothie
//        FrameLayout.LayoutParams layoutParams4 = (FrameLayout.LayoutParams) fab4.getLayoutParams();
////        layoutParams3.rightMargin -= (int) (fab3.getWidth() * 0.25);
////        layoutParams3.bottomMargin -= (int) (fab3.getHeight() * first.seventh);
//        layoutParams4.rightMargin -= (int) (fab4.getWidth() *second.fourth);
//        layoutParams4.bottomMargin -= (int) (fab4.getHeight() *second.9);
//        fab4.setLayoutParams(layoutParams4);
//        fab4.startAnimation(hide_fab_4);
//        fab4.setClickable(false);
//        smoothiePop.startAnimation(fade_out);
//        FrameLayout.LayoutParams layoutParams4text = (FrameLayout.LayoutParams) smoothiePop.getLayoutParams();
//        layoutParams4text.rightMargin -=(int)(fab4.getWidth()+(fab4.getWidth()*second.seventh));
//        layoutParams4text.bottomMargin -= (int) (fab4.getHeight()+(fab4.getHeight() *second.9));
//
//        smoothiePop.setLayoutParams(layoutParams4text);
//        smoothiePop.startAnimation(fade_out);
//
//
//
//        //Floating Action Button second chicken
//        FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) fab2.getLayoutParams();
////      layoutParams2.rightMargin -= (int) (fab2.getWidth() * first.five);
////      layoutParams2.bottomMargin -= (int) (fab2.getHeight() * first.five);
//        layoutParams2.rightMargin -= (int) (fab2.getWidth() *third.five);
//        layoutParams2.bottomMargin -= (int) (fab2.getHeight() *first.five);
//        fab2.setLayoutParams(layoutParams2);
//        fab2.startAnimation(hide_fab_2);
//
//        fab2.setClickable(false);
//        //  popup2.setVisibility(View.INVISIBLE);
//        popup2.startAnimation(fade_out);
//        FrameLayout.LayoutParams layoutParams2text = (FrameLayout.LayoutParams) popup2.getLayoutParams();
//        layoutParams2text.rightMargin -= (int)(fab2.getWidth()+(fab2.getWidth() * third.fourth) );
//        layoutParams2text.bottomMargin -= (int) (fab2.getHeight()/second+(fab2.getHeight() *first.seventh));
//
//        popup2.setLayoutParams(layoutParams2text);
//        popup2.startAnimation(fade_out);
//
//
//
//
//
//
//
//        // popup2.setLayoutParams(layoutParams2);
//
//        //
//
//
//
//
//        //Floating Action Button first fish
//        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) fab1.getLayoutParams();
////        layoutParams.rightMargin -= (int) (fab1.getWidth() * first.seventh);
////        layoutParams.bottomMargin -= (int) (fab1.getHeight() * 0.25);
//        layoutParams.rightMargin -= (int) (fab1.getWidth() *fourth.25);
//        layoutParams.bottomMargin -= (int) (fab1.getHeight() *0.05 );
//        fab1.setLayoutParams(layoutParams);
//        fab1.startAnimation(hide_fab_1);
//        fab1.setClickable(false);
//
//
//        fishPop.startAnimation(fade_out);
//        FrameLayout.LayoutParams layoutParams1text = (FrameLayout.LayoutParams) fishPop.getLayoutParams();
//        layoutParams1text.rightMargin -=(int)(fab1.getWidth()+(fab1.getWidth()*fourth.25));
//
//        layoutParams1text.bottomMargin -= (int) (fab1.getHeight()*0.second+(fab1.getHeight()/second));
//        fishPop.setLayoutParams(layoutParams1text);
//        fishPop.startAnimation(fade_out);


    }

    private void expandFABlessthan1260() {

        if (viewPager.getCurrentItem()==0){
            sPattern.setMealType("Breakfast");
        }





//        FrameLayout fl = (FrameLayout)findViewById(R.id.filterfabbg);
//
//        fl.setBackgroundColor(getResources().getColor(R.color.filterlayoutbgcoloronshow));

        fl = (LinearLayout)findViewById(org.contentarcade.apps.meditranianrecipes.R.id.filterfabbg);
        fl.startAnimation(fade_in_bg);
        fl.setClickable(true);

       // SingeltonPattern sPattern = SingeltonPattern.getInstance();

        sPattern.setAnimationshowflag(true);

        //Floating Action Button third beaf
        FrameLayout.LayoutParams layoutParams3 = (FrameLayout.LayoutParams) fab3.getLayoutParams();
//        layoutParams3.rightMargin += (int) (fab3.getWidth() * 0.25);
//        layoutParams3.bottomMargin += (int) (fab3.getHeight() * first.seventh);
        layoutParams3.rightMargin += (int) (0);
        layoutParams3.bottomMargin += (int) (fab3.getHeight()*2.5);
        fab3.setLayoutParams(layoutParams3);
        fab3.startAnimation(fab3_forkitkat_show);
        fab3.setClickable(true);
        FrameLayout.LayoutParams layoutParams3text = (FrameLayout.LayoutParams) beafPop.getLayoutParams();
        layoutParams3text.rightMargin +=(int)((fab3.getWidth()/2));
        layoutParams3text.bottomMargin += (int) (fab3.getHeight()*3.2);
        beafPop.setLayoutParams(layoutParams3text);
        beafPop.startAnimation(fade_in);


        //Floating Action Button fourth smoothies
        FrameLayout.LayoutParams layoutParams4 = (FrameLayout.LayoutParams) fab4.getLayoutParams();
        layoutParams4.rightMargin += (int) (fab4.getWidth() * 1.5);
        layoutParams4.bottomMargin += (int) (fab4.getHeight() * 1.9);
        fab4.setLayoutParams(layoutParams4);
        fab4.startAnimation(fab4_forkitkat_show);
        fab4.setClickable(true);
        FrameLayout.LayoutParams layoutParams4text = (FrameLayout.LayoutParams) smoothiePop.getLayoutParams();
        layoutParams4text.rightMargin +=(int)((fab4.getWidth()*2.5));
        layoutParams4text.bottomMargin += (int) (fab4.getHeight()+(fab4.getHeight() *1.7));
        smoothiePop.setLayoutParams(layoutParams4text);
        smoothiePop.startAnimation(fade_in);





        //Floating Action Button second chicken
        FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) fab2.getLayoutParams();
        layoutParams2.rightMargin += (int) (fab2.getWidth() * 2.5);
        layoutParams2.bottomMargin += (int) (fab2.getHeight() * 1);
        // layoutParams2.rightMargin += (int) (fab2.getWidth() +90);
        //layoutParams2.bottomMargin += (int) (fab2.getHeight()+60);
        fab2.setLayoutParams(layoutParams2);
        fab2.startAnimation(fab2_forkitkat_show);
        fab2.setClickable(true);
        FrameLayout.LayoutParams layoutParams2text = (FrameLayout.LayoutParams) popup2.getLayoutParams();
        layoutParams2text.rightMargin +=(int)((fab2.getWidth() * 3.3) );
        layoutParams2text.bottomMargin += (int) (fab2.getHeight()/2+(fab2.getHeight() *1));
        popup2.setLayoutParams(layoutParams2text);
        popup2.startAnimation(fade_in);

        // popup2.setVisibility(View.VISIBLE);





        //Floating Action Button first fish
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) fab1.getLayoutParams();
        layoutParams.rightMargin += (int) (fab1.getWidth() * 3);
        //  layoutParams.rightMargin += (int) (fab1.getWidth() * 100);
        layoutParams.bottomMargin += (int) (0);
        //  layoutParams.bottomMargin += (int) (fab1.getHeight() * 10);
        fab1.setLayoutParams(layoutParams);

        fab1.startAnimation(fab1_forkitkat_show);
        fab1.setClickable(true);
        FrameLayout.LayoutParams layoutParams1text = (FrameLayout.LayoutParams) fishPop.getLayoutParams();
        layoutParams1text.rightMargin +=(int)((fab1.getWidth()*3.9));
        layoutParams1text.bottomMargin += (int) (fab1.getHeight()/2);
        fishPop.startAnimation(fade_in);
        fishPop.setLayoutParams(layoutParams1text);

    }

    private void hideFABlessthan1260() {

        // FrameLayout fl = (FrameLayout)findViewById(R.id.filterfabbg);

        // fl.setBackgroundColor(getResources().getColor(R.color.filterlayoutbgcoloronhide));


        fl = (LinearLayout)findViewById(org.contentarcade.apps.meditranianrecipes.R.id.filterfabbg);
        fl.startAnimation(fade_out_bg);
        fl.setClickable(false);

       // SingeltonPattern sPattern = SingeltonPattern.getInstance();

        sPattern.setAnimationshowflag(false);



        FrameLayout.LayoutParams layoutParams3 = (FrameLayout.LayoutParams) fab3.getLayoutParams();
//        layoutParams3.rightMargin += (int) (fab3.getWidth() * 0.25);
//        layoutParams3.bottomMargin += (int) (fab3.getHeight() * first.seventh);
        layoutParams3.rightMargin -= (int) (0);
        layoutParams3.bottomMargin -= (int) (fab3.getHeight()*2.5);
        fab3.setLayoutParams(layoutParams3);
        fab3.startAnimation(fab3_forkitkat_hide);
        fab3.setClickable(true);
        FrameLayout.LayoutParams layoutParams3text = (FrameLayout.LayoutParams) beafPop.getLayoutParams();
        layoutParams3text.rightMargin -=(int)((fab3.getWidth()/2));
        layoutParams3text.bottomMargin -= (int) (fab3.getHeight()*3.2);

        beafPop.startAnimation(fade_out);
        beafPop.setLayoutParams(layoutParams3text);



        //Floating Action Button fourth smoothies
        FrameLayout.LayoutParams layoutParams4 = (FrameLayout.LayoutParams) fab4.getLayoutParams();
        layoutParams4.rightMargin -= (int) (fab4.getWidth() * 1.5);
        layoutParams4.bottomMargin -= (int) (fab4.getHeight() * 1.9);
        fab4.setLayoutParams(layoutParams4);
        fab4.startAnimation(fab4_forkitkat_hide);
        fab4.setClickable(true);
        FrameLayout.LayoutParams layoutParams4text = (FrameLayout.LayoutParams) smoothiePop.getLayoutParams();
        layoutParams4text.rightMargin -=(int)((fab4.getWidth()*2.5));
        layoutParams4text.bottomMargin -= (int) (fab4.getHeight()+(fab4.getHeight() *1.7));

        smoothiePop.startAnimation(fade_out);
        smoothiePop.setLayoutParams(layoutParams4text);







        //Floating Action Button second chicken
        FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) fab2.getLayoutParams();
        layoutParams2.rightMargin -= (int) (fab2.getWidth() * 2.5);
        layoutParams2.bottomMargin -= (int) (fab2.getHeight() * 1);
        // layoutParams2.rightMargin += (int) (fab2.getWidth() +90);
        //layoutParams2.bottomMargin += (int) (fab2.getHeight()+60);
        fab2.setLayoutParams(layoutParams2);
        fab2.startAnimation(fab2_forkitkat_hide);
        fab2.setClickable(true);
        FrameLayout.LayoutParams layoutParams2text = (FrameLayout.LayoutParams) popup2.getLayoutParams();
        layoutParams2text.rightMargin -=(int)((fab2.getWidth() * 3.3) );
        layoutParams2text.bottomMargin -= (int) (fab2.getHeight()/2+(fab2.getHeight() *1));

        popup2.setLayoutParams(layoutParams2text);
        popup2.startAnimation(fade_out);

        // popup2.setVisibility(View.VISIBLE);





        //Floating Action Button first fish
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) fab1.getLayoutParams();
        layoutParams.rightMargin -= (int) (fab1.getWidth() * 3);
        //  layoutParams.rightMargin += (int) (fab1.getWidth() * 100);
        layoutParams.bottomMargin -= (int) (0 );
        //  layoutParams.bottomMargin += (int) (fab1.getHeight() * 10);
        fab1.setLayoutParams(layoutParams);

        fab1.startAnimation(fab1_forkitkat_hide);
        fab1.setClickable(true);
        FrameLayout.LayoutParams layoutParams1text = (FrameLayout.LayoutParams) fishPop.getLayoutParams();
        layoutParams1text.rightMargin -=(int)((fab1.getWidth()*3.9));
        layoutParams1text.bottomMargin -= (int) (fab1.getHeight()/2);

        fishPop.startAnimation(fade_out);
        fishPop.setLayoutParams(layoutParams1text);


    }



    public ArrayList<FoodItem> loadJSONFromAsset() {
        ArrayList<FoodItem> locList = new ArrayList<>();
        String json = null;
        try {
            InputStream is = getAssets().open("southeren.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        try {
            JSONObject obj = new JSONObject(json);
            JSONArray m_jArry = obj.getJSONArray("Southern");

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);

                if(jo_inside.getString("Meal Type").contains("Breakfast")) {

                    FoodItem recipies = new FoodItem();

                    recipies.setRecipename(jo_inside.getString("Recipe Name"));
                    recipies.setIngredients(jo_inside.getString("Ingredients"));
                    recipies.setMethod(jo_inside.getString("Method"));
                    recipies.setIngredientType(jo_inside.getString("Ingredient Type"));
                    recipies.setMealType(jo_inside.getString("Meal Type"));
                    recipies.setFoodType(jo_inside.getString("Food Type"));
                    recipies.setServings(jo_inside.getString("No. of Servings"));
                    recipies.setCaloriesBudget(jo_inside.getString("Calorie Budget"));
                    recipies.setNutriInfo(jo_inside.getString("Nutritional Information"));


                    //Add your values in your `ArrayList` as below:
                    locList.add(recipies);

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return locList;
    }
    public void filter (String maxCalories,String type,String mealType){

        sPattern = SingeltonPattern.getInstance();





//             if (mealType.contentEquals("Lunch")) {
//
//                 filteredFood = sPattern.getLunchItems();
//                 filterArray = new ArrayList<FoodItem>();
//
//             }
//             else if (mealType.contentEquals("Dinner")) {
//                 filteredFood = sPattern.getDinnerItems();
//                 filterArray = new ArrayList<FoodItem>();
//             }
//             else if (mealType.contentEquals("Snacks")) {
//                 filteredFood = sPattern.getSnackItems();
//                 filterArray = new ArrayList<FoodItem>();
//             }
//
//             else {
//
//                 // sPattern.setMealType("Breakfast");
//                 filteredFood = sPattern.getFoodItems();
//                 filterArray = new ArrayList<FoodItem>();
//             }
//



        if (maxCalories.contentEquals("All recipies")) {

            filterArray = new ArrayList<FoodItem>();
            for (int i = 0; i < sPattern.getCurrentArray().size(); i++) {

                String mxCal = maxCalories;

                if ( type.contains(sPattern.getCurrentArray().get(i).getIngredientType())) {
                    {

                        filterArray.add(sPattern.getCurrentArray().get(i));


                    }


                }
                sPattern.setFilterArray(filterArray);

            }
        }

        else {
             filterArray = new ArrayList<FoodItem>();
             for (int i = 0; i <sPattern.getCurrentArray().size(); i++) {

//            int minCalFilter = Integer.parseInt(filteredFood.get(i).getCaloriesBudget());
//            int mnCal = Integer.parseInt(minCalories);
                 // int maxCalFilter = Integer.parseInt(filteredFood.get(i).getCaloriesBudget());
                 String mxCal = maxCalories;

                 if (sPattern.getCurrentArray().get(i).getCaloriesBudget().contentEquals(maxCalories)&&type.contains(sPattern.getCurrentArray().get(i).getIngredientType()))
                 {

                     filterArray.add(sPattern.getCurrentArray().get(i));


                 }


             }
             sPattern.setFilterArray(filterArray);
         }


      //  Toast.makeText(mContext, sPattern.getFoodItems().get(first).getRecipename(),
        //        Toast.LENGTH_LONG).show();
        //Log.d("foodItemsfilte", sPattern.getCurrentArray().get(first).getRecipename());








    }


    public static String removeWords(String word ,String remove) {
        return word.replace(remove,"");
    }



    /**
     * Exit the app if user select yes.
     */
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.drawer_layout);



        if (doubleBackToExitPressedOnce ) {
            super.onBackPressed();

        }



        if(FAB_Status == false){

            Toast.makeText(this, this.getResources().getString(org.contentarcade.apps.meditranianrecipes.R.string.exit_toast), Toast.LENGTH_SHORT).show();

            ratingFlag = false;


        }






       // if (doubleBackToExitPressedOnce == false && sPattern.isAnimationshowflag() == true) {

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {

                if(sPattern.isAnimationshowflag()){

                hideFABlessthan1260();

                    sPattern.setAnimationshowflag(false);
                  //  Toast.makeText(this, this.getResources().getString(R.string.exit_toast), Toast.LENGTH_SHORT).show();
                    FAB_Status = false;
                }

            }
          else {
                if (sPattern.isAnimationshowflag()) {

                    hideFAB();
                    sPattern.setAnimationshowflag(false);
                  //  Toast.makeText(this, this.getResources().getString(R.string.exit_toast), Toast.LENGTH_SHORT).show();
                    FAB_Status = false;


                }
            }

       this.doubleBackToExitPressedOnce = true;


        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;


            }
        }, 1000);


    }

    @Override
    protected void onStart() {
        super.onStart();
       // easyRatingDialog.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
      //  easyRatingDialog.showIfNeeded();
    }


    public void addmethod(){


        // Prepare the Interstitial Ad
        interstitial = new InterstitialAd(AddMeal.this);
        // Insert the Ad Unit ID
        interstitial.setAdUnitId("ca-app-pub-3005749278400559/9806356653");

        //Locate the Banner Ad in activity_main.xml
       // AdView adView = (AdView) this.findViewById(R.id.adView);

        // Request for Ads
        AdRequest adRequest = new AdRequest.Builder()

                // Add a test device to show Test Ads
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("D5D58A2C3DF4AD796F86B8DF57535701")
                .build();

        // Load ads into Banner Ads
       // adView.loadAd(adRequest);

        // Load ads into Interstitial Ads
        interstitial.loadAd(adRequest);

        // Prepare an Interstitial Ad Listener
        interstitial.setAdListener(new AdListener() {
            public void onAdLoaded() {
                // Call displayInterstitial() function
                displayInterstitial();
            }
        });

    }
    public void displayInterstitial() {
        // If Ads are loaded, show Interstitial else show nothing.
        if (interstitial.isLoaded()) {
            interstitial.show();
        }
    }
@Override
    public void onStop() {

        super.onStop();


    }

    public void pasta(){

        fab5.setOnClickListener(new View.OnClickListener()

                                {
                                    @Override
                                    public void onClick(View v) {


                                        //   Toast.makeText(getApplication(), "Floating Action Button third", Toast.LENGTH_SHORT).show();
                                        //  Toast.makeText(getApplication(), "Floating Action Button second", Toast.LENGTH_SHORT).show();

                                        sPattern.setIngredientType("Pasta");
                                        sPattern.setCaloriesBudgetIndex(0);

                                        sPattern.setMaxCalorie("All recipies");


                                        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
                                        View layout1 = inflater.inflate(org.contentarcade.apps.meditranianrecipes.R.layout.dialog_layout, (ViewGroup) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.root));

                                        Button search = (Button) layout1.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.searchButton);


//               final TextView tv = (TextView) layout1.findViewById(R.id.);
                                        NumberPicker np = (NumberPicker) layout1.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.np);
                                        //   NumberPicker np1 = (NumberPicker) layout1.findViewById(R.id.np1);

                                        //Set TextView text color
                                        // tv.setTextColor(Color.parseColor("#FF2C834F"));

                                        //Initializing a new string array with elements
                                        final String[] values = {"All recipies", ">100 calories", ">200 calories", ">300 calories"

                                        };
                                        final String[] values2 = {"No Maximum", "<100 calories", "<200 calories", "<300 calories", "<400 calories", "<500 calories",

                                                "<600 calories", "<700 calories", "<800 calories", "<900 calories", "<1000 calories"

                                        };


                                        //Populate NumberPicker values from String array valuese
                                        //Set the minimum value of NumberPicker
                                        np.setMinValue(0); //from array first value
                                        //Specify the maximum value/number of NumberPicker
                                        np.setMaxValue(values.length - 1); //to array last value

                                        //Specify the NumberPicker data source as array elements
                                        np.setDisplayedValues(values);

                                        //Gets whether the selector wheel wraps when reaching the min/max value.
                                        np.setWrapSelectorWheel(true);
                                        sPattern.setMaxCalorie(values[0]);
                                        sPattern.setCaloriesBudgetIndex(0);

                                        //Set a value change listener for NumberPicker
                                        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                                            @Override
                                            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                                                //Display the newly selected value from picker
                                                //  tv.setText("Selected value : " + values[newVal]);
                                                sPattern.setMaxCalorie(values[newVal]);
                                                sPattern.setCaloriesBudgetIndex(newVal);
                                                // Toast.makeText(getApplication(), String.valueOf(sPattern.getMaxCalorie()), Toast.LENGTH_SHORT).show();

                                            }
                                        });


                                        AlertDialog.Builder yourDialog = new AlertDialog.Builder(
                                                AddMeal.this);

                                        yourDialog.setPositiveButton("Search", new DialogInterface.OnClickListener() {


                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                                filter(sPattern.getMaxCalorie().toString(), sPattern.getIngredientType().toString(), sPattern.getMealType().toString());


                                                if (sPattern.getFilterArray().size() == 0) {

                                                    Toast.makeText(mContext, "No Record Found", Toast.LENGTH_SHORT).show();

                                                } else {
                                                    Intent filterRresultActivityIntent = new Intent(AddMeal.this, FilterresultActivity.class);

                                                    startActivity(filterRresultActivityIntent);

                                                }
                                            }


                                        });

                                        yourDialog.setNegativeButton("Cancel", null);

                                        yourDialog.setView(layout1);

                                        AlertDialog dialog = yourDialog.create();

                                        dialog.show();
                                    }


                                }
        );





    }


    public void starch(){

        fab8.setOnClickListener(new View.OnClickListener()

                                {
                                    @Override
                                    public void onClick(View v) {


                                        //   Toast.makeText(getApplication(), "Floating Action Button third", Toast.LENGTH_SHORT).show();
                                        //  Toast.makeText(getApplication(), "Floating Action Button second", Toast.LENGTH_SHORT).show();

                                        sPattern.setIngredientType("Starch");
                                        sPattern.setCaloriesBudgetIndex(0);

                                        sPattern.setMaxCalorie("All recipies");


                                        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
                                        View layout1 = inflater.inflate(org.contentarcade.apps.meditranianrecipes.R.layout.dialog_layout, (ViewGroup) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.root));

                                        Button search = (Button) layout1.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.searchButton);


//               final TextView tv = (TextView) layout1.findViewById(R.id.);
                                        NumberPicker np = (NumberPicker) layout1.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.np);
                                        //   NumberPicker np1 = (NumberPicker) layout1.findViewById(R.id.np1);

                                        //Set TextView text color
                                        // tv.setTextColor(Color.parseColor("#FF2C834F"));

                                        //Initializing a new string array with elements
                                        final String[] values = {"All recipies", ">100 calories", ">200 calories", ">300 calories"

                                        };
                                        final String[] values2 = {"No Maximum", "<100 calories", "<200 calories", "<300 calories", "<400 calories", "<500 calories",

                                                "<600 calories", "<700 calories", "<800 calories", "<900 calories", "<1000 calories"

                                        };


                                        //Populate NumberPicker values from String array valuese
                                        //Set the minimum value of NumberPicker
                                        np.setMinValue(0); //from array first value
                                        //Specify the maximum value/number of NumberPicker
                                        np.setMaxValue(values.length - 1); //to array last value

                                        //Specify the NumberPicker data source as array elements
                                        np.setDisplayedValues(values);

                                        //Gets whether the selector wheel wraps when reaching the min/max value.
                                        np.setWrapSelectorWheel(true);
                                        sPattern.setMaxCalorie(values[0]);
                                        sPattern.setCaloriesBudgetIndex(0);

                                        //Set a value change listener for NumberPicker
                                        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                                            @Override
                                            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                                                //Display the newly selected value from picker
                                                //  tv.setText("Selected value : " + values[newVal]);
                                                sPattern.setMaxCalorie(values[newVal]);
                                                sPattern.setCaloriesBudgetIndex(newVal);
                                                // Toast.makeText(getApplication(), String.valueOf(sPattern.getMaxCalorie()), Toast.LENGTH_SHORT).show();

                                            }
                                        });


                                        AlertDialog.Builder yourDialog = new AlertDialog.Builder(
                                                AddMeal.this);

                                        yourDialog.setPositiveButton("Search", new DialogInterface.OnClickListener() {


                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                                filter(sPattern.getMaxCalorie().toString(), sPattern.getIngredientType().toString(), sPattern.getMealType().toString());


                                                if (sPattern.getFilterArray().size() == 0) {

                                                    Toast.makeText(mContext, "No Record Found", Toast.LENGTH_SHORT).show();

                                                } else {
                                                    Intent filterRresultActivityIntent = new Intent(AddMeal.this, FilterresultActivity.class);

                                                    startActivity(filterRresultActivityIntent);

                                                }
                                            }


                                        });

                                        yourDialog.setNegativeButton("Cancel", null);

                                        yourDialog.setView(layout1);

                                        AlertDialog dialog = yourDialog.create();

                                        dialog.show();
                                    }


                                }
        );





    }

    public void bread(){

        fab7.setOnClickListener(new View.OnClickListener()

                                {
                                    @Override
                                    public void onClick (View v){


                                        //   Toast.makeText(getApplication(), "Floating Action Button third", Toast.LENGTH_SHORT).show();
                                        //  Toast.makeText(getApplication(), "Floating Action Button second", Toast.LENGTH_SHORT).show();

                                        sPattern.setIngredientType("Bread");
                                        sPattern.setCaloriesBudgetIndex(0);

                                        sPattern.setMaxCalorie("All recipies");


                                        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
                                        View layout1 = inflater.inflate(org.contentarcade.apps.meditranianrecipes.R.layout.dialog_layout, (ViewGroup) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.root));

                                        Button search = (Button) layout1.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.searchButton);


//               final TextView tv = (TextView) layout1.findViewById(R.id.);
                                        NumberPicker np = (NumberPicker) layout1.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.np);
                                        //   NumberPicker np1 = (NumberPicker) layout1.findViewById(R.id.np1);

                                        //Set TextView text color
                                        // tv.setTextColor(Color.parseColor("#FF2C834F"));

                                        //Initializing a new string array with elements
                                        final String[] values = {"All recipies", ">100 calories", ">200 calories", ">300 calories"

                                        };
                                        final String[] values2 = {"No Maximum", "<100 calories", "<200 calories", "<300 calories", "<400 calories", "<500 calories",

                                                "<600 calories", "<700 calories", "<800 calories", "<900 calories", "<1000 calories"

                                        };


                                        //Populate NumberPicker values from String array valuese
                                        //Set the minimum value of NumberPicker
                                        np.setMinValue(0); //from array first value
                                        //Specify the maximum value/number of NumberPicker
                                        np.setMaxValue(values.length - 1); //to array last value

                                        //Specify the NumberPicker data source as array elements
                                        np.setDisplayedValues(values);

                                        //Gets whether the selector wheel wraps when reaching the min/max value.
                                        np.setWrapSelectorWheel(true);
                                        sPattern.setMaxCalorie(values[0]);
                                        sPattern.setCaloriesBudgetIndex(0);

                                        //Set a value change listener for NumberPicker
                                        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                                            @Override
                                            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                                                //Display the newly selected value from picker
                                                //  tv.setText("Selected value : " + values[newVal]);
                                                sPattern.setMaxCalorie(values[newVal]);
                                                sPattern.setCaloriesBudgetIndex(newVal);
                                                // Toast.makeText(getApplication(), String.valueOf(sPattern.getMaxCalorie()), Toast.LENGTH_SHORT).show();

                                            }
                                        });


                                        AlertDialog.Builder yourDialog = new AlertDialog.Builder(
                                                AddMeal.this);

                                        yourDialog.setPositiveButton("Search" ,new DialogInterface.OnClickListener(){


                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                                filter(sPattern.getMaxCalorie().toString(), sPattern.getIngredientType().toString(), sPattern.getMealType().toString());


                                                if(sPattern.getFilterArray().size()==0){

                                                    Toast.makeText(mContext,"No Record Found",Toast.LENGTH_SHORT).show();

                                                }
                                                else {
                                                    Intent filterRresultActivityIntent = new Intent(AddMeal.this, FilterresultActivity.class);

                                                    startActivity(filterRresultActivityIntent);

                                                }
                                            }


                                        });

                                        yourDialog.setNegativeButton("Cancel",null);

                                        yourDialog.setView(layout1);

                                        AlertDialog dialog = yourDialog.create();

                                        dialog.show();
                                    }


                                }
        );



    }

    public void turkey(){

        fab6.setOnClickListener(new View.OnClickListener()

                                {
                                    @Override
                                    public void onClick (View v){


                                        //   Toast.makeText(getApplication(), "Floating Action Button third", Toast.LENGTH_SHORT).show();
                                        //  Toast.makeText(getApplication(), "Floating Action Button second", Toast.LENGTH_SHORT).show();

                                        sPattern.setIngredientType("Turkey");
                                        sPattern.setCaloriesBudgetIndex(0);

                                        sPattern.setMaxCalorie("All recipies");


                                        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
                                        View layout1 = inflater.inflate(org.contentarcade.apps.meditranianrecipes.R.layout.dialog_layout, (ViewGroup) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.root));

                                        Button search = (Button) layout1.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.searchButton);


//               final TextView tv = (TextView) layout1.findViewById(R.id.);
                                        NumberPicker np = (NumberPicker) layout1.findViewById(org.contentarcade.apps.meditranianrecipes.R.id.np);
                                        //   NumberPicker np1 = (NumberPicker) layout1.findViewById(R.id.np1);

                                        //Set TextView text color
                                        // tv.setTextColor(Color.parseColor("#FF2C834F"));

                                        //Initializing a new string array with elements
                                        final String[] values = {"All recipies", ">100 calories", ">200 calories", ">300 calories"

                                        };
                                        final String[] values2 = {"No Maximum", "<100 calories", "<200 calories", "<300 calories", "<400 calories", "<500 calories",

                                                "<600 calories", "<700 calories", "<800 calories", "<900 calories", "<1000 calories"

                                        };


                                        //Populate NumberPicker values from String array valuese
                                        //Set the minimum value of NumberPicker
                                        np.setMinValue(0); //from array first value
                                        //Specify the maximum value/number of NumberPicker
                                        np.setMaxValue(values.length - 1); //to array last value

                                        //Specify the NumberPicker data source as array elements
                                        np.setDisplayedValues(values);

                                        //Gets whether the selector wheel wraps when reaching the min/max value.
                                        np.setWrapSelectorWheel(true);
                                        sPattern.setMaxCalorie(values[0]);
                                        sPattern.setCaloriesBudgetIndex(0);

                                        //Set a value change listener for NumberPicker
                                        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                                            @Override
                                            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                                                //Display the newly selected value from picker
                                                //  tv.setText("Selected value : " + values[newVal]);
                                                sPattern.setMaxCalorie(values[newVal]);
                                                sPattern.setCaloriesBudgetIndex(newVal);
                                                // Toast.makeText(getApplication(), String.valueOf(sPattern.getMaxCalorie()), Toast.LENGTH_SHORT).show();

                                            }
                                        });


                                        AlertDialog.Builder yourDialog = new AlertDialog.Builder(
                                                AddMeal.this);

                                        yourDialog.setPositiveButton("Search" ,new DialogInterface.OnClickListener(){


                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                                filter(sPattern.getMaxCalorie().toString(), sPattern.getIngredientType().toString(), sPattern.getMealType().toString());


                                                if(sPattern.getFilterArray().size()==0){

                                                    Toast.makeText(mContext,"No Record Found",Toast.LENGTH_SHORT).show();

                                                }
                                                else {
                                                    Intent filterRresultActivityIntent = new Intent(AddMeal.this, FilterresultActivity.class);

                                                    startActivity(filterRresultActivityIntent);

                                                }
                                            }


                                        });

                                        yourDialog.setNegativeButton("Cancel",null);

                                        yourDialog.setView(layout1);

                                        AlertDialog dialog = yourDialog.create();

                                        dialog.show();
                                    }


                                }
        );



    }



    public void initializeFab(){

        pasta();
        starch();
        bread();
        turkey();



    }





}











