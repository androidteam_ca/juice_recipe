package org.contentarcade.apps.meditranianrecipes;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by mbk82 on 3/4/2017.
 */

public class BarAdapter extends BaseAdapter {

    public BarAdapter() {}


    private static ArrayList<FoodItem> listContact;

    TypedArray imgs ;

    private LayoutInflater mInflater;

    Context mContext;



    public BarAdapter(Context photosFragment, ArrayList<FoodItem> results){
        listContact = results;
        mInflater = LayoutInflater.from(photosFragment);
        mContext = photosFragment;

        imgs = mContext.getResources().obtainTypedArray(R.array.lunch_imgs);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return listContact.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return listContact.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }


    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        BarAdapter.ViewHolder holder;
        int i=0;
        final View result;
        if(convertView == null) {

            convertView = mInflater.inflate(R.layout.cardview, null);
            holder = new ViewHolder();
            // holder.foodName = (ImageView) convertView.findViewById(R.id.ingredient);
            holder.ingredient = (TextView) convertView.findViewById(R.id.ingredientvalue);
            holder.mealtype = (TextView) convertView.findViewById(R.id.mealtypevalue);
            holder.foodtype = (TextView) convertView.findViewById(R.id.foodtypevalue);
            holder.recipeImage = (ImageView) convertView.findViewById(R.id.recipieimage);
            holder.recipiename = (TextView)convertView.findViewById(R.id.titletext);
            //  String imageResource = listContact.get(1).getRecipename().toString();

            //  imgs.recycle();

            convertView.setTag(holder);
        }
        else {
            holder = (BarAdapter.ViewHolder) convertView.getTag();
            result=convertView;
        }

        holder.ingredient.setText(listContact.get(position).getIngredientType().toString());
        holder.mealtype.setText(listContact.get(position).getMealType().toString());
        holder.foodtype.setText(listContact.get(position).getFoodType().toString());
        holder.recipiename.setText(listContact.get(position).getRecipename().toString());
        // Picasso.with(mContext).load(imgs.getResourceId(0,-first)).into(holder.recipeImage);
        // holder.recipeImage.setImageResource(imgs.getResourceId(0, -first));

        holder.recipeImage.setScaleType(ImageView.ScaleType.FIT_XY);
        Picasso.with(mContext).load("file:///android_asset/southeren/"+listContact.get(position).getRecipename()+".jpg").into(holder.recipeImage);



//        if (listContact.get(position).getIngredientType().toString().contentEquals("Beef")) {
//
//            holder.recipeImage.setScaleType(ImageView.ScaleType.FIT_XY);
//            Picasso.with(mContext).load("file:///android_asset/beaf/" + listContact.get(position).getRecipename().toString() + ".jpg").into(holder.recipeImage);
//
//        }
//        if (listContact.get(position).getIngredientType().toString().contentEquals("Chicken")) {
//            holder.recipeImage.setScaleType(ImageView.ScaleType.FIT_XY);
//            Picasso.with(mContext).load("file:///android_asset/chicken/" + listContact.get(position).getRecipename().toString() + ".jpg").into(holder.recipeImage);
//        }
//        if (listContact.get(position).getIngredientType().toString().contentEquals("Fish")) {
//            holder.recipeImage.setScaleType(ImageView.ScaleType.FIT_XY);
//            Picasso.with(mContext).load("file:///android_asset/fish/" + listContact.get(position).getRecipename().toString() + ".jpg").into(holder.recipeImage);
//        }




        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FoodItem food= listContact.get(position);
                Intent i = new Intent(mContext, Main2Activity.class);
                i.putExtra("recipie",food);
                mContext.startActivity(i);
//                Toast.makeText(mContext, listContact.get(position).getIngredientType().toString(), Toast.LENGTH_SHORT).show();
//                Toast.makeText(mContext, Integer.toString(position), Toast.LENGTH_SHORT).show();


            }
        });







//        TextView textViewItemName = (TextView)
//                convertView.findViewById(R.id.foodname);
//        TextView textViewItemDesc = (TextView)
//                convertView.findViewById(R.id.fooddesc);
//        textViewItemName.setText(listContact.get(position).getName());
//
//        textViewItemDesc.setText(listContact.get(position).getDescription());
        // holder.txtphone.setText(listContact);

        return convertView;
    }

    private class ViewHolder {
        ImageView recipeImage;
        TextView ingredient;
        TextView mealtype;
        TextView foodtype;
        TextView recipiename;



    }

    public void updateList(){
        notifyDataSetChanged();
    }
}
