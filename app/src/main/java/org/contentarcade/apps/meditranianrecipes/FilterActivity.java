package org.contentarcade.apps.meditranianrecipes;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;


public class FilterActivity extends AppCompatActivity {

        public static final String EXTRA_IMAGE = "DetailActivity:image";

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            ImageView image = (ImageView) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.image);
            ViewCompat.setTransitionName(image, EXTRA_IMAGE);
          //  Picasso.with(this).load(getIntent().getStringExtra(EXTRA_IMAGE)).into(image);
        }




        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(org.contentarcade.apps.meditranianrecipes.R.menu.main, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();
//            if (id == R.id.action_settings) {
//                return true;
//            }
            return super.onOptionsItemSelected(item);
        }


    }
