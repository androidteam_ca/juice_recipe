package org.contentarcade.apps.meditranianrecipes;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class Splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
//        Toolbar toolbar = (Toolbar) findViewById(org.contentarcade.apps.southerenrecipes.R.id.toolbar);
//        setSupportActionBar(toolbar);




        Thread background = new Thread() {
            public void run() {

                try {
                    // Thread will sleep for five seconds
                    sleep(5*1000);

                    // After five seconds redirect to another intent
                    Intent i=new Intent(getBaseContext(),HomeScreen.class);
                    startActivity(i);

                    //Remove activity
                    finish();

                } catch (Exception e) {

                }
            }
        };


        background.start();

    }

}
