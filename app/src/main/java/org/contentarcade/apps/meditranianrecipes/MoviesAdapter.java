package org.contentarcade.apps.meditranianrecipes;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MyViewHolder>  {

    private List<FoodItem> FoodArray;
    RecyclerOnItemClickListener mItemClickListener;
    boolean AD_TYPE;
    boolean CONTENT_TYPE ;

    Context mContext;


    // views


    public class MyViewHolder extends RecyclerView.ViewHolder  {
        public TextView title, year, genre;
        public    TextView ingredient;
        public    TextView mealtype;
        public    TextView foodtype;
        public    ImageView recipieImage;
        TextView recipiename;
        public MyViewHolder(View view) {
            super(view);


            ingredient = (TextView) view.findViewById(R.id.ingredientvalue);
            mealtype = (TextView) view.findViewById(R.id.mealtypevalue);
            foodtype = (TextView) view.findViewById(R.id.foodtypevalue);
            recipieImage = (ImageView)view.findViewById(R.id.recipieimage);
            recipiename = (TextView)view.findViewById(R.id.titletext);

        }


    }


    public MoviesAdapter(Context inContext,List<FoodItem> moviesList) {




        this.FoodArray = moviesList;
        mContext = inContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        SingeltonPattern sPattern = SingeltonPattern.getInstance();
        View itemView ;
//        if (viewType == first){
//            itemView = LayoutInflater.from(parent.getContext())
//                    .inflate(R.layout.list_fruit, parent, false);
//            NativeExpressAdView adView = (NativeExpressAdView)itemView.findViewById(R.id.adView);
//            adView.loadAd(new AdRequest.Builder().build());
//
////        AdView adView = (AdView) itemView.findViewById(R.id.adView);
////        itemView = adView;
////        // Request for Ads
//        AdRequest adRequest = new AdRequest.Builder()
//
//                // Add a test device to show Test Ads
//                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
//                .addTestDevice("D5D58A2C3DF4AD796F86B8DF57535701")
//                .build();
//
//        // Load ads into Banner Ads
//        adView.loadAd(adRequest);
//        return new MyViewHolder(itemView);
//    }
//
//       else{


               itemView = LayoutInflater.from(parent.getContext())
                       .inflate(R.layout.cardview, parent, false);
               return new MyViewHolder(itemView);


    //   }



    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {


       //   if(holder.getItemViewType()==second) {
              FoodItem food = FoodArray.get(position);
              holder.ingredient.setText(food.getIngredientType());
              holder.mealtype.setText(food.getMealType());
              holder.foodtype.setText(food.getFoodType());
              holder.recipiename.setText(food.getRecipename().toString());
        holder.recipieImage.setScaleType(ImageView.ScaleType.FIT_XY);
        Picasso.with(mContext).load("file:///android_asset/southeren/" + food.getRecipename().toString() + ".jpg").into(holder.recipieImage);


//        if (food.getIngredientType().toString().contentEquals("Beef")) {
//
//                  holder.recipieImage.setScaleType(ImageView.ScaleType.FIT_XY);
//                  Picasso.with(mContext).load("file:///android_asset/beaf/" + food.getRecipename().toString() + ".jpg").into(holder.recipieImage);
//
//              }
//              if (food.getIngredientType().toString().contentEquals("Chicken")) {
//                  holder.recipieImage.setScaleType(ImageView.ScaleType.FIT_XY);
//                  Picasso.with(mContext).load("file:///android_asset/chicken/" + food.getRecipename().toString() + ".jpg").into(holder.recipieImage);
//              }
//
//              if (food.getIngredientType().contains("Fruit") || food.getIngredientType().contains("Vegetable")) {
//                  holder.recipieImage.setScaleType(ImageView.ScaleType.FIT_XY);
//                  Picasso.with(mContext).load("file:///android_asset/smoothies/" + food.getRecipename().toString() + ".jpg").into(holder.recipieImage);
//              }
//              if (food.getIngredientType().toString().contentEquals("Fish")) {
//                  holder.recipieImage.setScaleType(ImageView.ScaleType.FIT_XY);
//                  Picasso.with(mContext).load("file:///android_asset/fish/" + food.getRecipename().toString() + ".jpg").into(holder.recipieImage);
//
//
//              }


              holder.recipieImage.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                      FoodItem food = FoodArray.get(position);
                      Intent i = new Intent(mContext, Main2Activity.class);
                      i.putExtra("recipie", food);
                      mContext.startActivity(i);
                  }
              });

        //  }


    }

    @Override
    public int getItemCount() {

      int increaseArrayWith = FoodArray.size()/5;
        return FoodArray.size();
    }


    public interface RecyclerOnItemClickListener {
        /**
         * Called when an item is clicked.
         *
         * @param childView View of the item that was clicked.
         * @param position  Position of the item that was clicked.
         */
        public void onItemClick(View childView, int position);
    }

    @Override
    public int getItemViewType(int position)
    {
        if(position>0) {
            if (position % 5 == 0) {
                AD_TYPE = true;
                return 1;
            }
        }


            return 2;

    }

}