package org.contentarcade.apps.meditranianrecipes;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by yasir on 10/13/2016.
 */
public class twoFragment extends Fragment {

    public twoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(org.contentarcade.apps.meditranianrecipes.R.layout.fragment_one, container, false);
    }


    /**
     * Created by yasir on 10/24/2016.
     */
    public static class LunchFragment extends Fragment {
    }
}
