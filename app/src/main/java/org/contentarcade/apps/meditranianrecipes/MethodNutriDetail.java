package org.contentarcade.apps.meditranianrecipes;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;


public class MethodNutriDetail extends AppCompatActivity {
    TextView nutritiions;
    FoodItem fItem;
    LayoutInflater inflater;
    String ingredientArray[];
    int i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_method_nutri_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fItem = (FoodItem) getIntent().getSerializableExtra("recipie");
        addmethod();
        nutritiions = (TextView)findViewById(R.id.nutritionalfacts);
        nutritiions.setText(fItem.getNutriInfo());



    }
    public void addmethod(){
        String strings =fItem.getMethod();
        String line[] = strings.split("\\r?\\n");
//        strings=strings.replaceAll("1.","");



        inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout options_layout = (LinearLayout) findViewById(R.id.methods);
        for (i=0; i<line.length; i++)
        {

            View to_add = inflater.inflate(R.layout.methoditem, options_layout, false);
            options_layout.addView(to_add);
            TextView tv= (TextView) to_add.findViewById(R.id.text);
            String Line = line[i];
            Line = Line.replaceAll((i+1)+".","");
            tv.setText(Line);

        }

    }
    @Override
    public void onBackPressed() {


//        Intent i = new Intent(MethodNutriDetail.this, Main2Activity.class);
//        i.putExtra("recipie", fItem);
//        startActivity(i);
        finish();
    }
}
