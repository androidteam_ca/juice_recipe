package org.contentarcade.apps.meditranianrecipes;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by yasir on 10/24/2016.
 */
public class LunchFragment extends Fragment {

    SingeltonPattern sPattern;
    int totalinArray;

    int j;
    public LunchFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView = inflater.inflate(R.layout.lunch_fragment, container, false);



//        Toast.makeText(getActivity(), Integer.toString(sPattern.foodItems.size()),
//                Toast.LENGTH_LONG).show();
        ArrayList<FoodItem> fillArrayLunch = new ArrayList<FoodItem>();

        fillArrayLunch = loadJSONFromAsset();

        sPattern.setFoodItems( fillArrayLunch);



        Log.d("foodItemsfromlunch", Integer.toString(fillArrayLunch.size()));


        ListView lv = (ListView)rootView.findViewById(R.id.listViewlunch);
        ArrayList<FoodItem> mixedITems = new ArrayList<FoodItem>();
        LunchAdapter mAdapter       = new LunchAdapter(getActivity(),  mixedITems);



        Log.d("foodItemsbreak", Integer.toString(fillArrayLunch.size()));





        totalinArray = 0;
        j=0;
        mAdapter       = new LunchAdapter(getActivity(), mixedITems);

        for (int i = 0; i < fillArrayLunch.size(); i++) {

            //mixedITems.add(fillArrayLunch.get(i));

            if (i % 3 == 0) {
                if(i>0) {

                    totalinArray = totalinArray + 1;
                    //  FoodItem fItem = new FoodItem();
//                    mAdapter.addSeparatorItem(fillArrayLunch.get(0));

                }
            }
            else{


                mixedITems.add(fillArrayLunch.get(j));
                j = j +1;
            }

        }

           // mAdapter.addSeparatorItem(fillArrayLunch.get(0));

            lv.setAdapter(mAdapter);




        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ListViewFoodAdapter lvfa = new ListViewFoodAdapter();
                lvfa.notifyDataSetChanged();

            }
        });

        sPattern.setLunchItems(fillArrayLunch);
        Log.d("foodItemsfromlunchsP", Integer.toString(sPattern.getLunchItems().size()));

        return rootView;

    }


    public ArrayList<FoodItem> loadJSONFromAsset() {
        ArrayList<FoodItem> locList = new ArrayList<>();
        sPattern = SingeltonPattern.getInstance();
        String json = null;
        try {
            // InputStream is = getActivity().getAssets().open("lunch.json");
            InputStream is = getResources().openRawResource(R.raw.southeren);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
            //Log.d("abc", json);


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        try {
            JSONObject obj = new JSONObject(json);

            JSONArray m_jArry = obj.getJSONArray("Southern");


            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);
                if(jo_inside.getString("Type of Diet").contains("Detox")){


                    FoodItem recipies = new FoodItem();
                    String ingredientArray[] = jo_inside.getString("Ingredients").split("\\r?\\n");
                    recipies.setIngredientsArray(ingredientArray);

                    recipies.setRecipename(jo_inside.getString("Recipe Name"));
                    recipies.setIngredients(jo_inside.getString("Ingredients"));
                    recipies.setMethod(jo_inside.getString("Method"));
                    recipies.setIngredientType(jo_inside.getString("Type of Diet"));
                    recipies.setMealType(jo_inside.getString("Meal Type"));
                    recipies.setFoodType(jo_inside.getString("Food Type"));
                    recipies.setServings(jo_inside.getString("No. of Servings"));
                    recipies.setCaloriesBudget(jo_inside.getString("Calorie Budget"));
                    recipies.setNutriInfo(jo_inside.getString("Nutritional Information"));


                    //Add your values in your `ArrayList` as below:
                    //Add your values in your `ArrayList` as below:
                    locList.add(recipies);
                }



            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sPattern.setLunchItems(locList);

        //  Toast.makeText(getActivity(),sPattern.getLunchItems().get(30).getIngredientType().toString(),Toast.LENGTH_SHORT).show();
        return locList;
    }




}
