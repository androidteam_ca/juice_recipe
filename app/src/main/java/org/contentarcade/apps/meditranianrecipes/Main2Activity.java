package org.contentarcade.apps.meditranianrecipes;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.NativeExpressAdView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main2Activity extends AppCompatActivity {

    TextView time;
    TextView servings;
    TextView calories;
    TextView ingredients;
    TextView method;

    TextView recipeName;
    TextView nutritiions;
    TextView productdetail;
    String ingredientArray[];
    LayoutInflater inflater;
    FoodItem fItem;
    ImageView textsettingicon;

    TextView ingredientCount;

    TextView calLabel;
    ImageView recipeImage;

    TextView title;

    ImageView ivb;

    Button method_btn;

    int resourceID;

    private InterstitialAd mInterstitialAd;


    ArrayList<ShoppingItem> sItemglobal;


    Context mContext;

    List Listtasks;
    ImageView gImageView;
    boolean plusFlag;
    boolean minusFlag;
    int seekbarvalue;

    Integer[] imageIDs = {
            R.drawable.first,
            R.drawable.second,
            R.drawable.third,
            R.drawable.fourth,

            R.drawable.sixth,
            R.drawable.seventh,
            R.drawable.eighth,
            R.drawable.nineth,
            R.drawable.tenth




    };
    String[] colorIDs = {
           "#0099cc",
            "#000000",
            "#7d4b4b",
            "#9fc05a",

            "#ff8b5a",
            "#ff4081",
            "#ffce00",
            "#684D91",
            "#63ffbb"




    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        MobileAds.initialize(this, getString(R.string.app_id));
        // Prepare the Interstitial Ad
        mInterstitialAd = new InterstitialAd(Main2Activity.this);
        // Insert the Ad Unit ID
        mInterstitialAd.setAdUnitId("ca-app-pub-3005749278400559/9806356653");

        //Locate the Banner Ad in activity_main.xml
        // AdView adView = (AdView) this.findViewById(R.id.adView);

        // Request for Ads
        AdRequest adRequest = new AdRequest.Builder()

                // Add a test device to show Test Ads
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("D5D58A2C3DF4AD796F86B8DF57535701")
                .build();
        mInterstitialAd.loadAd(adRequest);


//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

       fItem = (FoodItem)getIntent().getSerializableExtra("recipie");

       ingredientArray  = fItem.getNutriInfo().split("\\r?\\n");

        mContext = this;
        addIngredients(fItem);
        initializeView();
        setValues();
       productdetail.setText(fItem.getRecipename());
        setSupportActionBar(toolbar);

        seekbarvalue = 12;

//        NativeExpressAdView adView = (NativeExpressAdView)findViewById(R.id.admainView);
//        adView.loadAd(new AdRequest.Builder().build());
        method_btn=(Button) findViewById(R.id.method_btn);
        method_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main2Activity.this, MethodNutriDetail.class);
                i.putExtra("recipie", fItem);
                startActivity(i);
                showInterstitial();
            }
        });



    }
    private void showInterstitial() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Log.d("TAG", "The interstitial wasn't loaded yet.");
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
        }
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                // Load the next interstitial.
                mInterstitialAd.loadAd(new AdRequest.Builder().build());}
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();


    }

    public  void  initializeView(){

       // time = (TextView)findViewById(R.id.ingredientvalue);
        servings = (TextView)findViewById(R.id.mealtypevalue);
        calories = (TextView)findViewById(R.id.foodtypevalue);
       // ingredients = (TextView)findViewById(R.id.ingredientlist);
        method = (TextView)findViewById(R.id.method);
        recipeImage = (ImageView)findViewById(R.id.recipieimage);
        recipeName = (TextView)findViewById(R.id.titletext);
        nutritiions = (TextView)findViewById(R.id.nutritionalfacts);
        productdetail = (TextView)findViewById(R.id.product_details_title);
        textsettingicon = (ImageView)findViewById(R.id.textsettingsicon);
        textsettingicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                seekbarTextSize();
            }
        });


    }

    public void setValues(){
        SpannableStringBuilder test = new SpannableStringBuilder();
        test.append("\n");
        test.append(Html.fromHtml("<sup>five</sup>/<sub>9</sub>"));
        test.append("\n");
        List<String> allMatches = new ArrayList<String>();
        String regex="\\d{1,5}([.]\\d{1,3}|(\\s\\d{1,5})?[/]\\d{1,3})?";
        Matcher m = Pattern.compile(regex)
                .matcher(fItem.getIngredients());
        while (m.find()) {
            allMatches.add(m.group());
        }
        String str = ingredientArray[0];
        str = str.replaceAll("\\D+","");
       // time.setText("20 min");
        servings.setText(fItem.getServings());
        calories.setText(str+" "+"calories");
       // ingredients.setText(fItem.getIngredients());
        recipeImage.setScaleType(ImageView.ScaleType.FIT_XY);
        recipeName.setText(fItem.getRecipename());
        nutritiions.setText(fItem.getNutriInfo());
        Picasso.with(mContext).load("file:///android_asset/southeren/" + fItem.getRecipename() + ".jpg").into(recipeImage);

//        if(fItem.getIngredientType().contentEquals("Chicken")) {
//
//            Picasso.with(mContext).load("file:///android_asset/chicken/" + fItem.getRecipename() + ".jpg").into(recipeImage);
//
//        }
//
//        if(fItem.getIngredientType().contentEquals("Beef")) {
//
//            Picasso.with(mContext).load("file:///android_asset/beaf/" + fItem.getRecipename() + ".jpg").into(recipeImage);
//
//
//        }
//        if(fItem.getIngredientType().contentEquals("Fruit")||fItem.getIngredientType().contentEquals("Vegetable")||fItem.getIngredientType().contentEquals("Fruit, Vegetable)")) {
//            Picasso.with(mContext).load("file:///android_asset/smoothies/" + fItem.getRecipename() + ".jpg").into(recipeImage);
//
//        }
//        if(fItem.getIngredientType().contentEquals("Fish")) {
//
//            Picasso.with(mContext).load("file:///android_asset/fish/" + fItem.getRecipename() + ".jpg").into(recipeImage);
//
//
//        }




        method.setText(fItem.getMethod());


    }

    public void addIngredients(final FoodItem fItem){


        inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout options_layout = (LinearLayout) findViewById(R.id.optionlist);
        final String[] options = fItem.getIngredientsArray();
        //getResources().getStringArray(R.array.option_list);
       // ingredientCount.setText(Integer.toString(fItem.getIngredientsArray().length));

        for ( int i = 0; i < fItem.getIngredientsArray().length; i++) {

            final int j = i;

            View to_add = inflater.inflate(R.layout.ingredientitem,options_layout, false);

            TextView text = (TextView) to_add.findViewById(R.id.text);
            final ImageView   ivb = (ImageView) to_add.findViewById(R.id.button);
            DatabaseHandler db = new DatabaseHandler(mContext);


            ArrayList<ShoppingItem>   sItemglobal = new ArrayList<ShoppingItem>();

            sItemglobal =   db.getRecipeName(options[i]);

         //
         //
         //  Toast.makeText(mContext, Integer.toString(sItemglobal.size()), Toast.LENGTH_SHORT).show();

            if(sItemglobal.size()== 0) {

                ivb.setImageResource(R.drawable.plus);
                ivb.setTag("positive");
            }
            if(sItemglobal.size()>0){

                ivb.setImageResource(R.drawable.minus);
                ivb.setTag("negative");
            }


            //  resourceID = R.drawable.plus;
            //   sItemglobal.clear();

            gImageView = ivb;

            ivb.setOnClickListener(new View.OnClickListener() {


                @Override
                public void onClick(View v) {
                    // set(fItem.getRecipename(),options[j]);



                    gImageView = ivb;


                    //  Integer   ivTag = (Integer) ivb.getTag();


                    // imageviewAction(new ShoppingItem(fItem.getRecipename(),options[j]),v,ivb);
                  //  Toast.makeText(mContext, Integer.toString(resourceID), Toast.LENGTH_SHORT).show();

                    int zeroFlag = 0;

                    //resourceID = R.drawable.plus;

                    DatabaseHandler db = new DatabaseHandler(mContext);


                    if (ivb.getTag() == "positive") {
                        ivb.setImageResource(R.drawable.minus);
                        minusFlag = true;
                        plusFlag = false;
                        add(new ShoppingItem(fItem.getRecipename(), options[j]), v);
                        zeroFlag = 1;

                        ivb.setTag("negative");


                    }



                    if (ivb.getTag()=="negative"&&zeroFlag==0) {

                        ivb.setImageResource(R.drawable.plus);
                        // resourceID = R.drawable.minus;
                        db = new DatabaseHandler(mContext);
                        ArrayList<ShoppingItem> sItem = db.getRecipeName(options[j]);

                        delete(sItem, v);

                        zeroFlag = 1;
                        ivb.setTag("positive");
                    }





                }
            });
            text.setText(options[i]);



            //  text.setTypeface(FontSelector.getBold(getActivity()));
            options_layout.addView(to_add);
        }


    }

    public void add(ShoppingItem sItem,View v){
        final DatabaseHandler db = new DatabaseHandler(mContext);

        /**
         * CRUD Operations
         * */




        Log.d("Insert: ", "Inserting ..");
        db.addContact(sItem);


        // Reading all contacts
        Log.d("Reading: ", "Reading all contacts..");






        Snackbar bar = Snackbar.make(v, Html.fromHtml("<font color=\"#ffffff\">Ingredient Added</font>"), Snackbar.LENGTH_LONG)
                .setAction(Html.fromHtml("<font color=\"#b4ac01\">View Shopping List</font>"), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Main2Activity.this, ShoppingListActivity.class);

                        startActivity(intent);
                    }
                });

        bar.show();



    }

    public void delete(ArrayList<ShoppingItem> sItem,View v) {
        // Inserting Contacts
        final DatabaseHandler db = new DatabaseHandler(mContext);

        if(sItem.size()>0){

            for (int i=0;i<sItem.size();i++){
                db.deleteContact(sItem.get(i));
            }


        }




        // ivb.setTag(R.drawable.minus);
        //do work here

        Snackbar bar = Snackbar.make(v, "Ingredient Item deleted", Snackbar.LENGTH_LONG)
                .setAction("View List", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Main2Activity.this, ShoppingListActivity.class);

                        startActivity(intent);
                    }
                });

        bar.show();


    }

    public void seekbarTextSize(){

        AlertDialog.Builder yourDialog = new AlertDialog.Builder(
                Main2Activity.this);
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        View layout1 = inflater.inflate(R.layout.textsizedialoge, (ViewGroup) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.root));
        final SeekBar sk=(SeekBar) layout1.findViewById(R.id.textsizeseekbar);
       sk.setMax(40);
       sk.setProgress(seekbarvalue);

        sk.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // TODO Auto-generated method stub

                if(progress>10){
                    seekbarvalue = progress;
                }
                else{

                    seekbarvalue=12;
                }

               // method.setTextSize(progress);
               // Toast.makeText(getApplicationContext(), String.valueOf(progress), Toast.LENGTH_LONG).show();

            }
        });


        yourDialog.setPositiveButton("Set", new DialogInterface.OnClickListener() {


            @Override
            public void onClick(DialogInterface dialog, int which) {

                method.setTextSize(seekbarvalue);


            }


        });

        yourDialog.setNegativeButton("Cancel", null);

        GridView gridView = (GridView) layout1.findViewById(R.id.gridview);
        gridView.setAdapter(new ImageAdapter(this));

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent,
                                    View v, int position, long id) {

                //  iv_sticker.setImageResource(imageIDs[position]);
                method.setTextColor(Color.parseColor(colorIDs[position]));
                // method.setTextColor(Integer.valueOf(colorIDs[position]));
//                Toast.makeText(getBaseContext(),
//                        "pic" + (position + 1) + " selected",
//                        Toast.LENGTH_SHORT).show();
            }
        });

        yourDialog.setView(layout1);



        AlertDialog dialog = yourDialog.create();

        dialog.show();



    }

    public class ImageAdapter extends BaseAdapter
    {
        private Context context;

        public ImageAdapter(Context c)
        {
            context = c;
        }

        //---returns the number of images---
        public int getCount() {
            return imageIDs.length;
        }

        //---returns the ID of an item---
        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        //---returns an ImageView view---
        public View getView(int position, View convertView, ViewGroup parent)
        {
            ImageView imageView;
            if (convertView == null) {
                imageView = new ImageView(Main2Activity.this);
                imageView.setLayoutParams(new GridView.LayoutParams(185, 185));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setPadding(5, 5, 5, 5);
            } else {
                imageView = (ImageView) convertView;
            }
            imageView.setImageResource(imageIDs[position]);
            return imageView;
        }
    }









}
