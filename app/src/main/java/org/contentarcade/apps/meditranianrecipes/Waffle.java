package org.contentarcade.apps.meditranianrecipes;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by yasir on 10/13/2016.
 */
public class Waffle extends Fragment {


    int totalinArray;

    int j;

    SingeltonPattern sPattern;
    public Waffle() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView = inflater.inflate(R.layout.fragment_one, container, false);



        sPattern = SingeltonPattern.getInstance();
        loadJSONFromAsset();

        sPattern.setFoodItems(loadJSONFromAsset());



        ArrayList<FoodItem> foodItems = sPattern.getFoodItems();

        ArrayList<FoodItem> mixedITems = new ArrayList<FoodItem>();

        Log.d("foodItemsbreak", Integer.toString(foodItems.size()));



        ListView lv = (ListView)rootView.findViewById(R.id.listView);
        WaffleAdapter mAdapter ;
        totalinArray = 1;
        j=0;
        mAdapter       = new WaffleAdapter(getActivity(), mixedITems);

        for (int i = 0; i < foodItems.size()+totalinArray; i++) {

            //  mixedITems.add(foodItems.get(i));


            if (i % 3 == 0) {

                if(i>0) {
                    totalinArray = totalinArray + 1;
                    //  FoodItem fItem = new FoodItem();
                    mAdapter.addSeparatorItem(foodItems.get(0));
                }

                //
            }
            else {



                mixedITems.add(foodItems.get(j));

                j = j + 1;

            }

        }



        lv.setAdapter(mAdapter);




        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ListViewFoodAdapter lvfa       = new ListViewFoodAdapter();
                lvfa.notifyDataSetChanged();

            }
        });

        return rootView;
    }


    public ArrayList<FoodItem> loadJSONFromAsset() {
        ArrayList<FoodItem> locList = new ArrayList<>();

        String json = null;
        try {
            // InputStream is = getActivity().getAssets().open("breakfast.json");
            InputStream is = getResources().openRawResource(R.raw.southeren);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        try {
            sPattern = SingeltonPattern.getInstance();
            JSONObject obj = new JSONObject(json);
            JSONArray m_jArry = obj.getJSONArray("Southern");

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);

                if(jo_inside.getString("Food Type").contains("Waffle")) {

                    FoodItem recipies = new FoodItem();
                    String ingredientArray[] = jo_inside.getString("Ingredients").split("\\r?\\n");
                    recipies.setIngredientsArray(ingredientArray);

                    recipies.setRecipename(jo_inside.getString("Recipe Name"));
                    recipies.setIngredients(jo_inside.getString("Ingredients"));
                    recipies.setMethod(jo_inside.getString("Method"));
                    recipies.setIngredientType(jo_inside.getString("Ingredient Type"));
                    recipies.setMealType(jo_inside.getString("Meal Type"));
                    recipies.setFoodType(jo_inside.getString("Food Type"));
                    recipies.setServings(jo_inside.getString("No. of Servings"));
                    recipies.setCaloriesBudget(jo_inside.getString("Calorie Budget"));
                    recipies.setNutriInfo(jo_inside.getString("Nutritional Information"));


                    //Add your values in your `ArrayList` as below:
                    locList.add(recipies);

                }



            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sPattern.setWaffleItems(locList);

        // Log.d("OneFragmentLog", sPattern.foodItems.get(1).getRecipename());


        return locList;
    }

    public interface YourFragmentInterface {
        void fragmentBecameVisible();
    }


}
