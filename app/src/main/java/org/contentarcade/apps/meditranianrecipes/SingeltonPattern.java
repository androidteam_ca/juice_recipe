package org.contentarcade.apps.meditranianrecipes;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by yasir on 10/22/2016.
 */
public class SingeltonPattern {

    private static SingeltonPattern singleton = new SingeltonPattern( );

    String recipieType;
    String minCalories;
    String maxCalorie;
    ArrayList<FoodItem> lunchItems;
    ArrayList<FoodItem> dinnerItems;
    ArrayList<FoodItem> appetizeItems;

    public ArrayList<FoodItem> getCookieItems() {
        return CookieItems;
    }

    public void setCookieItems(ArrayList<FoodItem> cookieItems) {
        CookieItems = cookieItems;
    }

    ArrayList<FoodItem> CookieItems;


    public ArrayList<FoodItem> getBreadItems() {
        return BreadItems;
    }

    public void setBreadItems(ArrayList<FoodItem> breadItems) {
        BreadItems = breadItems;
    }

    ArrayList<FoodItem> BreadItems;





    public ArrayList<FoodItem> getSandwichItems() {
        return sandwichItems;
    }

    public void setSandwichItems(ArrayList<FoodItem> sandwichItems) {
        this.sandwichItems = sandwichItems;
    }

    public ArrayList<FoodItem> getAppetizeItems() {
        return appetizeItems;
    }

    public void setAppetizeItems(ArrayList<FoodItem> appetizeItems) {
        this.appetizeItems = appetizeItems;
    }

    public ArrayList<FoodItem> getWaffleItems() {
        return waffleItems;
    }

    public void setWaffleItems(ArrayList<FoodItem> waffleItems) {
        this.waffleItems = waffleItems;
    }

    ArrayList<FoodItem> waffleItems;

    public ArrayList<FoodItem> getPancakeItems() {
        return pancakeItems;
    }

    public void setPancakeItems(ArrayList<FoodItem> pancakeItems) {
        this.pancakeItems = pancakeItems;
    }

    ArrayList<FoodItem> pancakeItems;

    public ArrayList<FoodItem> getSmoothieItems() {
        return smoothieItems;
    }

    public void setSmoothieItems(ArrayList<FoodItem> smoothieItems) {
        this.smoothieItems = smoothieItems;
    }

    ArrayList<FoodItem> smoothieItems;

    public ArrayList<FoodItem> getDesrtItems() {
        return desrtItems;
    }

    public void setDesrtItems(ArrayList<FoodItem> desrtItems) {
        this.desrtItems = desrtItems;
    }

    ArrayList<FoodItem> desrtItems;


    ArrayList<FoodItem> sandwichItems;

    ArrayList<FoodItem> snackItems;
    ArrayList<FoodItem> currentArray;

    public ArrayList<JSONObject> getCategoriesArray() {
        return categoriesArray;
    }

    public void setCategoriesArray(ArrayList<JSONObject> categoriesArray) {
        this.categoriesArray = categoriesArray;
    }

    ArrayList<JSONObject> categoriesArray;
    String ingredientType;
    ArrayList<FoodItem> foodItems;
    String[] ingredientsArray;

    public String ingredientSeafood;
    public String ingredientTurky;
    public String ingredientChicken;
    public String ingredientPasta;
    public String ingredientFruit;
    public String ingredientStarch;
    public String ingredientBeef;
    public String ingredientBread;

    public ArrayList<category> getCatList() {
        return catList;
    }

    public void setCatList(ArrayList<category> catList) {
        this.catList = catList;
    }

    public ArrayList<category> catList;






    public String[] getMethodArray() {
        return methodArray;
    }

    public void setMethodArray(String[] methodArray) {
        this.methodArray = methodArray;
    }

    String[] methodArray;

    public String[] getIngredientsArray() {
        return ingredientsArray;
    }

    public void setIngredientsArray(String[] ingredientsArray) {
        this.ingredientsArray = ingredientsArray;
    }




    public boolean isAnimationshowflag() {
        return animationshowflag;
    }

    public void setAnimationshowflag(boolean animationshowflag) {
        this.animationshowflag = animationshowflag;
    }

    boolean animationshowflag;

    public ArrayList<FoodItem> getCurrentArray() {
        return currentArray;
    }

    public void setCurrentArray(ArrayList<FoodItem> currentArray) {
        this.currentArray = currentArray;
    }



    public ArrayList<FoodItem> getSnackItems() {
        return snackItems;
    }

    public void setSnackItems(ArrayList<FoodItem> snackItems) {
        this.snackItems = snackItems;
    }



    public ArrayList<FoodItem> getDinnerItems() {
        return dinnerItems;
    }

    public void setDinnerItems(ArrayList<FoodItem> dinnerItems) {
        this.dinnerItems = dinnerItems;
    }



    public String getIngredientType() {
        return ingredientType;
    }

    public void setIngredientType(String ingredientType) {
        this.ingredientType = ingredientType;
    }



    public String getMealType() {
        return mealType;
    }

    public void setMealType(String mealType) {
        this.mealType = mealType;
    }

    String mealType;







    public ArrayList<FoodItem> getLunchItems() {
        return lunchItems;
    }

    public void setLunchItems(ArrayList<FoodItem> lunchItems) {
        this.lunchItems = lunchItems;
    }


    int caloriesBudgetIndex;
    String[] values= {"All recipies",">100 calories", ">200 calories", ">300 calories", ">400 calories"

    };

    public ArrayList<FoodItem> getFilterArray() {
        return filterArray;
    }

    public int getCaloriesBudgetIndex() {
        return caloriesBudgetIndex;
    }

    public void setCaloriesBudgetIndex(int caloriesBudgetIndex) {
        this.caloriesBudgetIndex = caloriesBudgetIndex;
    }

    public void setFilterArray(ArrayList<FoodItem> filterArray) {
        this.filterArray = filterArray;
    }

    ArrayList<FoodItem> filterArray;


    public ArrayList<FoodItem> getFoodItems() {
        return foodItems;
    }

    public void setFoodItems(ArrayList<FoodItem> foodItems) {
        this.foodItems = foodItems;
    }



    public String getRecipieType() {
        return recipieType;
    }

    public void setRecipieType(String recipieType) {
        this.recipieType = recipieType;
    }

    public String getMinCalories() {
        return minCalories;
    }

    public void setMinCalories(String minCalories) {
        this.minCalories = minCalories;
    }

    public String getMaxCalorie() {
        return maxCalorie;
    }

    public void setMaxCalorie(String maxCalorie) {
        this.maxCalorie = maxCalorie;
    }




    /* A private Constructor prevents any other
     * class from instantiating.
     */
    private SingeltonPattern() {

   // foodItems = new ArrayList<FoodItem>();

    }

    /* Static 'instance' method */
    public static SingeltonPattern getInstance( ) {
        return singleton;
    }

    /* Other methods protected by singleton-ness */
    protected static void demoMethod( ) {
        System.out.println("demoMethod for singleton");
    }



}
