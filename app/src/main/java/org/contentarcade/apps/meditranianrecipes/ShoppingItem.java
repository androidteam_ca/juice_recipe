package org.contentarcade.apps.meditranianrecipes;

/**
 * Created by yasir on 11/15/2016.
 */
public class ShoppingItem {

    public String shoppingItem;
    public String recipeRefrence;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    String _id;

    // Empty constructor
    public ShoppingItem(){

    }
    //Constructor first

    public ShoppingItem(String name){

        this.recipeRefrence = name;

    }

    // constructor
    public ShoppingItem(String id, String name, String shoping_item){
        this._id = id;
        this.recipeRefrence = name;
        this.shoppingItem = shoping_item;
    }

    // constructor
    public ShoppingItem(String name, String shopingITem){
        this.recipeRefrence = name;
        this.shoppingItem = shopingITem;
    }



    public String getShoppingItem() {
        return shoppingItem;
    }

    public void setShoppingItem(String shoppingItem) {
        this.shoppingItem = shoppingItem;
    }


    public String getRecipeRefrence() {
        return recipeRefrence;
    }

    public void setRecipeRefrence(String recipeRefrence) {
        this.recipeRefrence = recipeRefrence;
    }





}
