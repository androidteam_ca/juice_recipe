package org.contentarcade.apps.meditranianrecipes;

/**
 * Created by yasir on 11/15/2016.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "contactsManager";

    // Contacts table name
    private static final String TABLE_CONTACTS = "contacts";

    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "recipename";
    private static final String KEY_INGREDIENT = "shopingingredient";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_CONTACTS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
                + KEY_INGREDIENT + " TEXT" + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);

        // Create tables again
        onCreate(db);
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new contact
    void addContact(ShoppingItem contact) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, contact.getShoppingItem()); // Contact Name
        values.put(KEY_INGREDIENT, contact.getRecipeRefrence()); // Contact Phone

        // Inserting Row
        db.insert(TABLE_CONTACTS, null, values);
        db.close(); // Closing database connection
    }

//    // Getting single contact
//   ArrayList<ShoppingItem>  getContact(ShoppingItem shopingItem) {
//        SQLiteDatabase db = this.getReadableDatabase();
//         ArrayList<ShoppingItem> sItem = new ArrayList<ShoppingItem>();
//        Cursor cursor = db.query(TABLE_CONTACTS, new String[] {
//                        KEY_NAME }, KEY_NAME + "=?",
//                new String[] {shopingItem.getShoppingItem().toString()});
//        if (cursor != null)
//            cursor.moveToFirst();
//
//        ShoppingItem contact = new ShoppingItem( cursor.getString(0));
//        // return contact
//                 sItem.add(contact);
//
//
//        return sItem;
//
//
//    }

    public ArrayList<ShoppingItem> getRecipeName(String recipeName) {


            SQLiteDatabase db = this.getReadableDatabase();
        String[] COLUMNS = {KEY_ID,KEY_INGREDIENT, KEY_NAME};
        Cursor res =
                db.query(TABLE_CONTACTS,
                        COLUMNS,
                        "recipename= ?",
                        new String[]{recipeName},
                        null,
                        null,
                        null,
                        null);
           // Cursor res = db.rawQuery("SELECT * FROM contacts WHERE shopingingredient = ?", new String[] {recipeName});
            ArrayList<ShoppingItem> sItemArray = new ArrayList<ShoppingItem>();
        Log.d("counte1",Integer.toString(sItemArray.size()));

        if (res.moveToFirst()){
            do{

                ShoppingItem nShoppingItem = new ShoppingItem();
                nShoppingItem.set_id(res.getString(res.getColumnIndex(KEY_ID)));
                nShoppingItem.setShoppingItem(res.getString(res.getColumnIndex(KEY_NAME)));
                nShoppingItem.setRecipeRefrence(res.getString(res.getColumnIndex(KEY_INGREDIENT)));

                sItemArray.add(nShoppingItem);

                // do what ever you want here
            }while(res.moveToNext());
        }
        res.close();

            return sItemArray;


    }


    // Getting All Contacts
    public List<ShoppingItem> getAllContacts() {
        List<ShoppingItem> contactList = new ArrayList<ShoppingItem>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ShoppingItem contact = new ShoppingItem();
                contact.set_id(cursor.getString(0));
                contact.setShoppingItem(cursor.getString(1));
                contact.setRecipeRefrence(cursor.getString(2));
                // Adding contact to list
                contactList.add(contact);
            } while (cursor.moveToNext());
        }

        // return contact list
        return contactList;
    }

    // Updating single contact
    public int updateContact(ShoppingItem contact) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, contact.getRecipeRefrence());
        values.put(KEY_INGREDIENT, contact.getShoppingItem());

        // updating row
        return db.update(TABLE_CONTACTS, values, KEY_ID + " = ?",
                new String[] { String.valueOf(contact.get_id()) });
    }

    // Deleting single contact
    public void deleteContact(ShoppingItem contact) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CONTACTS, KEY_ID + " = ?",
                new String[] { String.valueOf(contact.get_id()) });
        db.close();
    }


    // Getting contacts Count
    public int getContactsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_CONTACTS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }

}