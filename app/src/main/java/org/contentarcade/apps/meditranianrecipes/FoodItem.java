package org.contentarcade.apps.meditranianrecipes;

import java.io.Serializable;

/**
 * Created by yasir on 10/16/2016.
 */

@SuppressWarnings("serial")

public class FoodItem implements Serializable {

    String recipename;
    String ingredients;
    String[] ingredientsArray;

    String method;

    String ingredientType;

    public String getRecipename() {
        return recipename;
    }

    public void setRecipename(String recipename) {
        this.recipename = recipename;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getIngredientType() {
        return ingredientType;
    }

    public void setIngredientType(String ingredientType) {
        this.ingredientType = ingredientType;
    }

    public String getMealType() {
        return mealType;
    }

    public void setMealType(String mealType) {
        this.mealType = mealType;
    }

    public String getFoodType() {
        return foodType;
    }

    public void setFoodType(String foodType) {
        this.foodType = foodType;
    }

    public String getServings() {
        return servings;
    }

    public void setServings(String servings) {
        this.servings = servings;
    }

    public String getCaloriesBudget() {
        return caloriesBudget;
    }

    public void setCaloriesBudget(String caloriesBudget) {
        this.caloriesBudget = caloriesBudget;
    }

    public String getNutriInfo() {
        return nutriInfo;
    }

    public void setNutriInfo(String nutriInfo) {
        this.nutriInfo = nutriInfo;
    }

    String mealType;
    String foodType;
    String servings;
    String caloriesBudget;
    String nutriInfo;

    public String[] getMethodArray() {
        return methodArray;
    }

    public void setMethodArray(String[] methodArray) {
        this.methodArray = methodArray;
    }

    String[] methodArray;

    public String[] getIngredientsArray() {
        return ingredientsArray;
    }

    public void setIngredientsArray(String[] ingredientsArray) {
        this.ingredientsArray = ingredientsArray;
    }



    public FoodItem() {
    }







}
