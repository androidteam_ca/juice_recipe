package org.contentarcade.apps.meditranianrecipes;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

public class ShoppingListActivity extends AppCompatActivity {


    ShoppingItem sItem;
    Context mContext;
    ShoppingListActivityAdapter lvfa;
    DatabaseHandler db;
    ListView lv;
    List<ShoppingItem> contacts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(org.contentarcade.apps.meditranianrecipes.R.layout.activity_shopping_list);
        Toolbar toolbar = (Toolbar) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.toolbar);
        setSupportActionBar(toolbar);

        mContext = this;

        FloatingActionButton fab = (FloatingActionButton) findViewById(org.contentarcade.apps.meditranianrecipes.R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });





            db = new DatabaseHandler(this);



            // Reading all contacts

         contacts = db.getAllContacts();
        Log.d("Reading: ", Integer.toString(contacts.size()));
         lv = (ListView)findViewById(org.contentarcade.apps.meditranianrecipes.R.id.listView);
        lvfa  = new ShoppingListActivityAdapter(ShoppingListActivity.this, contacts);
        lv.setAdapter(lvfa);

       lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
           @Override
           public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
              // lvfa.swapItems(db.getAllContacts());

               runOnUiThread(new Runnable() {

                   @Override
                   public void run() {
                       //notifyDataSetChanged here or update your UI on different thread

                       db.deleteContact(contacts.get(position));
                       contacts.clear();
                       contacts = db.getAllContacts();


                       lvfa = new ShoppingListActivityAdapter(ShoppingListActivity.this, contacts);
                       lv.setAdapter(lvfa);

                       lvfa.notifyDataSetChanged();
                   }
               });
           }
       });




            for (ShoppingItem cn : contacts) {
                String log = "Id: "+cn.get_id()+" ,Name: " + cn.getRecipeRefrence() + " ,Phone: " + cn.shoppingItem;
                // Writing Contacts to log
                Log.d("Name: ", log);
            }
        }


    @Override
    public void onResume() {
        super.onResume();
        lvfa.swapItems(db.getAllContacts());
    }







}
