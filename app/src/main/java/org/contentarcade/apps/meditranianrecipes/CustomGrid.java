package org.contentarcade.apps.meditranianrecipes;

/**
 * Created by Yasir on 1/5/2017.
 */
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CustomGrid extends BaseAdapter{
    private Context mContext;
//    private final String[] web;
//    private final int[] Imageid;
    ArrayList<category> web;

    public CustomGrid(Context c, ArrayList<category>web) {
        mContext = c;

        this.web = web;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return web.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View grid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            grid = new View(mContext);
            grid = inflater.inflate(R.layout.grid_item, null);
           // TextView textView = (TextView) grid.findViewById(R.id.counter);
            TextView textIngredientType = (TextView) grid.findViewById(R.id.ingredienttype);
            ImageView imageView = (ImageView)grid.findViewById(R.id.grid_item_image);
            ImageView bg = (ImageView)grid.findViewById(R.id.bg);
            SingeltonPattern sp = SingeltonPattern.getInstance();

                //textView.setText(web.get(position).getCounter());

                textIngredientType.setText(web.get(position).getIngredientType());


                Picasso.with(mContext).load("file:///android_asset/southeren/" + web.get(position).getIcon() + ".PNG").into(imageView);


                Picasso.with(mContext).load("file:///android_asset/southeren/" + web.get(position).getBg() + ".png").into((bg));


        } else {
            grid = (View) convertView;
        }

        return grid;
    }
}