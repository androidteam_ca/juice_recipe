package org.contentarcade.apps.meditranianrecipes;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by yasir on 10/25/2016.
 */
public class Snacks  extends Fragment {

    SingeltonPattern sPattern;

    int totalinArray;

    int j;
    public Snacks() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView = inflater.inflate(R.layout.fragment_one, container, false);



        loadJSONFromAsset();

        sPattern = SingeltonPattern.getInstance();

        ArrayList<FoodItem> foodItems = sPattern.getSnackItems();

        // Log.d("foodItems", Integer.toString(foodItems.size()));



        ListView lv = (ListView)rootView.findViewById(R.id.listView);
        ArrayList<FoodItem> mixedITems = new ArrayList<FoodItem>();




        Log.d("foodItemsbreak", Integer.toString(foodItems.size()));





        totalinArray = 1;
        j=0;
        SnacksAdapter mAdapter       = new SnacksAdapter(getActivity(), mixedITems);

        for (int i = 0; i < foodItems.size(); i++) {

            //mixedITems.add(foodItems.get(j));

            if (i % 3 == 0) {
                if(i>0) {

                    totalinArray = totalinArray + 1;
                    //  FoodItem fItem = new FoodItem();
                    mAdapter.addSeparatorItem(foodItems.get(0));

                }
            }
            else{


                mixedITems.add(foodItems.get(j));
                j = j +1;
            }


        }
       // mAdapter.addSeparatorItem(foodItems.get(0));


        lv.setAdapter(mAdapter);



        return rootView;
    }

    private ArrayList<FoodItem> GetFoodItems(){
        ArrayList<FoodItem> contactlist = new ArrayList<FoodItem>();

        FoodItem recipies = new FoodItem();

//        recipies.setName("Stephen");
//        recipies.setDescription("01213113568");
//        recipies.setServingSize("1");
//
//        contactlist.add(recipies);
//
//        FoodItem recipies1 = new FoodItem();
//
//        recipies1.setName("Mephen");
//        recipies1.setDescription("0145623");
//        recipies1.setServingSize("2");
//
//        contactlist.add(recipies1);
//
//        FoodItem recipies2 = new FoodItem();
//
//        recipies2.setName("Bephen");
//        recipies2.setDescription("0014589");
//        recipies2.setServingSize("3");

        //   contactlist.add(recipies2);

        return contactlist;
    }
    public ArrayList<FoodItem> loadJSONFromAsset() {
        ArrayList<FoodItem> locList = new ArrayList<>();
        sPattern = SingeltonPattern.getInstance();
        String json = null;
        try {
            // InputStream is = getActivity().getAssets().open("snacks.json");
            InputStream is = getResources().openRawResource(R.raw.southeren);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        try {
            JSONObject obj = new JSONObject(json);
            JSONArray m_jArry = obj.getJSONArray("Southern");

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);
                if(jo_inside.getString("Food Type").contains("Bar")) {
                    FoodItem recipies = new FoodItem();
                    String ingredientArray[] = jo_inside.getString("Ingredients").split("\\r?\\n");
                    recipies.setIngredientsArray(ingredientArray);

                    recipies.setRecipename(jo_inside.getString("Recipe Name"));
                    recipies.setIngredients(jo_inside.getString("Ingredients"));
                    recipies.setMethod(jo_inside.getString("Method"));
                    recipies.setIngredientType(jo_inside.getString("Ingredient Type"));
                    recipies.setMealType(jo_inside.getString("Meal Type"));
                    recipies.setFoodType(jo_inside.getString("Food Type"));
                    recipies.setServings(jo_inside.getString("No. of Servings"));
                    recipies.setCaloriesBudget(jo_inside.getString("Calorie Budget"));
                    recipies.setNutriInfo(jo_inside.getString("Nutritional Information"));


                    //Add your values in your `ArrayList` as below:
                    locList.add(recipies);
                }

                sPattern.setSnackItems(locList);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return locList;
    }


}
