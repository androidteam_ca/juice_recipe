package org.contentarcade.apps.meditranianrecipes;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class RateUs {
    // Insert your Application Title
    private final static String TITLE = "Healthy Recipies for Weight Loss";

    // Insert your Application Package Name
    private final static String PACKAGE_NAME = "org.contentarcade.apps.tofurecipes";

    // Day until the Rate Us Dialog Prompt(Default second Days)
    private final static int DAYS_UNTIL_PROMPT = 1;

    // App launches until Rate Us Dialog Prompt(Default five Launches)
    private final static int LAUNCHES_UNTIL_PROMPT = 5;
    private static long launch_count;
    private static SharedPreferences prefs;




    public static void app_launched(Context mContext) {
        prefs = mContext.getSharedPreferences("rateus", 0);
        if (prefs.getBoolean("dontshowagain", false)) {
            return;
        }

        SharedPreferences.Editor editor = prefs.edit();

        // Increment launch counter
         launch_count = prefs.getLong("launch_count", 0) + 1;
        editor.putLong("launch_count", launch_count);

        // Get date of first launch
        Long date_firstLaunch = prefs.getLong("date_firstlaunch", 0);
        if (date_firstLaunch == 0) {
            date_firstLaunch = System.currentTimeMillis();
            editor.putLong("date_firstlaunch", date_firstLaunch);
        }

        // Wait at least n days before opening
        if (launch_count >= LAUNCHES_UNTIL_PROMPT) {
            if (System.currentTimeMillis() >= date_firstLaunch
                    + (DAYS_UNTIL_PROMPT * 24 * 60 * 60 * 1000)) {
                showRateDialog(mContext, editor,launch_count);
            }
        }

        editor.commit();
    }

    public static void showRateDialog(final Context mContext,
                                      final SharedPreferences.Editor editor,  long launchCount) {




       // final Dialog dialog = new Dialog(mContext);
        AlertDialog.Builder dialog = new AlertDialog.Builder(
                mContext);
        // Set Dialog Title
       // dialog.setTitle("Rate " + TITLE);

        LinearLayout ll = new LinearLayout(mContext);
        ll.setOrientation(LinearLayout.VERTICAL);

        TextView tv = new TextView(mContext);
//        tv.setText("If you like " + TITLE
//                + ", please give us some stars and comment");
//        tv.setWidth(240);
//        tv.setPadding(fourth, 0, fourth, 10);
//        ll.addView(tv);

        dialog.setPositiveButton("Now", new DialogInterface.OnClickListener() {


            @Override
            public void onClick(DialogInterface dialog, int which) {

                mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri
                        .parse("market://details?id=" + PACKAGE_NAME)));
                editor.putBoolean("dontshowagain", true);
                    editor.commit();
            }


        });
        editor.putLong("launch_count", launch_count);
        dialog.setNegativeButton("Remind me later",new DialogInterface.OnClickListener() {


            @Override
            public void onClick(DialogInterface dialog, int which) {

                editor.putLong("launch_count", 0);
                editor.commit();
            }


        });




//        Button b1 = new Button(mContext);
//        b1.setText("Rate " + TITLE);
//        b1.setOnClickListener(new OnClickListener() {
//            public void onClick(View v) {
//                mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri
//                        .parse("market://details?id=" + PACKAGE_NAME)));
//               // dialog.dismiss();
//            }
//        });
//        ll.addView(b1);

//        // Second Button
//        Button b2 = new Button(mContext);
//        b2.setText("Remind me later");
//        b2.setOnClickListener(new OnClickListener() {
//            public void onClick(View v) {
//                //dialog.dismiss();
//            }
//        });
//        ll.addView(b2);
//
//        // Third Button
//        Button b3 = new Button(mContext);
//        b3.setText("Stop Bugging me");
//        b3.setOnClickListener(new OnClickListener() {
//            public void onClick(View v) {
//                if (editor != null) {
//                    editor.putBoolean("dontshowagain", true);
//                    editor.commit();
//                }
//                dialog.dismiss();
//            }
//        });
//        ll.addView(b3);
        View view;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(org.contentarcade.apps.meditranianrecipes.R.layout.rank_dialog, null);

        dialog.setView(org.contentarcade.apps.meditranianrecipes.R.layout.rank_dialog);


        // Show Dialog
        dialog.show();


    }



}