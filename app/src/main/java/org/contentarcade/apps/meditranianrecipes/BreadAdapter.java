package org.contentarcade.apps.meditranianrecipes;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.NativeExpressAdView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.TreeSet;

/**
 * Created by mbk82 on 3/10/2017.
 */

public class BreadAdapter extends BaseAdapter {



    private static ArrayList<FoodItem> listContact;

    private LayoutInflater mInflater;

    Context mContext;

    private static final int TYPE_ITEM = 0;
    private static final int TYPE_SEPARATOR = 1;
    private static final int TYPE_MAX_COUNT = TYPE_SEPARATOR + 1;
    private TreeSet mSeparatorsSet = new TreeSet();

    public BreadAdapter(){


    }

    public BreadAdapter(Context photosFragment, ArrayList<FoodItem> results){
        listContact = results;
        mInflater = LayoutInflater.from(photosFragment);
        mContext = photosFragment;
    }

    public void addItem(final FoodItem item) {
        listContact.add(item);
        notifyDataSetChanged();
    }

    public void addSeparatorItem(final FoodItem item) {
        listContact.add(item);
        // save separator position
        mSeparatorsSet.add(listContact.size() - 1);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return mSeparatorsSet.contains(position) ? TYPE_SEPARATOR : TYPE_ITEM;
    }

    @Override
    public int getViewTypeCount() {
        return TYPE_MAX_COUNT;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return listContact.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return listContact.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return arg0;
    }


    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        BreadAdapter.ViewHolder holder;

        final View result;
        if(convertView == null) {


            holder = new ViewHolder();
            int type = getItemViewType(position);
            System.out.println("getView " + position + " " + convertView + " type = " + type);
            switch (type) {
                case TYPE_ITEM:
                    convertView = mInflater.inflate(R.layout.cardview, null);
                    holder.ingredient = (TextView) convertView.findViewById(R.id.ingredientvalue);
                    holder.mealtype = (TextView) convertView.findViewById(R.id.mealtypevalue);
                    holder.foodtype = (TextView) convertView.findViewById(R.id.foodtypevalue);
                    holder.recipeImage = (ImageView) convertView.findViewById(R.id.recipieimage);
                    holder.recipiename = (TextView)convertView.findViewById(R.id.titletext);
                    // String imageResource = listContact.get(1).getRecipename().toString();

                    break;
                case TYPE_SEPARATOR:
                    convertView = mInflater.inflate(R.layout.list_fruit, null);
                    NativeExpressAdView adView = (NativeExpressAdView)convertView.findViewById(R.id.adView);
                    adView.loadAd(new AdRequest.Builder().build());

                    break;
            }

            // holder.foodName = (ImageView) convertView.findViewById(R.id.ingredient);


            //  imgs.recycle();

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
            result=convertView;
        }
        if(getItemViewType(position)==TYPE_ITEM) {
            holder.ingredient.setText(listContact.get(position).getIngredientType().toString());
            holder.mealtype.setText(listContact.get(position).getMealType().toString());
            holder.foodtype.setText(listContact.get(position).getFoodType().toString());
            holder.recipiename.setText(listContact.get(position).getRecipename().toString());

            // Picasso.with(mContext).load(imgs.getResourceId(0,-1)).into(holder.recipeImage);
            // holder.recipeImage.setImageResource(imgs.getResourceId(0, -1));
            holder.recipeImage.setScaleType(ImageView.ScaleType.FIT_XY);
            Picasso.with(mContext).load("file:///android_asset/southeren/" + listContact.get(position).getRecipename() + ".jpg").into(holder.recipeImage);
        }


//        if (listContact.get(position).getIngredientType().toString().contentEquals("Beef")) {
//
//            holder.recipeImage.setScaleType(ImageView.ScaleType.FIT_XY);
//            Picasso.with(mContext).load("file:///android_asset/beaf/" + listContact.get(position).getRecipename().toString() + ".jpg").into(holder.recipeImage);
//
//        }
//        if (listContact.get(position).getIngredientType().toString().contentEquals("Chicken")) {
//            holder.recipeImage.setScaleType(ImageView.ScaleType.FIT_XY);
//            Picasso.with(mContext).load("file:///android_asset/chicken/" + listContact.get(position).getRecipename().toString() + ".jpg").into(holder.recipeImage);
//        }
//        if (listContact.get(position).getIngredientType().toString().contains("Fruit")||
//                listContact.get(position).getIngredientType().toString().contains("Vegetable")) {
//            holder.recipeImage.setScaleType(ImageView.ScaleType.FIT_XY);
//            Picasso.with(mContext).load("file:///android_asset/smoothies/" + listContact.get(position).getRecipename().toString() + ".jpg").into(holder.recipeImage);
//        }




        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FoodItem food= listContact.get(position);
                Intent i = new Intent(mContext, Main2Activity.class);
                i.putExtra("recipie",food);
                mContext.startActivity(i);
//                Toast.makeText(mContext, listContact.get(position).getIngredientType().toString(), Toast.LENGTH_SHORT).show();
//                Toast.makeText(mContext, Integer.toString(position), Toast.LENGTH_SHORT).show();


            }
        });







//        TextView textViewItemName = (TextView)
//                convertView.findViewById(R.id.foodname);
//        TextView textViewItemDesc = (TextView)
//                convertView.findViewById(R.id.fooddesc);
//        textViewItemName.setText(listContact.get(position).getName());
//
//        textViewItemDesc.setText(listContact.get(position).getDescription());
        // holder.txtphone.setText(listContact);

        return convertView;
    }

    private class ViewHolder {
        ImageView foodName;
        TextView ingredient;
        TextView mealtype;
        TextView foodtype;
        TextView recipiename;
        ImageView recipeImage;



    }

    public void updateList(){
        notifyDataSetChanged();
    }
}
